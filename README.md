# Aplicación de WAOCards
Repositorio oficial privado del código fuente de la aplicación.



## Cómo obtenerlo

### Descarga del proyecto y pruebas en el navegador

1. Crear el repositorio y realizar el  respectivo `git clone` de acuerdo a las instrucciones dadas por el sitio
2. Ejecute el siguiente comando `npm install`
3. Correr el servidor de ionic con el comando `ionic serve`


## Compilación de la app en el dispositivo

### Android

1. Ejecute `ionic cordova run android` ó...
2. Ejecute `ionic cordova build android`


### iOS

1. Ejecute `ionic cordova run ios` ó...
2. Ejecute `ionic cordova build ios`
