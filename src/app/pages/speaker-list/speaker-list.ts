import { Component, ViewEncapsulation, AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ActionSheetController, LoadingController, AlertController, Platform, ModalController } from '@ionic/angular';

import { usuarios } from '../../providers/usuarios';
import { Educacion } from '../../providers/educacion';
import { Experiencias } from '../../providers/experiencias';
import { ToastController } from '@ionic/angular';
import * as urls from '../../providers/urls.servicios';
import { AppComponent } from '../../app.component';
import { TranslateService } from '@ngx-translate/core';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';
import { DeviceMotion, DeviceMotionAccelerationData, DeviceMotionAccelerometerOptions } from '@ionic-native/device-motion/ngx';
import { Ubicacion } from '../../providers/ubicacion';
import { Vibration } from '@ionic-native/vibration/ngx';
import { ChatPage } from '../chat/chat.page';

// import { Network } from '@ionic-native/network/ngx';

@Component({
  selector: 'page-speaker-list',
  templateUrl: 'speaker-list.html',
  styleUrls: ['./speaker-list.scss'],
})
export class SpeakerListPage implements AfterContentInit {
  speakers: any[] = [];
  formaciones: any[] = [];
  experiencias: any[] = [];
  completo: string;
  URLavatar: string;
  contreferencias: any[] = ['1', '1', '1', '1'];
  // elementos del idioma
  DesliceParaRecargar: string;
  ObteniendoSus: string;
  ImgSrc: string;
  // ELEMENTOS DE AGITAR
  lastX: number;
  lastY: number;
  lastZ: number;
  x = 0;
  y = 0;
  z = 0;
  mov = 0;
  buscando = 0;
  subscription;
  acelera: string;
  segment = 'contactos';
  repartidas = false;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public confData: usuarios,
    public edd: Educacion,
    public exxp: Experiencias,
    public inAppBrowser: InAppBrowser,
    public router: Router,
    public toastController: ToastController,
    public AppComponente: AppComponent,
    public _translate: TranslateService,
    public registro: RegistroUsuariosService,
    public valor: Valores,
    private deviceMotion: DeviceMotion,
    public loadingController: LoadingController,
    public ubicacion: Ubicacion,
    public alertController: AlertController,
    public vibration: Vibration,
    // private network: Network,
    public platform: Platform,
    public modalCtrl: ModalController
    ) {
    this.cargarIdiomaElementos();
    this.completo = 'false';
    console.log('los speakers antes: ' + this.completo);
    console.log(this.speakers);

    /* setTimeout(() => {
      this.confData.getSpeakers().subscribe((speakers: any[]) => {
        this.completo = 'true';
        this.speakers = speakers;
        this.URLavatar = urls.URLimagen;
      }, (err: any) => {
        this.completo = 'error';
          this.AppComponente.presentarToastError();
        console.log(JSON.stringify(err));
      }
      );
    }, 1500); */
  }

  ionViewDidEnter() {
    if (this.completo === 'error') {
      this.AppComponente.presentarToastError();
    }

  }

  actualizarSegment() {
    this.repartidas = !this.repartidas;
  }

  ngAfterContentInit() {
 setTimeout(() => {
   this.recargarAmigos();
  // this.acelerar();
  this.verificarConexion();
 }, 80);
 /* setTimeout(() => {
   this.presentarActionSheetCercanos();
 }, 1000); */
  }

  acelerar() {
    // this.agiteahora = true;
    try {
      const opcion: DeviceMotionAccelerometerOptions = {
        frequency: 500
      };

      this.deviceMotion.getCurrentAcceleration().then(
        (acceleration: DeviceMotionAccelerationData) => console.log(acceleration),
        (error: any) => console.log(error)
      );

      // Watch device acceleration
      this.subscription = this.deviceMotion.watchAcceleration(opcion).subscribe((acceleration: DeviceMotionAccelerationData) => {
        console.log('Esta habilitado acelerar!');
        if (!this.lastX) {
          this.lastX = acceleration.x;
          this.lastY = acceleration.y;
          this.lastZ = acceleration.z;
          return;
        }
        this.x = Math.abs(acceleration.x - this.lastX);
        this.y = Math.abs(acceleration.y - this.lastY);
        this.z = Math.abs(acceleration.z - this.lastZ);
        this.acelera = '' + this.x + this.y + this.z;
        if (this.x + this.y + this.z > 36) {
          this.mov = this.mov + 1;
       //   this.PruebaAgitando();
        }
        if (this.mov === 5) {
          setTimeout(() => {
            this.antesdeRepartir();
          //  this.repartirTarjetas();
        //    this.PruebaBuscando();
            // this.subscription.unsubscribe();
          }, 400);

        }
      });
    } catch (err) {
      alert('Error en: ' + err);
    }
  }

  async antesdeRepartir() { // Método para revisar personas cercanas para repartir
    setTimeout(() => {
      this.registro.verPersonasAntesdeRepartir(this.valor.identificacion, ["123", "434", "456456"]);
    }, 100);
    setTimeout(() => {
      this.presentarActionSheetCercanos();
    }, 1000);
  }

  async presentarActionSheetCercanos() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Albums',
      buttons: [{
        text: 'Delete',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
        }
      }, {
        text: 'Share',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'Play (open modal)',
        icon: 'arrow-dropright-circle',
        handler: () => {
          console.log('Play clicked');
        }
      }, {
        text: 'Favorite',
        icon: 'heart',
        handler: () => {
          console.log('Favorite clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }


 async repartirTarjetas() {
   setTimeout(() => {
     console.log('Repartir tarjetas contiene: ');
     console.log(this.ubicacion.latitud, this.ubicacion.longitud, this.valor.identificacion);
     //this.registro.RepartirTarjetas(this.ubicacion.latitud, this.ubicacion.longitud, this.valor.identificacion);
     setTimeout(() => {
       this.reparticionCompleta();
     }, 300);

   }, 2500);
   const loading = await this.loadingController.create({
     duration: 2600,
     message: 'Un momento, por favor...',
     translucent: true,
     cssClass: 'custom-class custom-loading'
   });
   return await loading.present();
 }

  async reparticionCompleta() {
    this.vibration.vibrate([200, 100, 200]);
    const alert = await this.alertController.create({
      header: 'Tarjetas repartidas correctamente',
      message: '<ion-item *ngFor="let speaker of registro.Repartidas.Personas" lines="none" text-wrap> <img src="'
        + this.URLavatar + 'speaker.foto_perfil' +
        '" class="fotoEncontra"><ion-label><p class="parrafoEnc"><ion-text>' +
        'speaker.cargo_actual' +
        '</ion-text></p></ion-label></ion-item>',
      buttons: [
        {
          text: '',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Listo',
          handler: () => {
            // console.log('Confirm Okay');
          }
        }
      ]
    });
    this.subscription.unsubscribe();

    await alert.present();
  }


  cargarIdiomaElementos() {
    this._translate.stream('PAGES.SPEAKER-LIST.DESLICE_PARA_RECARGAR').subscribe((text: string) => {
      this.DesliceParaRecargar = text;
    });
    this._translate.stream('PAGES.SPEAKER-LIST.OBTENIENDO_SUS_CONT').subscribe((text: string) => {
      this.ObteniendoSus = text;
    });
    this._translate.stream('PAGES.SPEAKER-LIST.IMG_SRC').subscribe((text: string) => {
      this.ImgSrc = text;
    });
  }

  async presentarErrorToast() {
    const toast = await this.toastController.create({
      message: 'No hay conexión al servidor, ¡Datos limitados!',
      showCloseButton: true,
      position: 'bottom',
      closeButtonText: 'Listo',
      color: 'danger'
    });
    toast.present();
  }

  recargarContactos() {

    this.completo = 'false';
    console.log('los speakers antes: ' + this.completo);
    console.log(this.speakers);
    setTimeout(() => {
      this.confData.getSpeakers().subscribe((speakers: any[]) => {
        this.completo = 'true';
        this.speakers = speakers;
        this.URLavatar = urls.URLimagen;
      }, (err: any) => {
        this.completo = 'error';
        console.log(JSON.stringify(err));
        this.presentarErrorToast();
      }
      );
    }, 1500);
  }

  abrirContactos() {
    this.router.navigateByUrl('/importar-contactos');
  }

  recargarAmigos() {

    this.completo = 'false';
    // console.log("los speakers antes: " + this.completo);
   // console.log(this.speakers);
   this.URLavatar = urls.URLimagen;
   this.registro.VerAmigos(this.valor.identificacion, this.ubicacion.latitud, this.ubicacion.longitud);
   this.registro.verMisTarjetasRecibidas(this.valor.identificacion);
   this.verificarConexion();
  }

  verificarConexion() {
    setTimeout(() => {
      if (this.registro.Amigos.amigos !== undefined) {
        this.speakers = this.registro.Amigos.amigos;
      }
      console.log(this.speakers.length);
      if (this.speakers.length === 0 && this.speakers === undefined) {
        this.completo = 'zero';
      }
      if (this.registro.CargaAmigosCompleta === true) {
        this.completo = 'true';
      }
      if (this.registro.CargaAmigosCompleta === false) {
        this.completo = 'error';
      }
    }, 1500);
  }

  goToSpeakerTwitter(speaker: any) {
    this.inAppBrowser.create(
      `https://twitter.com/${speaker.twitter}`,
      '_blank'
    );
  }

  async openSpeakerShare(speaker: any) {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Share ' + speaker.name,
      buttons: [
        {
          text: 'Copy Link',
          handler: () => {
            console.log(
              'Copy link clicked on https://twitter.com/' + speaker.twitter
            );
            if (
              (window as any)['cordova'] &&
              (window as any)['cordova'].plugins.clipboard
            ) {
              (window as any)['cordova'].plugins.clipboard.copy(
                'https://twitter.com/' + speaker.twitter
              );
            }
          }
        },
        {
          text: 'Share via ...'
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });

    await actionSheet.present();
  }

  async openContact(speaker: any) {
    const mode = 'ios'; // this.config.get('mode');

    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Contact ' + speaker.Nombre,
      buttons: [
        {
          text: `Email ( ${speaker.Email} )`,
          icon: mode !== 'ios' ? 'mail' : null,
          handler: () => {
            window.open('mailto:' + speaker.Email);
          }
        },
        {
          text: `Call ( ${speaker.Telefono_Celular} )`,
          icon: mode !== 'ios' ? 'call' : null,
          handler: () => {
            window.open('tel:' + speaker.Telefono_Celular);
          }
        }
      ]
    });

    await actionSheet.present();
  }

  async openChat(speaker: string, ID: string, photo: string) {
    console.log('VAMOS A CREAR LA VENTAN PARA EL CHAT CON ' + speaker);
    const modal = await this.modalCtrl.create({
      component: ChatPage,
      componentProps: {
        speakerName: speaker,
        speakerID: ID,
        speakerPhoto: photo
      }
    });
    await modal.present();

  }

  doRefrescar(event) {
    console.log('Empieza a refrescar todo');

    setTimeout(() => {
      console.log('Se refrescó correctamente');
      this.recargarAmigos();
      event.target.complete();
    }, 2000);
  }
}
