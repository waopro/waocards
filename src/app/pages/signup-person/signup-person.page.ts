import { Component, OnInit, AfterContentInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { UserOptions } from '../../interfaces/user-options';

import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';
import { EnterpriseOptions } from '../../interfaces/enterprise-options';
import { Paises } from '../../providers/paises';
import { ToastController, AlertController, LoadingController } from '@ionic/angular';
import { WelcomePage } from '../welcome/welcome.page';
@Component({
  selector: 'signup-person',
  templateUrl: './signup-person.page.html',
  styleUrls: ['./signup-person.page.scss'],
})
export class SignupPersonPage implements OnInit, AfterContentInit {
  customActionSheetOptions: any = {
    header: 'Códigos de acceso',
    subHeader: 'Seleccione el código de su país'
  };
  
  signup: UserOptions = {
    username: '',
    id_user: '',
    tel_cel: '',
    email: '',
    bornDate: '',
    password: ''
    
  };
  signupenterprise: EnterpriseOptions = {
    enterprisename: '',
    nit_enterprise: '',
    Enterprise_tel_cel: '',
    Enterpriseemail: '',
    ciiu: ''
  };
  segment = 'Usuario';
  codigoseleccionado = false;
  numerocodigo: any;
  submitted: boolean = false;
  emailInvalido: boolean = false;
  FaltaTelefono: boolean = false;
  FaltaNombre: boolean = false;
  FaltaID: boolean = false;
  faltaDOB: boolean = false;
  keybaordShowSub: any;
  keybaordHideSub: any;

  constructor(
    public router: Router,
    public registro: RegistroUsuariosService,
    public valid: Valores,
    public toastController: ToastController,
    public paises: Paises,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public welcomepage: WelcomePage
  ) { }

  ngOnInit() {
  }

  ngAfterContentInit() {
  }

  updateSelectedValue(evt) {
    if (evt) {
    this.numerocodigo = evt.detail.value;
      this.codigoseleccionado = true ;
    }

  }

  cambiarTipoCodigo() {
    this.codigoseleccionado = false;
  }

  soloLetra(e) {
    // Se captura el código ASCII de la tecla presionada en la variable local "key"
    let key: number = e.keyCode || e.which;
    // console.log(key);

    // Se evalua si el código ASCII capturado corresponde a las letras del abcedario en mayúsculas o en minúsculas
    if (
      (key > 64 && key < 91) ||
      (key > 96 && key < 123) ||
      key == 32 ||
      (key > 127 && key < 155) ||
      (key > 159 && key < 166) ||
      (key > 223 && key < 238)
    ) {
      return true; // Se permite la escritura en el ion-input
    }
    return false; // Se bloquea la escritura en el ion-input
  }

  async presentarToastError() {
    const toast = await this.toastController.create({
      message: 'Ya existe un usuario registrado',
      showCloseButton: true,
      position: 'bottom',
      closeButtonText: 'Listo',
      color: 'danger'
    });
    toast.present();
  }

  async mostrarAlertConfirmacion() {
    console.log('se dispara correcto');
    const alert = await this.alertController.create({
      header: 'Confirmar',
      message: 'Se enviará a <strong>' + this.signup.email +
        '</strong> un pin de confirmación para poder continuar con el registro. ' +
        'También se verificará si los datos ingresados ya existen en el sistema',
      buttons: [{
        text: 'Confirmar',
        role: 'confirmar',
        cssClass: 'secondary',
        handler: (blah) => {
          // console.log('Confirm Cancel: blah');
          this.presentLoadingWithOptions(3000);
          setTimeout(() => {
            this.preregistro();
          }, 1000);
          
        }
      }]
    });
    alert.present();
  }

  async presentLoadingWithOptions(tiempo) {
    const loading = await this.loadingController.create({
      duration: tiempo,
      message: 'Un momento, por favor...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  preregistro() {
    // SI SE LLEGA AQUÍ ES PORQUE LA VALIDACIÓN DE DATOS ESÁ COMPLETA Y SE PROCEDE A PREREGISTRAR AL USUARIO
    this.registro.preregistrar(
      this.signup.username,
      this.numerocodigo + this.signup.tel_cel,
      this.signup.email,
      this.signup.bornDate,
      this.signup.id_user
    ).subscribe((rta) => {
      console.log(rta);

      if (rta['mensaje'] !== 'Ya existe un usuario con ese número de identificación') {
        this.valid.guardarid(this.signup.id_user);
        this.valid.correo = this.signup.email;
        this.router.navigateByUrl('/signup-confirmation');
      }
      else {
        this.presentarToastError();
      }
    });
  }

  onSignup(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      let indiceArroba = this.signup.email.indexOf('@');
      if (
        indiceArroba == -1 ||
        indiceArroba == 0 ||
        indiceArroba == this.signup.email.length - 1
      ) {
        this.emailInvalido = true;
        return;
      } else {
        let indiceOtroArroba = this.signup.email.indexOf('@', indiceArroba + 1);
        if (indiceOtroArroba === -1) {
          this.emailInvalido = false;
        } else {
          this.emailInvalido = true;
          return;
        }
      }
      this.mostrarAlertConfirmacion();
    }
  }
}


