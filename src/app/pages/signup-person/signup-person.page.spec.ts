import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupPersonPage } from './signup-person.page';

describe('SignupPersonPage', () => {
  let component: SignupPersonPage;
  let fixture: ComponentFixture<SignupPersonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupPersonPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupPersonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
