import { Component, OnInit } from '@angular/core';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import * as urls from '../../providers/urls.servicios';
import { DetallePersonaEncontradaPage } from '../detalle-persona-encontrada/detalle-persona-encontrada.page';
import { ModalController } from '@ionic/angular';
import { Valores } from '../../providers/valores';

@Component({
  selector: 'solicitudes-pendientes',
  templateUrl: './solicitudes-pendientes.page.html',
  styleUrls: ['./solicitudes-pendientes.page.scss'],
})
export class SolicitudesPendientesPage implements OnInit {
  URLavatar = urls.URLimagen;
  public solicitudes: any;
  cantidad: any;
  LongitudComunes = 0;
  CargandoConfirmacion = 'false';
  CargandoRechazo = 'false';

  constructor(
              public modalController: ModalController,
              public registro: RegistroUsuariosService,
              public valor: Valores
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.solicitudes = this.registro.solicitudesPendientes.solicitudes;
    this.cantidad = this.registro.solicitudesPendientes.cantidad;
    this.CargandoConfirmacion = 'false';
    this.CargandoRechazo = 'false';
   /*  for (let l of this.solicitudes) {
     this.LongitudComunes++;
    } */
    // this.LongitudComunes = this.registro.solicitudesPendientes.solicitudes.AmigosComunes.length;
    // console.log(JSON.stringify(this.registro.solicitudesPendientes.solicitudes.AmigosComunes));
    console.log(this.solicitudes.AmigosComunes);
  }

  volveraCargar() {
  // this.registro.RevisarSolicitudes(this.valor.identificacion);
  // this.solicitudes.pop();
  setTimeout(() => {
  //  this.solicitudes = this.registro.solicitudesPendientes.solicitudes;
  //  this.cantidad = this.registro.solicitudesPendientes.cantidad;
   // this.ionViewDidEnter();
    this.CargandoConfirmacion = 'false';
    this.CargandoRechazo = 'false';
  }, 600);
  }

  aceptarSolicitud(Identificacion) {
    console.log('Aceptó la solicitud de: ' + Identificacion);
    this.CargandoConfirmacion = 'cargando';
    setTimeout(() => {
      this.CargandoConfirmacion = 'correcto';
      this.registro.ConfirmarSolicitud(this.valor.identificacion, Identificacion);
      this.volveraCargar();
    }, 1300);
  }

  cancelarSolicitud(Identificacion) {
    console.log('Rechazó la solicitud de: ' + Identificacion);
    this.CargandoRechazo = 'cargando';
    setTimeout(() => {
      this.CargandoRechazo = 'correcto';
       this.registro.RechazarSolicitud(this.valor.identificacion, Identificacion);
      this.volveraCargar();
    }, 1300);
  }

  async verPerfil(Identificacion) {
    console.log('Entra al perfil async de: ' + Identificacion);
    const modal = await this.modalController.create({
      component: DetallePersonaEncontradaPage,
      componentProps: {
        IDEncontrado: Identificacion,
        solicitante: 'true'
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
  }


}
