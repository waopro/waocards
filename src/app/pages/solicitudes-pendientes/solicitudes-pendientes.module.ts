import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SolicitudesPendientesPage } from './solicitudes-pendientes.page';

const routes: Routes = [
  {
    path: '',
    component: SolicitudesPendientesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SolicitudesPendientesPage]
})
export class SolicitudesPendientesPageModule {}
