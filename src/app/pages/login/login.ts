import { Component, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { UserData } from '../../providers/user-data';

import { UserOptions } from '../../interfaces/user-options';
import { Location } from '@angular/common';
import { IngresoPlataformaService} from '../../providers/ingreso-plataforma.service';
import { LoginValidation } from '../../interfaces/login-validation';
import { Valores } from '../../providers/valores';
import { AppComponent } from '../../app.component';
import { AlertController } from '@ionic/angular';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';



@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  styleUrls: ['./login.scss'],
})
export class LoginPage {
  login: UserOptions = {
    username: '',
    password: '',
    tel_cel: '',
    email: '',
    bornDate: '',
    id_user: ''
   
  };
  submitted = false;
  accesoDenegado: boolean = false;
  UUID: any;
  // public answer$: Observable<LoginValidation>;

  constructor(
    public userData: UserData,
    public router: Router,
    public loginAccount: IngresoPlataformaService,
    public valor: Valores,
    public componenteApp: AppComponent,
    public location: Location,
    public alertController: AlertController,
    public registro: RegistroUsuariosService
  ) {
  }

  // ventana de verificacion
  async presentAlert() {
    console.log('se dispara correcto');
    const alert = await this.alertController.create({
      header: '¡Registro Correcto!',
      message: 'Se han registrado correctamente sus datos, puede iniciar sesión',
      buttons: ['Continuar']
    });
    alert.present();
  }

  recuperarContr() {
    this.router.navigateByUrl('/recuperar-contrasena');
  }

  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {

        this.loginAccount.validarDatos(this.login.username, this.login.password, this.valor.UUID)
        .subscribe((rta: LoginValidation) => {

          console.log(rta);

          if (!rta.error) {
            
            this.valor.identificacion = rta.id_usuario;
            this.userData.setUserID(rta.id_usuario);
            setTimeout(() => {
              this.registro.contrasenaCambiada('0', rta.id_usuario);
            }, 200);
            this.componenteApp.sesiones();
            this.userData.login();
            // this.userData.login(this.login.username);
            this.componenteApp.presentLoading();
            setTimeout(() => {
              this.router.navigateByUrl('/app/tabs/find-people');
            }, 1500);
          } else {
            this.accesoDenegado = true;
            console.log('acceso denegado');
          }

        });
    }
  }
}
