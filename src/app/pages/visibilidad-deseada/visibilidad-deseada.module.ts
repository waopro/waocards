import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VisibilidadDeseadaPage } from './visibilidad-deseada.page';

const routes: Routes = [
  {
    path: '',
    component: VisibilidadDeseadaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VisibilidadDeseadaPage]
})
export class VisibilidadDeseadaPageModule {}
