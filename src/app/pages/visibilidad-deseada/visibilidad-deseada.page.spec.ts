import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisibilidadDeseadaPage } from './visibilidad-deseada.page';

describe('VisibilidadDeseadaPage', () => {
  let component: VisibilidadDeseadaPage;
  let fixture: ComponentFixture<VisibilidadDeseadaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisibilidadDeseadaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisibilidadDeseadaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
