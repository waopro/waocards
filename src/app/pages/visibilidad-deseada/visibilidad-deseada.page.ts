import { Component, OnInit, Input } from '@angular/core';
import { ModalController, ToastController, AlertController } from '@ionic/angular';
// import { stringify } from '@angular/compiler/src/util';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';
@Component({
  selector: 'visibilidad-deseada',
  templateUrl: './visibilidad-deseada.page.html',
  styleUrls: ['./visibilidad-deseada.page.scss'],
})
export class VisibilidadDeseadaPage implements OnInit {
  @Input() modoPreferencias: string;
  // aqui se guardan las variables seleccionadas

  // elementosSeleccionadosB: number [] = [0];
  // seleccionConstruccion: string[] = [''];
  // seleccionDistribucion: string[] = [''];
  // seleccionIndustria: string[] = [''];
  // seleccionEstetica: string[] = [''];
  // seleccionAlimentacion: string[] = [''];
  // seleccionAgricola: string[] = ['']; 
  // seleccionServicios: string[] = [''];
  // seleccionFinal: string[] = [''];

  textoMostradoEjecucion: string = '';
  tituloEncabezado: string = '';
  seleccionFinalString: string;
  datosPreferencias: any = [{
    Preferencias: '',
    CampoAccion: ''
  }];
  datosSeparados: string[] = [];

  datosComprobarCategorias: string[] = [''];
  datosDivCategoria: number = 9;  // el 9 es para mostrar toda la informacion
  categoriasSeleccionadasUser: string = '';
  categorias: any[];
  areaTexto: string = '';
  banderaBloqueoBotonEnvio: boolean = false;
  iconosMuestra: any[] =
    

    [
      ['../../../assets/icons/arrow-back.svg'],
      ['../../../assets/icons/arrow-loop-outline.svg'],
      ['../../../assets/icons/arrow-move-outline.svg'],
      ['../../../assets/icons/arrow-up-outline.svg'],
      ['../../../assets/icons/attachment.svg'],
      ['../../../assets/icons/beer.svg'],
      ['../../../assets/icons/calculator.svg']
    ];

  variableAuxiliarSuperCategoria: boolean = false;

  // items = [
  //   {
  //     'tipo': 'Construcción',
  //     'elementos': [
  //       { 'item': 'co01', 'nombre': 'Fabricante de material' },
  //       { 'item': 'co02', 'nombre': 'Constructor ' },
  //       { 'item': 'co03', 'nombre': 'Arquitectos ' },
  //       { 'item': 'co04', 'nombre': 'Decoración-interiores' },
  //       { 'item': 'co05', 'nombre': 'Reformas ' },
  //       { 'item': 'co06', 'nombre': 'Ferretería' },
  //       { 'item': 'co07', 'nombre': 'Mina' },
  //       { 'item': 'co08', 'nombre': 'Eléctricos - equipo eléctrico' },
  //       { 'item': 'co09', 'nombre': 'Energía - iluminación ' },
  //       { 'item': 'co10', 'nombre': 'Maquinaria - equipamiento' },
  //       { 'item': 'co11', 'nombre': 'Minerales y metalurgia ' },
  //       { 'item': 'co12', 'nombre': 'Seguridad y protección epp' }
  //     ]
  //   },
  //   {
  //     'tipo': 'Distribución y comercio',
  //     'elementos': [
  //       { 'item': 'di01', 'nombre': 'Tecnología ' },
  //       { 'item': 'di02', 'nombre': 'Hogar y jardín  ' },
  //       { 'item': 'di03', 'nombre': 'Automotriz' },
  //       { 'item': 'di04', 'nombre': 'Didácticos, juguetes juegos ' },
  //       { 'item': 'di05', 'nombre': 'Papelería - miscelánea' },
  //       { 'item': 'di06', 'nombre': 'Relojería joyería ' },
  //       { 'item': 'di07', 'nombre': 'Accesorios de moda' },
  //       { 'item': 'di08', 'nombre': 'Sonido e imagen ' },
  //       { 'item': 'di09', 'nombre': 'Publicidad - merchandising - digital- impresos' }
  //     ]
  //   },
  //   {
  //     'tipo': 'Industria',
  //     'elementos': [
  //       { 'item': 'in01', 'nombre': 'envasado' },
  //       { 'item': 'in02', 'nombre': 'Fabricación de maquinaria ' },
  //       { 'item': 'in03', 'nombre': 'Instalaciones industriales' },
  //       { 'item': 'in04', 'nombre': 'Industria pesada ' },
  //       { 'item': 'in05', 'nombre': 'Textiles y cueros ' },
  //       { 'item': 'in06', 'nombre': 'Calzado ' },
  //       { 'item': 'in07', 'nombre': 'Mobiliario' },
  //       { 'item': 'in08', 'nombre': 'Electrodomésticos ' },
  //       { 'item': 'in09', 'nombre': 'Artículos de hogar' },
  //       { 'item': 'in10', 'nombre': 'Plástico y cauchos ' },
  //       { 'item': 'in11', 'nombre': 'Prendas maletas y bolsos ' }
  //     ],
  //   },
  //   {
  //     'tipo': 'Estética y Salud',
  //     'elementos': [
  //       { 'item': 'es01', 'nombre': 'Peluquerías' },
  //       { 'item': 'es02', 'nombre': 'Centros de belleza' },
  //       { 'item': 'es03', 'nombre': 'Perfumería ' },
  //       { 'item': 'es04', 'nombre': 'Farmacia' },
  //       { 'item': 'es05', 'nombre': 'Tienda de estética ' },
  //       { 'item': 'es06', 'nombre': 'Herboristería ' },
  //       { 'item': 'es07', 'nombre': 'Comercio de material medico ' },
  //       { 'item': 'es08', 'nombre': 'Equipos médicos ' },
  //       { 'item': 'es09', 'nombre': 'Deportes' },
  //       { 'item': 'es10', 'nombre': 'Instrumentos de medición y análisis' },
  //       { 'item': 'es11', 'nombre': 'Medicina y salud ' }
  //     ],
  //   },
  //   {

  //     'tipo': 'Alimentación',
  //     'elementos': [
  //       { 'item': 'al01', 'nombre': 'Tienda de alimentación ' },
  //       { 'item': 'al02', 'nombre': 'Industria ' },
  //       { 'item': 'al03', 'nombre': 'Distribuidor ' },
  //       { 'item': 'al04', 'nombre': 'Catering' },
  //       { 'item': 'al05', 'nombre': 'Suministros de alimentación y eventos ' },
  //       { 'item': 'al06', 'nombre': 'Servicios para la alimentación y eventos ' }
  //     ],
  //   },
  //   {
  //     'tipo': 'Agrícola',
  //     'elementos': [
  //       { 'item': 'ag01', 'nombre': 'Explotación ganadera ' },
  //       { 'item': 'ag02', 'nombre': 'Granja - eco cultivos ' },
  //       { 'item': 'ag03', 'nombre': 'Viñedos' },
  //       { 'item': 'ag04', 'nombre': 'Explotación agrícola' },
  //       { 'item': 'ag05', 'nombre': 'Producto cárnico ' },
  //       { 'item': 'ag06', 'nombre': 'Piscifactoría' },
  //       { 'item': 'ag07', 'nombre': 'Granja apícola ' },
  //       { 'item': 'ag08', 'nombre': 'Explotación forestal' },
  //       { 'item': 'ag09', 'nombre': 'Negocio pesquero' }
  //     ],
  //   },
  //   {
  //     'tipo': 'Servicios para empresas',
  //     'elementos': [
  //       { 'item': 'se01', 'nombre': 'Bufete de abogados ' },
  //       { 'item': 'se02', 'nombre': 'Comunicación de publicidad' },
  //       { 'item': 'se03', 'nombre': 'Organización de eventos ' },
  //       { 'item': 'se04', 'nombre': 'Consultorías y outsorcing' },
  //       { 'item': 'se05', 'nombre': 'Limpieza y mantenimientos ' },
  //       { 'item': 'se06', 'nombre': 'Suministro para comercio y oficina ' },
  //       { 'item': 'se07', 'nombre': 'Servicios de telecomunicaciones' },
  //       { 'item': 'se08', 'nombre': 'Editorial imprenta ' },
  //       { 'item': 'se09', 'nombre': 'Mecánico - taller ' },
  //       { 'item': 'se10', 'nombre': 'Logística y transporte ' }
  //     ]
  //   }
  // ];



  constructor(
    private modalCtrl: ModalController,
    private registroUsuariosService: RegistroUsuariosService,
    public toastController: ToastController,
    private valor: Valores,
    private alertController: AlertController
  ) { }




  ngOnInit() {
    this.encabezadoAccionEjecutar();
    this.registroUsuariosService.consultarCamposAccionYCodigosUsuario(this.valor.identificacion).subscribe(respuesta => {
      this.datosPreferencias = respuesta;
      this.categorias = this.datosPreferencias.items.categorias;
      this.categoriasSeleccionadasUser = this.datosPreferencias.items.categoriasUsuario;
      console.log('esto es lo que obtengo', this.datosPreferencias.items.categorias, ' al pedir datos ', this.categoriasSeleccionadasUser);

      this.datosComprobarCategorias = this.categoriasSeleccionadasUser.split(',');

    }, (error: any) => {
      console.log(error);
    });

  }



  // esto mustra los textos en la parte superior  dependiendo de la forma de accesar desde count
  encabezadoAccionEjecutar() {

    // if (this.modoPreferencias == 'visibilidadDeseada') {
    //   this.textoMostradoEjecucion = 'Marque las categorias para las cuales desea ser visible';
    //   this.tituloEncabezado = 'Visibilidad deseada';
    // }
    if (this.modoPreferencias == 'campoAccion') {
      this.textoMostradoEjecucion = 'Marque su campo de acción en las categorias listadas abajo';
      this.tituloEncabezado = 'Campo de acción';
    }
    // console.log('usted presion el boton de ', this.tituloEncabezado);
  }

  // esta funcion recoge los datos para mostrarlos manejar una u otra interfaz
  // marcarItemsPreseleccionados() {

  //   if (this.modoPreferencias == 'visibilidadDeseada') {
  //     this.datosSeparados = this.datosPreferencias.res.Preferencias.split(',');

  //   } else {
  //     this.datosSeparados = this.datosPreferencias.res.CampoAccion.split(',');
  //     console.log('efectivamente pasa por este lugar');
  //   }
  //   this.datosSeparados = this.datosSeparados.filter(Boolean);
  //   console.log('por aqui esto es datosSeparados: ', this.datosSeparados);

  // }



  // en estemetodo se guardan los elementos que vengan del servidor en sus respectivas variables

  // ponerDatosEnVariables() {
  //   this.limpiarVariablesAuxiliares();
  //   this.datosPreferencias
  //   for (let datos of this.datosSeparados) {

  //     switch (datos.charAt(0) + datos.charAt(1)) {
  //       case 'co':
  //         this.seleccionConstruccion.push(datos);
  //         break;
  //       case 'di':
  //         this.seleccionDistribucion.push(datos);
  //         break;
  //       case 'in':
  //         this.seleccionIndustria.push(datos);
  //         break;
  //       case 'es':
  //         this.seleccionEstetica.push(datos);
  //         break;
  //       case 'al':
  //         this.seleccionAlimentacion.push(datos);
  //         break;
  //       case 'ag':
  //         this.seleccionAgricola.push(datos);
  //         break;
  //       case 'se':
  //         this.seleccionServicios.push(datos);
  //         break;
  //       default:
  //         break;
  //     }
  //   }

  //   // this.seleccionFinal = (this.seleccionConstruccion.concat(this.seleccionDistribucion, 
  // this.seleccionIndustria, this.seleccionEstetica,
  //   //   this.seleccionAlimentacion, this.seleccionAlimentacion, this.seleccionAgricola, this.seleccionServicios));

  //   //   console.log('estos son los datos enviados0 ', this.seleccionServicios, 'otrasconstrucion ', this.seleccionConstruccion );
  // }



  // esta funcion es usada para separar por categorias y guardar la informacion en la vaariable correcta
  // al escoger cualquier opcion en el ion select

  // cambioItemSeleccionado($event, i: any) {
  //   let variableAuxiliarElementoSeleccionado: string[] = $event.target.value;

  //   if (variableAuxiliarElementoSeleccionado === undefined || variableAuxiliarElementoSeleccionado.length == 0) {
  //     variableAuxiliarElementoSeleccionado = [''];
  //   }

  //   switch (i.tipo.charAt(0) + i.tipo.charAt(1)) {
  //     case 'Co':
  //       this.seleccionConstruccion = variableAuxiliarElementoSeleccionado;
  //       break;
  //     case 'Di':
  //       this.seleccionDistribucion = variableAuxiliarElementoSeleccionado;
  //       break;
  //     case 'In':
  //       this.seleccionIndustria = variableAuxiliarElementoSeleccionado;
  //       break;
  //     case 'Es':
  //       this.seleccionEstetica = variableAuxiliarElementoSeleccionado;
  //       break;
  //     case 'Al':
  //       this.seleccionAlimentacion = variableAuxiliarElementoSeleccionado;
  //       break;
  //     case 'Ag':
  //       this.seleccionAgricola = variableAuxiliarElementoSeleccionado;
  //       break;
  //     case 'Se':
  //       this.seleccionServicios = variableAuxiliarElementoSeleccionado;
  //       break;
  //     default:
  //       break;
  //   }


  // }



  // estemetodo se llama al presionar el boton y se concatena toda la informacion
  // para enviarla al servicio

  guardarPreferenciasVisibilidadUsuario() {


    this.registroUsuariosService.guardarPreferenciasPreferenciasCampoAccion(this.categoriasSeleccionadasUser, this.valor.identificacion)
      .subscribe(respuesta => {
        this.salidaPage(respuesta);
      }, (error: any) => {
        console.log(error);
      });

  }

  salidaPage(operacionCorrrecta: any) {

    if (operacionCorrrecta.mensaje == 'Correcto') {
      this.presentToastWithOptions('guardado exitoso', 'correcto', true, 1000);
      this.dismiss();
    } else {
      this.presentToastWithOptions('ha ocurrido un error, inténtalo mas tarde', 'error', true, 1000);
    }
  }



  // // se limpian las variables auxiliares
  // limpiarVariablesAuxiliares() {
  //   this.seleccionConstruccion = [];
  //   this.seleccionDistribucion = [];
  //   this.seleccionIndustria = [];
  //   this.seleccionEstetica = [];
  //   this.seleccionAlimentacion = [];
  //   this.seleccionAgricola = [];
  //   this.seleccionServicios = [];
  //   this.seleccionFinal = [];
  // }




  // esto se usa para mostrar elementos seleccionados o no en la opcion ion selected
  // banderaComparacionA(itemcomparacion) {
  //   // if (itemcomparacion[2] ==  '0' && itemcomparacion[3] ==  '0' ) {
  //   //     console.log('si puede funcional', itemcomparacion);
  //   // }
  //   for (let comprobarItem of this.datosSeparados) {
  //     if (comprobarItem == itemcomparacion) {
  //       return true;
  //     }
  //   }
  //   return false;
  // }



  // metodo para cerrar el modal correctamente

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  async presentToastWithOptions(mensaje: string, tipo: string, cerrar: boolean, tiempo: number) {
    // solo tomamos de tipo si es error o correcto
    // cerrar es para que se cierre solo poniendo true
    // tiempo el tiempo deseado de visibilidad en pantalla
    if (tipo == 'error') {
      tipo = 'danger';
    } else {
      tipo = 'success';
    }
    let posicionlugar;
    if (tipo == 'danger') {
      posicionlugar = 'bottom';
    } else {
      posicionlugar = 'top';
    }
    const toast = await this.toastController.create({
      message: mensaje,
      showCloseButton: cerrar,
      position: posicionlugar,
      closeButtonText: 'Listo',
      color: tipo,
      duration: tiempo
    });
    toast.present();
  }

  tipos(tipoClick) {
    this.datosDivCategoria = tipoClick;
  }

  clickElementoChip(nombre: string) {
    // console.log(nombre);
    this.datosComprobarCategorias = this.categoriasSeleccionadasUser.split(',');
    this.variableAuxiliarSuperCategoria = false;
    this.opcionesSeleccion(nombre);






  }

  deficinioncolor(nombre) {
    const camposPreseleccionados = this.datosComprobarCategorias.map(function (item) {
      return item.substring(0, 4);
    });
    if (camposPreseleccionados.includes(nombre)) {
      return 'primary';
    } else {
      return 'success';
    }

  }


  EnviarComentariosNuevaCategoria() {
    // aun no se ha implementad debe ser usada
    // areaTexto es una variable
    const Asunto: string = 'Nuevo Campo de Acción:';
    if ((this.banderaBloqueoBotonEnvio == false) && (this.areaTexto != '')) {
      this.banderaBloqueoBotonEnvio = true;
      this.registroUsuariosService.envioCorreoSugerenciaCampoAccion(Asunto, this.areaTexto)
        .subscribe(respuesta => {
          this.salidaPage2(respuesta);
        }, (error: any) => {
          console.log(error);
        });
    } else {
      this.presentToastWithOptions('lo sentimos, su sugerencia no puede ser procesada.', 'error', true, 3000);
    }
  }
  salidaPage2(operacionCorrrecta: any) {

    if (operacionCorrrecta.error == false) {
      this.presentToastWithOptions('guardado exitoso', 'correcto', true, 2000);
      this.areaTexto = '';
    } else {
      this.presentToastWithOptions('ha ocurrido un error, inténtalo mas tarde', 'error', true, 2000);
    }
  }


  async opcionesSeleccion(nombre: string) {
    const alert = await this.alertController.create({
      header: 'subCategorias',
      inputs: [
        {
          name: 'Comprador',
          type: 'checkbox',
          label: 'Comprador',
          value: 'C'
        },

        {
          name: 'Vendedor',
          type: 'checkbox',
          label: 'Vendedor',
          value: 'V'
        },

        {
          name: 'Importador',
          type: 'checkbox',
          label: 'Importador',
          value: 'I'
        },

        {
          name: 'Fabricante',
          type: 'checkbox',
          label: 'Fabricante',
          value: 'F'
        },

        {
          name: 'Distribuidor',
          type: 'checkbox',
          label: 'Distribuidor',
          value: 'D'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancelar',
          cssClass: 'secondary',
          handler: (data) => {
            console.log('Confirm Cancel', data);
            this.variableAuxiliarSuperCategoria = false;
            this.cambioEstadoChip(nombre, data.toString());
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log('Confirm Ok', data);
            this.variableAuxiliarSuperCategoria = true;
            this.cambioEstadoChip(nombre, data.toString());
          }
        }
      ]
    });

    await alert.present();
  }

  cambioEstadoChip(nombre: string, datos: string) {
    // se debe analizar por las 4 primeras letras
    // en nombre llega la categoria completa
    // en datos llega las subcategorias IVCD...

    datos = datos.split(',').join('');

    // si se clickea cancelar no suscede nada
    if (this.variableAuxiliarSuperCategoria) {

      // se genera un sub arreglo para comprobar donde estan los elementos seleccionados y borrarlos, o reemplazarlos
      const camposPreseleccionados = this.datosComprobarCategorias.map(function (item) {
        return item.substring(0, 4);
      });

      if (camposPreseleccionados.includes(nombre)) {

        const filtradoCamposRepetidos = this.datosComprobarCategorias.filter(function (item, index) {
          if (nombre != camposPreseleccionados[index]) {
            return item;
          }
        });
        if (datos != '') {
          filtradoCamposRepetidos.push(nombre + datos);
        }
        // let index = this.datosComprobarCategorias.indexOf(nombre);
        // this.datosComprobarCategorias.splice(index, 1); // eliminamos el elemento encontrado
        this.datosComprobarCategorias = filtradoCamposRepetidos;
      } else {
        // se incluye el elemento
        if (datos != '') {
        this.datosComprobarCategorias.push(nombre + datos);
        }
      }
      this.variableAuxiliarSuperCategoria = false;
      this.datosComprobarCategorias = this.datosComprobarCategorias.filter(Boolean);
      this.categoriasSeleccionadasUser = this.datosComprobarCategorias.toString();

      console.log(this.categoriasSeleccionadasUser);
    }

  }

}
