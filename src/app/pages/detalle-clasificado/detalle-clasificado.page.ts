import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Clasificados } from '../../providers/clasificados';

import * as urls from '../../providers/urls.servicios';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';

@Component({
  selector: 'detalle-clasificado',
  templateUrl: './detalle-clasificado.page.html',
  styleUrls: ['./detalle-clasificado.page.scss'],
})
export class DetalleClasificadoPage implements OnInit, AfterContentInit {

  URLAvatar: string = urls.URLimgClasificado;
  DetalleClasificadoData: any = [];
  URLavatar: any;
  completo = 'false';
  Color = 'ion-col3';
  constructor(
    private route: ActivatedRoute,
    public clasificado: Clasificados,
    public registro: RegistroUsuariosService,
    public valor: Valores
  ) { }

  ngOnInit() {
  }

  cargarAmigosComun() {
  this.registro.CargaAmigosComunCompleta = false;
  this.URLavatar = urls.URLimagen;
  setTimeout(() => {
    this.registro.VerAmigosComun(this.valor.identificacion, this.DetalleClasificadoData.Identificacion);
  }, 2500);
  }

  llamar() {
    console.log('llamando a: ' + this.DetalleClasificadoData.Contacto);
    window.open('tel:' + this.DetalleClasificadoData.Contacto);
  }

  ngAfterContentInit() {
    setTimeout(() => {
      this.clasificado.load().subscribe((clasificado: any['records']) => {
        const speakerId = this.route.snapshot.paramMap.get('ID');
        console.log('Speakerid es: ' + speakerId);
        setTimeout(() => {
          for (const i of clasificado.records) {
            if (i.ID == speakerId) {
              this.DetalleClasificadoData = i;
              console.log('clasificado: ' + clasificado.records);
              break;
            }
          }
          console.log(this.DetalleClasificadoData);
          this.completo = 'true';
          this.cargarAmigosComun();
        }, 500);
        
     
      }, (err: any) => {
        console.log(JSON.stringify(err));
        this.completo = 'error';
      }
      );
    }, 500);
  }

}
