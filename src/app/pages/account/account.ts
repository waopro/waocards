import { AfterViewInit, Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController, ActionSheetController, ModalController, LoadingController } from '@ionic/angular';

import { AlertController } from '@ionic/angular';

import { UserData } from '../../providers/user-data';
import { Valores } from '../../providers/valores';
import * as urls from '../../providers/urls.servicios';
import { AppComponent } from '../../app.component';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';

import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { MiCVPage } from '../mi-cv/mi-cv.page';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { inflate } from 'zlib';
import { TranslateService } from '@ngx-translate/core';
import { VisibilidadDeseadaPage } from '../visibilidad-deseada/visibilidad-deseada.page';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';



@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
  styleUrls: ['./account.scss'],
})
export class AccountPage implements AfterViewInit {
  username: string;
  usr: any;
  URLavatar: any;
  fotografia: any;
  fototomada: boolean = false;
  editableCV = false;
  contrasena: any;
  visibilidad: boolean;
  codigoReferido: any;


  constructor(
    public alertCtrl: AlertController,
    public router: Router,
    public userData: UserData,
    public valor: Valores,
    public toastController: ToastController,
    public AppComponente: AppComponent,
    public registro: RegistroUsuariosService,
    public route: Router,
    public actionSheetController: ActionSheetController,
    private camera: Camera,
    private transfer: FileTransfer,
    public modalController: ModalController,
    public loadingController: LoadingController,
    private fileChooser: FileChooser,
    private _translate: TranslateService,
    private socialSharing: SocialSharing
  ) {
    this.URLavatar = urls.URLimagen; // URL de acceso a las imagenes
    this.traduccionCambioContrasena();
  }

  // Mauricio:  para ser usado despues de solicitudes nuevas  -> MERCADOFREE
  // tipoClases se usa para definir cuantas clases hay
  tipoCategoria: any[] = ['Información relevante', 'Ajustes adicionales', 'Cerrrar sesión'];
  traduccioncontrasena: string = 'Cambio de contraseña';
  traduccionCerraSesion: string = 'Cerrar Sesión';
  //  Mauricio: componenteA son los elementos que queremos mostrar en este componente
  // cClick :llama el metodo que deseamos que ejecute
  // texto: a desplegar si esta vacio debe tener traduccion
  // si tiene un campo vacio en traduccion no traduce
  // icon: muestra el icono que deseamos
  // categoria: se basa en el arreglo tipoCategoria
  // infAdicional: informacion a desplegar bajo el elemento
  componentesA: any[] = [
    
    {
      cClick: 'vermiCV()',
      texto: 'Administrar mi CV',
      icon: 'assets/icons/_ionicons_svg_md-document.svg',
      categoria: this.tipoCategoria[0],
      infAdicional: ''
    },
    {
      cClick: 'changePassword()',
      texto: this.traduccioncontrasena,
      icon: 'assets/icons/key-outline.svg',
      categoria: this.tipoCategoria[0],
      infAdicional: ''
    },
    {
      cClick: '',
      texto: 'Mi número de celular',
      icon: 'assets/icons/device-phone.svg',
      categoria: this.tipoCategoria[0],
      infAdicional: this.userData.datos.Telefono_Celular // this.userData.datos.Telefono_Celular
    },
     {
      cClick: 'copiarCodReferidos()',
      texto: 'Mi código para referidos',
      icon: 'assets/icons/mail.svg',
      categoria: this.tipoCategoria[0],
      infAdicional: this.registro.referidos.mensaje + ' | presione para más opciones'
    },
    {
      cClick: 'presentarCampoAccion()',
      texto: 'Segmentos/Campo de acción', // aqui se establece el campo de accion
      icon: 'assets/icons/eye.svg',
      categoria: this.tipoCategoria[1],
      infAdicional: ''
    },
    {
      cClick: 'logout()',
      texto: this.traduccionCerraSesion,
      icon: 'assets/icons/_ionicons_svg_md-power.svg',
      categoria: this.tipoCategoria[2],
      infAdicional: ''
    }

  ]; // adicionar funciones a direccionamientoFunciones para que funcione corerctamente

  // asi funciona los metodos dentro de la lista que redirecciona a la funcion necesaria
  //  SE repiten las funciones pero es la forma mas facil para dibujar las opciones
  direccionamientoFunciones(funcionUsada: string) {

    switch (funcionUsada) {
      case 'vermiCV()':
        this.vermiCV();
        break;
      case 'changePassword()':
        this.changePassword();
        break;
      case 'presentModalVisibilidadDeseada()':
        this.presentModalVisibilidadDeseada();
        break;
      case 'copiarCodReferidos()':
        this.copiarCodReferidos();
        break;
      case 'presentarCampoAccion()':
        this.presentarCampoAccion();
        break;
      case 'logout()':
        this.logout();
        break;

      default:
        break;

    }
  }
  // se debe tener en cuenta el orden de componentesA y nombrara  traduccionVariable[ INDICE DE ESE ARREGLO PARA QUE FUNCIONE] 
  // por el momento se encuentran los indices 1 y 4

  traduccionCambioContrasena() {
    this._translate.stream('PAGES.ACOUNT.CAMBIAR_CONTRASENA').subscribe((text: string) => {
      this.traduccioncontrasena = text;
    });
    this._translate.stream('PAGES.ACOUNT.CERRAR_SESION').subscribe((text: string) => {
      this.traduccionCerraSesion = text;
    });

  }

 async copiarCodReferidos() {
    // aqui se copia para el texto para enviar a otros lugares
    console.log('Referidos seleccionado');
   const actionSheet = await this.actionSheetController.create({
     header: 'Compartir con',
     buttons: [{
       text: 'Whatsapp',
       role: 'destructive',
       cssClass: 'colorWhatsapp',
       icon: 'logo-whatsapp',
       handler: () => {
         this.socialSharing.shareViaWhatsApp('Descargue ya WAOCards con este código y reciba muchos beneficios: *' +
                                              this.registro.referidos.mensaje + '* ',
                                             '', 'http://www.google.com/');
       }
     }, {
       text: 'Facebook',
       icon: 'logo-facebook',
       cssClass: 'colorFacebook',
       handler: () => {
         this.socialSharing.shareViaFacebook('Descargue ya WAOCards con este código y reciba muchos beneficios: '
                                              + this.registro.referidos.mensaje,
                                            '', 'http://www.google.com/');
       }
       }, {
         text: 'Correo',
         icon: 'mail',
         cssClass: 'colorWhatsapp',
         handler: () => {
           this.socialSharing.shareViaEmail('Descargue ya WAOCards con este código y reciba muchos beneficios: '
                                            + this.registro.referidos.mensaje,
                                            'Prueba WAOCards',
                                            ['']);
         }
       }
     , {
       text: 'Cancelar',
       icon: 'close',
       role: 'cancel',
       handler: () => {
         console.log('Cancel clicked');
       }
     }]
   });
   await actionSheet.present();
  }

  // Notificación de error de conexión al servidor
  async presentarErrorToast() {
    const toast = await this.toastController.create({
      message: 'No hay conexión al servidor, ¡Datos limitados!',
      showCloseButton: true,
      position: 'bottom',
      closeButtonText: 'Listo',
      color: 'danger'
    });
    toast.present();
  }

  ngAfterViewInit() {
    this.comprobar_visibilidad();
    this.codigoReferido = this.registro.referidos.mensaje;
    //this.registro.Registro_referidos(this.valor.identificacion);
  }

  ionViewDidEnter() {

  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: MiCVPage
    });
    return await modal.present();
  }

  verSolicitudes() {
    this.route.navigateByUrl('/solicitudes-pendientes');
  }

  async updatePicture() {
    console.log('Clicked to update picture');
    const actionSheet = await this.actionSheetController.create({
      header: 'Editar la foto de perfil',
      buttons: [{
        text: 'Tomar una foto',
        role: 'destructive',
        icon: 'camera',
        handler: () => {
          console.log('Camera clicked');
          this.tomarFoto();
        }
      }, {
        text: 'Subir una foto',
        icon: 'images',
        handler: () => {
          console.log('Subir clicked');
          this.GaleriaImagen();
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  tomarFoto() {
    let options: CameraOptions = {
      quality: 95,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
      allowEdit: true,
      targetWidth: 600,
      targetHeight: 600
    };

    this.camera.getPicture(options).then(
      imageData => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.fotografia = 'data:image/jpeg;base64,' + imageData;
        setTimeout(() => {
          this.SubirFoto();
          this.fototomada = true;
        }, 500);

      },
      err => {
        this.fototomada = false;
        // Handle error
      }
    );
  }

  GaleriaImagen() {
    let options: CameraOptions = {
      quality: 95,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: true,
      allowEdit: true,
      targetWidth: 600,
      targetHeight: 600
    };

    this.camera.getPicture(options).then(
      imageData => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.fotografia = 'data:image/jpeg;base64,' + imageData;
        setTimeout(() => {

          this.fototomada = true;
          this.SubirFoto();
        }, 500);
      },
      err => {
        // Handle error
        this.fototomada = false;
      }
    );
  }

  SubirFoto() {
    // create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();
    console.log('direccion: ' + urls.fotos + 'uploadPhoto.php');

    // option transfer
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: this.valor.identificacion + '.jpg',
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: 'image/jpeg',
      headers: {}
    };

    // file transfer action
    fileTransfer
      .upload(this.fotografia, urls.fotos + 'uploadPhoto.php', options)
      .then(
        data => {
          // alert('Correcto');
          this.userData.inicio();
        },
        err => {
          console.log(err);
          this.presentarToastFoto();
          // alert('Incorrecto');
        }
      );
  }

  // Present an alert with the current username populated
  // clicking OK will update the username and display it
  // clicking Cancel will close the alert and do nothing
  async changeUsername() {
    const alert = await this.alertCtrl.create({
      header: 'Cambiar mi nombre',
      buttons: [
        '',
        {
          text: 'Listo',
          handler: (data: any) => {
            this.userData.setUsername(data.username);
            this.getUsername();
            this.alertContrasena();
          }
        }
      ],
      inputs: [
        {
          type: 'text',
          name: 'username',
          value: this.username,
          placeholder: 'Nuevo nombre'
        }
      ]
    });
    await alert.present();
  }

  async alertContrasena() {
    const alert = await this.alertCtrl.create({
      header: 'Ingrese la contraseña',
      message: 'No hay duda que sea usted, pero es pertinente que ingrese la contraseña antes de realizar cambios a su cuenta',
      buttons: [
        '',
        {
          text: 'Listo',
          handler: (data: any) => {
            this.registro.comprobarContrasena(this.valor.identificacion, data.password);
            this.presentarComprobando();
            setTimeout(() => {
              this.verificarContrasena();
            }, 1000);

            // this.userData.setUsername(data.username);
            // this.getUsername();
          }
        }
      ],
      inputs: [
        {
          type: 'password',
          name: 'password',
          value: this.contrasena,
          placeholder: 'Contraseña'
        }
      ]
    });
    await alert.present();
  }

  verificarContrasena() {
    if (this.registro.Contrasena == false) {
      this.presentarToastContrasena();
      console.log('Incorrecto');
      return false;
    }
    if (this.registro.Contrasena == true) {
      console.log('Correcto');
      return true;
    }
  }
  async presentarComprobando() {
    const loading = await this.loadingController.create({
      duration: 1500,
      message: 'Un momento, por favor',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  async presentarToastContrasena() {
    const toast = await this.toastController.create({
      message: 'Contraseña incorrecta',
      position: 'bottom',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  async presentarToastFoto() {
    const toast = await this.toastController.create({
      message: 'Ocurrió un error al actualizar su foto de perfil',
      position: 'bottom',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  getUsername() {
    this.username = this.userData.datos.Nombre;
  }

  vermiCV() {
    this.presentModal();
  }
  // 1 cambio contraseña   comprueba si la constraseña es correcta
  async changePassword() {

    console.log('Clicked to change password');
    // toca ajustar para que quede en inglés
    // esto esta basado en alert de ionic

    const alertA = await this.alertCtrl.create({
      header: 'cambiar contraseña!',
      message: 'Por favor confirme su contraseña actual',
      inputs: [
        {
          name: 'contrasenaActual',
          type: 'password',
          placeholder: 'contraseña'
        }],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: (datos: any) => {
            // aqui se redirecciona para confirmar
            // contraseña actual
            // y dos campos de texto para validar la nueva contraseña
            // una vez confirmada la nueva contraseña la guardamos en el servidor
            this.registro.comprobarContrasena(this.valor.identificacion, datos.contrasenaActual)
              .subscribe((data: any) => {
                // this.verificarContrasena();

                this.presentarComprobando(); // muestra un cuadro de cargando

                setTimeout(() => {

                  console.log(data.mensaje);
                  // accedemos a la siguiente alerta o regresamos a la misma alerta 
                  if (data.mensaje == 'Correcto') {
                    this.buttonCheckNewPassword();

                  } else {
                    this.changePassword();
                    this.presentToastWithOptions('Error en la contraseña, por favor intentelo nuevamente', 'error', false, 3000);
                  }
                }, 1800);
              },
                (error: any) => {

                });


          }
        }
      ]
    }); // fin alerta

    await alertA.present(); // fin

  }
  // 2 cambio contraseña comprueba los dos nuevos campos de contraseña
  async buttonCheckNewPassword() {

    const alertB = await this.alertCtrl.create({
      message: 'Por favor ingrese la nueva contraseña',
      inputs: [
        {
          name: 'contrasenaNueva',
          type: 'password',
          placeholder: 'Nueva contraseña'
        },
        {
          name: 'contrasenaNuevaConfirmacion',
          type: 'password',
          placeholder: 'Confirme su nueva contraseña'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: (datosNuevaContrasena: any) => {
            if (this.comprobarLonguitudMinimaMasNumerosContrasena
              (datosNuevaContrasena.contrasenaNueva)) { // comprobarLonguitudMinimaContrasena(

              if (datosNuevaContrasena.contrasenaNueva == datosNuevaContrasena.contrasenaNuevaConfirmacion) {

                this.confirmacionCambioContrasena(datosNuevaContrasena.contrasenaNueva);
              } else {
                this.presentToastWithOptions('los campos no coinciden, intentelo nuevamente', 'error', false, 3000);
                this.buttonCheckNewPassword();
              }
            } else {
              this.presentToastWithOptions(
                'la contraseña debe tener una longitud mínima de 6 caracteres, y contener por lo menos un número',
                'error', false, 4000);
              this.buttonCheckNewPassword();
            }

          }
        }
      ]
    }); // fin alerta nueva contraseña
    await alertB.present(); // fin ingreso nueva contraseña

  }

  // 3 cambio contraseña advertencia y aceptacion para enviarla

  async confirmacionCambioContrasena(contrasenaAGuardar: string) {

    const alertC = await this.alertCtrl.create({
      header: 'Cambio de Contraseña!',
      message: 'recuerde que su nueva contraseña será modificada, y no podra ingresar a la aplicación con su anterior contraseña  ',

      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');

          }
        }, {
          text: 'Cambiar',
          handler: () => {
            // aqui se modifica la contraseña en el servidor

            this.registro.GuardarContrasena(contrasenaAGuardar, this.valor.identificacion);  // aqui guardo la contraseña
            this.presentarComprobando();
            setTimeout(() => {
              if (this.registro.verificacionContrasena) {
                this.presentToastWithOptions('Cambio exitoso de la contraseña', 'ok', false, 3000);
              } else {
                this.presentToastWithOptions('ha ocurrido un error, intentelo más tarde', 'error', false, 3000);
              }
            }, 1200);

          }
        }
      ]
    }); // fin alerta nueva contraseña
    await alertC.present(); // fin cambio contraseña

  }

  // mensajes alertas superiores o inferiores error correcto para , mostrar barra arriba o abajo

  async presentToastWithOptions(mensaje: string, tipo: string, cerrar: boolean, tiempo: number) {
    // solo tomamos de tipo si es error o correcto
    if (tipo == 'error') {
      tipo = 'danger';
    } else {
      tipo = 'success';
    }
    let posicionlugar;
    if (tipo == 'danger') {
      posicionlugar = 'bottom';
    } else {
      posicionlugar = 'top';
    }
    const toast = await this.toastController.create({
      message: mensaje,
      showCloseButton: cerrar,
      position: posicionlugar,
      closeButtonText: 'Listo',
      color: tipo,
      duration: tiempo
    });
    toast.present();
  }

  comprobarLonguitudMinimaContrasena(comprobarcontrasena: string): boolean {
    const longuitudMinima = 6;
    if (comprobarcontrasena.length >= longuitudMinima) {
      console.log('longitud contra:   ' + comprobarcontrasena.length);
      return true;
    }
    return false;
  }

  comprobarLonguitudMinimaMasNumerosContrasena(comprobarcontrasena: string): boolean {
    const longuitudMinima = 6;

    let encontrar = '0123456789';
    if (comprobarcontrasena.length >= longuitudMinima) {
      for (let i = 0; i < comprobarcontrasena.length; i++) {
        if (encontrar.indexOf(comprobarcontrasena.charAt(i)) != -1) {
          return true;
        }
      }
      return false;
    }
    return false;
  }

  

  logout() {
    this.userData.logout();
    this.router.navigateByUrl('/login');
  }

  support() {
    this.router.navigateByUrl('/support');
  }

  seleccionarArchivo() {
    this.fileChooser.open()
      .then(uri => console.log('Dirección', uri))
      .catch(e => console.log(e));
  }

  SubirCV() {
    // create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();
    console.log('direccion: ' + urls.fotos + 'uploadPDF.php');

    // option transfer
    let options: FileUploadOptions = {
      fileKey: 'documento',
      fileName: this.valor.identificacion + '.pdf',
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: 'application/pdf',
      headers: {}
    };

    // file transfer action
    fileTransfer.upload(this.fotografia, urls.fotos + 'uploadPhoto.php', options)
      .then(
        data => {
          // alert('Correcto');
        },
        err => {
          console.log(err);
          // alert('Incorrecto');
        }
      );
  }

  // con esto se abre una nueva ventana (VisibilidadDeseadaPage) en un nuevo  modal

  async presentModalVisibilidadDeseada() {

    this.presentarComprobando();
    const modal = await this.modalController.create({
      component: VisibilidadDeseadaPage,
      componentProps: {
        'modoPreferencias': 'visibilidadDeseada'
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
  }

   async presentarCampoAccion() {
    // console.log('aun no hace  nada');
    this.presentarComprobando();
    const modal = await this.modalController.create({
      component: VisibilidadDeseadaPage,
      componentProps: {
        'modoPreferencias': 'campoAccion'
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
  }

  comprobar_visibilidad(){
    if(this.AppComponente.visible== true){
      this.visibilidad= true;
      console.log(this.visibilidad);
    }else{
      this.visibilidad=false;
      console.log(this.visibilidad);
    }

  }

  actualizar_visibilidad(event){
    let selectedObject = event.detail.value;
    if(selectedObject== 'vista'){
      this.AppComponente.cambiarVisibilidad();
      this.comprobar_visibilidad();
      console.log('compruebo visibilidad')
    }
  }

}


