import { Component, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { ConferenceData } from '../../providers/conference-data';
import { Platform } from '@ionic/angular';
import { Ubicacion } from '../../providers/ubicacion';
import { ModalController } from '@ionic/angular';
import { AgitarPage } from '../agitar/agitar.page';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';
import { UserData } from '../../providers/user-data';
import * as urls from '../../providers/urls.servicios';
import { DetallePersonaEncontradaPage } from '../detalle-persona-encontrada/detalle-persona-encontrada.page';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation
} from '@ionic-native/google-maps/ngx';

// import { AppComponent } from '../../app.component';

@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
  styleUrls: ['./map.scss']
})
export class MapPage implements AfterViewInit {
  @ViewChild('mapCanvas') mapElement: ElementRef;
  tope: number = 2;
  intento: number = 0;
  visible: boolean;
  URLAvatar = urls.URLimagen;
  _i = 0;
  constructor(
    public confData: ConferenceData,
    public platform: Platform,
    public ubicacion: Ubicacion,
    public modalController: ModalController,
    public registro: RegistroUsuariosService,
    public valor: Valores,
    public userData: UserData
  ) {
    setTimeout(() => {
      this.registro.BuscarPersonas(this.ubicacion.latitud,
        this.ubicacion.longitud, this.valor.identificacion, '50');
    }, 500);
  }


   async ngAfterViewInit() {
    const googleMaps = await getGoogleMaps(
      'AIzaSyB8pf6ZdFQj5qw7rc_HSGrhUwQKfIe9ICw'
    );

      const mapEle = this.mapElement.nativeElement;
      const map = new googleMaps.Map(mapEle, {
        center: {'name': 'Mi ubicación',
                 'lat': parseFloat(this.ubicacion.latitud),
                 'lng': parseFloat(this.ubicacion.longitud),
                 'center': true },
        zoom: 20
      });
     const center = googleMaps.ILatLng = {
                    'lat': parseFloat(this.ubicacion.latitud),
                    'lng': parseFloat(this.ubicacion.longitud),
                    };

     const infoWindow = new googleMaps.InfoWindow({
       content: `<ion-avatar><img src="${this.URLAvatar}${this.valor.identificacion}.jpg" alt="Mi ubicación"></ion-avatar>
                      `
     });

     // console.log('markerdata: ' + JSON.stringify(markerData));
     const marker = new googleMaps.Marker({
       position: {
         'lat': parseFloat(this.ubicacion.latitud),
         'lng': parseFloat(this.ubicacion.longitud)
       },
       map,
       title: 'Mi ubicación',
       icon: './assets/img/puntoGps2.png'
     });


       marker.addListener('click', () => {
        infoWindow.open(map, marker);
      });


     googleMaps.event.addListenerOnce(map, 'idle', () => {
       mapEle.classList.add('show-map');
       infoWindow.open(map, marker);
     });


      setTimeout(() => {
     

        setTimeout(() => {
          
          console.log('mi identificacion en el mapa es:', this.valor.identificacion);
        
          this.registro.PersonasCercanas.cercanos.forEach((markerData: any) => {

            const infoWindow = new googleMaps.InfoWindow({
              content: `<div [hidden]="${this.valor.identificacion}==${markerData.Identificacion}" >
                          <ion-chip (click)="detalles(${markerData.Identificacion})" outline="true">
                            <ion-avatar>
                              <img src="http://186.30.53.158/php-crud/usuarios/${markerData.foto_perfil}">
                            </ion-avatar>
                              <ion-label> <strong>${markerData.Cargo_Actual}</strong> </ion-label>
                              <ion-icon name="person-add"></ion-icon>
                          </ion-chip>
                      </div>
                      `
            });
            console.log('markerdata: ' + JSON.stringify(markerData));
            const marker = new googleMaps.Marker({
              position: {
                'lat': parseFloat(markerData.LatitudActual),
                'lng': parseFloat(markerData.LongitudActual)
              },
              map,
              title: markerData.Nombre,
              icon: './assets/img/puntoGps.png'
            });

            setTimeout(() => {
              marker.addListener('click', () => {
                infoWindow.open(map, marker);
              });
            });

            googleMaps.event.addListenerOnce(map, 'idle', () => {
              mapEle.classList.add('show-map');
            });
          }, 100);

        
        }, 1500);

       
        
  
      }, 100);
  }

  async detalles(idusr) {
    /*
    console.log('LISTO PARA MOSTRAR DETALLES');
    this.presentAlert();
    */

    const modal = await this.modalController.create({
      component: DetallePersonaEncontradaPage,
      componentProps: {
        IDEncontrado: idusr,
        solicitante: 'false'
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
  }
}

function getGoogleMaps(apiKey: string): Promise<any> {
  const win = window as any;
  const googleModule = win.google;
  if (googleModule && googleModule.maps) {
    return Promise.resolve(googleModule.maps);
  }

  return new Promise((resolve, reject) => {
    const script = document.createElement('script');
    script.src = `https://maps.googleapis.com/maps/api/js?v=weekly&key=${apiKey}&v=3.37`;
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);
    script.onload = () => {
      const googleModule2 = win.google;
      if (googleModule2 && googleModule2.maps) {
        resolve(googleModule2.maps);
      } else {
        reject('google maps not available');
      }
    };
  });
}
