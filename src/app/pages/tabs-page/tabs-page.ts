import { Component } from '@angular/core';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';
// import { AppComponent } from '../../app.component';

@Component({
  templateUrl: 'tabs-page.html'
})
export class TabsPage {
 public NotifPerfil = false;
 public NotifTarjetero = false;
 public intervalo: any;
 
  constructor(
              public registro: RegistroUsuariosService,
              public valor: Valores,
              // public appcomponente: AppComponent
              ) {
                  //// verifica contactos nuevos
                  this.correrNotificaciones();
              }



  correrNotificaciones() {
    this.intervalo = setInterval(() => {
      if (this.valor.identificacion !== null) {
     // console.log('se activa en tabs setinterval');
      this.registro.RevisarSolicitudes(this.valor.identificacion);
      this.verificarNotificaciones();
      }
      else {
       // console.log('No se ha activado en tabs setinterval');
       // this.correrNotificaciones();
      }
    }, 20000);
  }


  verificarNotificaciones() {
     // console.log(this.registro.solicitudesPendientes.cantidad);
        if (this.registro.solicitudesPendientes.cantidad !== undefined) {
          this.NotifPerfil = true;
            // console.log('Solicitud nueva');
        }
        if (this.registro.solicitudesPendientes.cantidad === undefined) {
          this.NotifPerfil = false;
          // console.log('No tiene solicictudes');
      }
        /* if (this.appcomponente.NuevasTarjetasRepartidas.cantidad !== undefined) {
          this.NotifTarjetero = true;
          // console.log('No tiene solicictudes');
        }
      if (this.appcomponente.NuevasTarjetasRepartidas.cantidad === undefined) {
        this.NotifTarjetero = false;
        // console.log('No tiene solicictudes');
      } */
    }

  cambiarPunto() {
    this.NotifPerfil = false;
  }

  cambiarPuntoT() {
    this.NotifTarjetero = false;
  }
}
