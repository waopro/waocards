import { Component, AfterContentInit, OnDestroy, Input } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import * as urls from '../../providers/urls.servicios';
import { DeviceMotion, DeviceMotionAccelerationData, DeviceMotionAccelerometerOptions } from '@ionic-native/device-motion/ngx';
import { Ubicacion } from '../../providers/ubicacion';
import { usuarios } from '../../providers/usuarios';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Vibration } from '@ionic-native/vibration/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Valores } from '../../providers/valores';
import { AlertController} from '@ionic/angular';
import { PagosPage } from '../pagos/pagos.page';

@Component({
  selector: 'compartir-tarjeta',
  templateUrl: './compartir-tarjeta.page.html',
  styleUrls: ['./compartir-tarjeta.page.scss'],
})
export class CompartirTarjetaPage implements OnDestroy{

  @Input() IdUsuario: string;
  @Input() foto: string;
  repartiendoTarjeta:boolean = false;
  saldoTarjetas: string = "0";
  comprandoTarjetas: boolean = false;
  eligiendoAccion: boolean = true;
  agiteahora: boolean= false;
  estaAgitando: string= 'false';
  buscando: boolean=false;
  repartiendo: boolean = false;
  seleccionando: boolean = false;
  seleccionoAlguno: boolean = true;
  seleccionados: string[] = [];
  cantidadNoAmigos: number = 0;
  cantidadSeleccionados: number = 0;
  URLavatar: string = urls.URLimagen;
  subscription: any;
  lastX: any;
  lastY: number;
  lastZ: number;
  x: number;
  y: number;
  z: number;
  acelera: string;
  mov: number;
  numeroContactos: any;
  listado: string= '';
  cantidadTarjetas: string = "0";
 PersonasAntesdeRepartir: any = [{
    noAmigos: ''
  }];


  constructor(
              private modalController: ModalController,
 
              private deviceMotion: DeviceMotion,
              public ubicacion: Ubicacion,
              public usuario: usuarios,
              public registro: RegistroUsuariosService,
              public vibration: Vibration,
              public platform: Platform,
              public localNotifications: LocalNotifications,
              public translate: TranslateService,
              public valor: Valores,
              private alertcontroller: AlertController,
              ) {
                this.usuario.datosUsuario(this.valor.identificacion).subscribe((respuesta) => {
                  console.log(respuesta);
                  this.saldoTarjetas = respuesta['SaldoTarjetas'];
                  console.log(this.saldoTarjetas);
                });
              }

// ngAfterContentInit(){
//   setTimeout(() => {
//     this.acelerar();
//   }, 800);
// }

acelerar() {
  //this.agiteahora = true;
  try {
    const opcion: DeviceMotionAccelerometerOptions = {
      frequency: 500
    };

    this.deviceMotion.getCurrentAcceleration().then(
      (acceleration: DeviceMotionAccelerationData) => console.log(acceleration),
      (error: any) => console.log(error)
    );

    // Watch device acceleration
    this.subscription = this.deviceMotion.watchAcceleration(opcion).subscribe((acceleration: DeviceMotionAccelerationData) => {
      if (!this.lastX) {
        this.lastX = acceleration.x;
        this.lastY = acceleration.y;
        this.lastZ = acceleration.z;
        return;
      }
      this.x = Math.abs(acceleration.x - this.lastX);
      this.y = Math.abs(acceleration.y - this.lastY);
      this.z = Math.abs(acceleration.z - this.lastZ);
      this.acelera = '' + this.x + this.y + this.z;
      if (this.x + this.y + this.z > 15) {
        this.mov = 1;
        this.PruebaAgitando();
      } else {
        this.mov = 0;
      }
      if (this.mov === 1) {
    setTimeout(() => {
      this.buscarPersonas();
      this.PruebaBuscando();
      this.subscription.unsubscribe();//apaga el sensor
      
    }, 3000);

  }
    });
  } catch (err) {
    alert('Error en: ' + err);
  }
}
  PruebaBuscando() {
    this.agiteahora = true;
    this.buscando = true;
    this.estaAgitando = 'false';
    this.agiteahora = false;
  }
  buscarPersonas() {
    
    this.registro.BuscarPersonas(this.ubicacion.latitud, this.ubicacion.longitud, this.valor.identificacion, '15')
    .subscribe((respuesta)=>{
      this.registro.PersonasCercanas = respuesta;
      this.PruebaCorrecto();
      this.numeroContactos = this.registro.PersonasCercanas.cercanos.length;
      this.vibration.vibrate([400, 200, 400]);
       this.seleccionando = true;
      this.buscando = false;

      let idsEncontrados: string[] = [];

      for(let n = 0; n < respuesta.cercanos.length; n++){
        console.log('idsencontrados', respuesta.cercanos[n].Identificacion);
        idsEncontrados[n] = respuesta.cercanos[n].Identificacion;
      }
      console.log("IDS ANTES DE LLAMAR 'antesdeRepartir()'");
      console.log(idsEncontrados);
      this.antesdeRepartir(this.valor.identificacion, idsEncontrados);

     // this.aviso_tarjeta_repartida(); 
    });
    // setTimeout(() => {
    //   this.PruebaCorrecto();
    //   this.numeroContactos = this.registro.PersonasCercanas.cercanos.length;
    //   this.vibration.vibrate([400, 200, 400]);
    // }, 2000);
  }

  antesdeRepartir(idUsr:string, idEncontrados: string[]){
    this.registro.verPersonasAntesdeRepartir(idUsr, idEncontrados).subscribe((respuesta)=>{
      console.log(respuesta);
      this.PersonasAntesdeRepartir = respuesta;
      this.cantidadNoAmigos = respuesta.noAmigos.length;
      setTimeout(() => {
        this.seleccionando = true;
        console.log(this.seleccionando);
      }, 200);
    });
  }

  repartir() {

    console.log('SE VA A REPARTIR A ESTAS PERSONAS', this.seleccionados);
    this.registro.RepartirTarjetas(this.valor.identificacion, this.seleccionados)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.aviso_tarjeta_repartida();
        this.modalController.dismiss();
      });
    // this.registro.RepartirTarjetas();
  }

  seleccionado(idSeleccionado: string, event: any){
    console.log(event);
    if (event.detail.checked === true){
      this.seleccionoAlguno = true;
      this.cantidadSeleccionados++;
      console.log('CONTADOR DE SELECCIONADOS:', this.cantidadSeleccionados);
      this.seleccionados.push(idSeleccionado);
    }
    else {
      this.cantidadSeleccionados--;
      console.log('CONTADOR DE SELECCIONADOS:', this.cantidadSeleccionados);
      let elemento = this.seleccionados.indexOf(idSeleccionado);
      if (elemento !== -1) {
        this.seleccionados.splice(elemento, 1);
      }
    }
    if (this.cantidadSeleccionados != 0) {
      this.seleccionoAlguno = false;
     }
     else {
      this.seleccionoAlguno = true;
     }
  }

  PruebaCorrecto() {
    this.agiteahora = false;
    this.estaAgitando = 'correcto';
    this.buscando = false;
  }
  
  PruebaAgitando() {
    this.estaAgitando = 'true';
  this.agiteahora = false;
  this.buscando = false;
  // this.aviso_tarjeta_repartida();
  }


 

  comprarTarjetas() {
    this.eligiendoAccion = false;
    this.comprandoTarjetas = true;
  }

  compartirTarjeta() {
    this.eligiendoAccion = false;
    this.repartiendoTarjeta = true;
    console.log(this.numeroContactos);
    console.log(this.agiteahora);
    console.log(this.estaAgitando);
    console.log(this.buscando);
    console.log(this.repartiendoTarjeta);
   /*  this.buscarPersonas();
    setTimeout(() => {
      this.PruebaBuscando();
    }, 1000); */
    this.acelerar();
  }

  elegirAccion() {
    if (this.subscription !== undefined) 
      this.subscription.unsubscribe();

    this.eligiendoAccion = true;
    this.repartiendoTarjeta = false;
    this.comprandoTarjetas = false;
    this.buscando = false;
    this.estaAgitando = 'false';
    console.log('estamos imprimiendo elegir accion....')
    console.log(this.agiteahora);
    console.log(this.estaAgitando);
    console.log(this.buscando);
    console.log(this.repartiendoTarjeta);
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and pass back data
    this.modalController.dismiss();
    //this.subscription.unsubscribe();
    //this.suscritoChat = false;
    console.log('compartir cerrado');
  }

  seleccion(){
    console.log('seleccionado');
  }

  // detener() {
  //   this.subscription.unsubscribe();
  //   this.estaAgitando = 'false';
  //   this.agiteahora = false;
  // }

  setSound() {
    if (this.platform.is('android')) {
      return 'file://assets/sounds/notifsound.mp3';
    } else {
      return 'file://assets/sounds/notifsound.mp3';
    }
  }
  errSound() {
    if (this.platform.is('android')) {
      return 'file://assets/sounds/notifsound.mp3';
    } else {
      return 'file://assets/sounds/notifsound.mp3';
    }
  }
  ngOnDestroy() {
    console.log('¡Agitar destruido!');
    
    if (this.subscription !== undefined)
      this.subscription.unsubscribe();
    }

    async aviso_tarjeta_repartida() {
      const alert = await this.alertcontroller.create({
        header: '¡Correcto!',
        message: 'Se repartió correctamente a ' + this.cantidadSeleccionados + ' personas.',
        buttons: [
          {
            text: 'OK',
            role: 'OK',
            cssClass: 'secondary',
            handler: () => {
            }
          }
        ]
        
      });
  
      await alert.present();
    }

    cadena_de_nombres(){
      let i=0;
      console.log('pasa por funcion cadena_de_nombres');
      console.log(this.registro.PersonasCercanas.cercanos);
      
        for( i = 0; i < this.registro.PersonasCercanas.cercanos.length; i++){
          this.listado += "<ion-item>" + this.registro.PersonasCercanas.cercanos[i].Nombre + "</ion-item>";
       //   console.log(this.listado);
        }
        this.listado = "<ion-list>" +this.listado+ "</ion-list>";
        return this.listado;
    }

  async pagar(){

    console.log(this.valor.identificacion);
    console.log(this.cantidadTarjetas);
    
    const modal = await this.modalController.create({
      component: PagosPage,
      componentProps: {
        'cantidadTarjetas': this.cantidadTarjetas,
        'IdentificacionUsr': this.valor.identificacion
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
    
  }

}
