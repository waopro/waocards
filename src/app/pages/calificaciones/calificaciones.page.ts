import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController} from '@ionic/angular';
import { Valores } from '../../providers/valores';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';

@Component({
  selector: 'calificaciones',
  templateUrl: './calificaciones.page.html',
  styleUrls: ['./calificaciones.page.scss'],
})
export class CalificacionesPage implements OnInit {
  calificacionObtenida: string = '';
  rate: any = '5';
  areaTexto: string = '';
  objetoRecibido: any = ''; // usado para recibir los datos
  respuestaDatosEntrada: any = [{
    error: '',
    mensaje: '',
  }] ;

  constructor(
    private toastControl: ToastController,
    private activatedRoute: ActivatedRoute,
    private valor: Valores,
    private registroUsuariosService: RegistroUsuariosService,
    public router: Router
  ) {

  }

  ngOnInit() {
    this.objetoRecibido = this.activatedRoute.snapshot.paramMap.get('Identificacion');
  }

  logRatingChange($event) {
    this.rate = $event;
    console.log('respuesta: ', $event);

  }

  enviarComentario() {

    this.registroUsuariosService.guardarCalificacionComentarios( this.objetoRecibido, this.valor.identificacion,
      this.rate, this.areaTexto).subscribe(respuesta => {
        this.respuestaDatosEntrada = respuesta;
        // console.log("respuesta que llega: ", this.respuestaDatosEntrada);
        if (this.respuestaDatosEntrada.error == false) {
          this.presentToastWithOptions('comentario enviado correctamente', 'correcto', true, 3000);
          this.cerrarPaginaActual();
        } else {
          this.presentToastWithOptions('Su comentario no ha sido enviado, intentelo nuevamente', 'error', true, 2000);
        }
      }, (error: any) => {
        console.log(error);
      });

  }


  async presentToastWithOptions(mensaje: string, tipo: string, cerrar: boolean, tiempo: number) {
    // solo tomamos de tipo si es error o correcto
    // cerrar es para que se cierre solo poniendo true
    // tiempo el tiempo deseado de visibilidad en pantalla
    if (tipo == 'error') {
      tipo = 'danger';
    } else {
      tipo = 'success';
    }
    let posicionlugar;
    if (tipo == 'danger') {
      posicionlugar = 'bottom';
    } else {
      posicionlugar = 'top';
    }
    const toast = await this.toastControl.create({
      message: mensaje,
      showCloseButton: cerrar,
      position: posicionlugar,
      closeButtonText: 'Listo',
      color: tipo,
      duration: tiempo
    });
    toast.present();
  }

  cerrarPaginaActual() {
    this.router.navigateByUrl('/app/tabs/speakers');
  }

}
