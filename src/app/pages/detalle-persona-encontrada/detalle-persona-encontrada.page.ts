import { Component, Input, OnInit } from '@angular/core';
// import { ActivatedRoute, Router } from '@angular/router';
import { usuarios } from '../../providers/usuarios';
// import { Educacion } from '../../providers/educacion';
// import { Experiencias } from '../../providers/experiencias';
import * as urls from '../../providers/urls.servicios';
import { TranslateService } from '@ngx-translate/core';

import { ModalController, AlertController, ToastController } from '@ionic/angular';

import { DatosUsuarioInterface } from '../../interfaces/datos-usuario-interface';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';
import { PagosPage } from '../pagos/pagos.page';

@Component({
  selector: 'detalle-persona-encontrada',
  templateUrl: './detalle-persona-encontrada.page.html',
  styleUrls: ['./detalle-persona-encontrada.page.scss'],
})
export class DetallePersonaEncontradaPage implements OnInit {

  @Input() IDEncontrado: number;
  @Input() solicitante?: string;
  DatosBasicos: any;
  Educacion: any;
  Experiencias: any;
  enviada = 'indefinido';
  ocultarDatosBasicos: boolean = true;
  completo: string;
  URLavatar: string;
  CargandoConfirmacion = 'false';
  CargandoRechazo = 'false';
  // elementos del idioma
  DesliceParaRecargar: string;
  ObteniendoSus: string;

  // las siguientes variables son usadas para obtener las categorias
  categoriasSeleccionadasUser: any  = '';
  categorias: any;
  datosPreferencias: any;
  datosComprobarCategoriasUser: any;
  datosComprobarCategoriasUserMuestra: any;
  myInputs = [];

  constructor(
    private dataProvider: usuarios,
    private _translate: TranslateService,
    public modalController: ModalController,
    public registro: RegistroUsuariosService,
    public valor: Valores,
    public alertController: AlertController,
    public toastController: ToastController,
  ) {
    // En el contructor no se deben asignar parámetros recibidos desde el exterior debido a que este se ejecuta
    // console.log(this.IDEncontrado);
    this.cargarIdiomaElementos();
    setTimeout(() => {
      this.comprobarSolicitud();
    }, 500);


  }

  ngOnInit() {
    // tube la necesidad de usar after init debido a que la informacion no la cargaba en el consructor
    // console.log("esto lo carga despues del inicio");
    this.cargaCampoAccion(); // este metodo  carga informacion de Campos de accion
  }
  cargarIdiomaElementos() {
    this._translate.stream('PAGES.SPEAKER-LIST.DESLICE_PARA_RECARGAR').subscribe((text: string) => {
      this.DesliceParaRecargar = text;
    });
    this._translate.stream('PAGES.SPEAKER-DETAIL.OBTENIENDO_SUS_CONT').subscribe((text: string) => {
      this.ObteniendoSus = text;
    });
  }

  aceptarSolicitud(Identificacion) {
    console.log('Aceptó la solicitud de: ' + Identificacion);
    this.CargandoConfirmacion = 'cargando';
    setTimeout(() => {
      this.CargandoConfirmacion = 'correcto';
      this.enviada = 'agregado';
      this.registro.ConfirmarSolicitud(this.valor.identificacion, Identificacion);
      this.volveraCargar();
    }, 1300);
  }

  async presentarToastError() {
    const toast = await this.toastController.create({
      message: 'Solicitud rechazada',
      showCloseButton: true,
      position: 'middle',
      duration: 3000
     });
    toast.present();
  }

  volveraCargar() {
    // this.registro.RevisarSolicitudes(this.valor.identificacion);
    // this.solicitudes.pop();
    setTimeout(() => {
    //  this.solicitudes = this.registro.solicitudesPendientes.solicitudes;
    //  this.cantidad = this.registro.solicitudesPendientes.cantidad;
     // this.ionViewDidEnter();
     this.solicitante = 'false';
      this.CargandoConfirmacion = 'false';
      this.CargandoRechazo = 'false';
    }, 600);
    }

  cancelarSolicitud(Identificacion) {
    console.log('Rechazó la solicitud de: ' + Identificacion);
    this.CargandoRechazo = 'cargando';
    setTimeout(() => {
      this.CargandoRechazo = 'correcto';
       this.registro.RechazarSolicitud(this.valor.identificacion, Identificacion);
      this.modalController.dismiss();
      this.presentarToastError();
    }, 1300);
  }

  async alertaRechazado() {
    const alert = await this.alertController.create({
      header: 'Rechazado',
      message: 'Solicitud rechazada',
      buttons: ['Listo']
    });

    await alert.present();
  }

  ionViewWillEnter() {
    console.log(this.IDEncontrado);

    console.log('Solicitante:' + this.solicitante);
    this.dataProvider.datosUsuario(this.IDEncontrado)
    .subscribe((RTADataUsuario: DatosUsuarioInterface) => {
      console.log(RTADataUsuario);
      this.DatosBasicos = RTADataUsuario.DatosPersonales;
      this.Educacion = RTADataUsuario.Educacion;
      this.Experiencias = RTADataUsuario.Experiencia;
      this.URLavatar = urls.URLimagen;
      this.completo = 'true';


      console.log(this.DatosBasicos);
      console.log(this.Educacion);
      console.log(this.Experiencias);

    });

  }

  comprobarSolicitud() {
    this.registro.ComprobarSolicitud(this.valor.identificacion, String(this.IDEncontrado));
   console.log(String(this.IDEncontrado));
    setTimeout(() => {
      if (this.registro.SolicitudComprobada.solicitudenviada == 'Si') {
        this.enviada = 'true';
        console.log('Solicitud Enviada para: ' + String(this.IDEncontrado));
      }
      if (this.registro.SolicitudComprobada.solicitudenviada == 'No') {
        this.enviada = 'false';
        console.log('Solicitud No Enviada para: ' );

      }
      if (this.registro.SolicitudComprobada.solicitudenviada == 'Agregado') {
        console.log('Agregado para: ' );
        this.enviada = 'agregado';
        }
    }, 1800);
  }

  Contactar() {
    setTimeout(() => {
    this.registro.Contactar(this.valor.identificacion, String(this.IDEncontrado));
    this.enviada = 'true';
    // this.comprobarSolicitud();
    }, 600);
  }


  doRefrescar(event) {
    console.log('Empieza a refrescar todo');

    setTimeout(() => {
      console.log('Se refrescó correctamente');
      this.ionViewWillEnter();
      event.target.complete();
    }, 2000);
  }

  async avisoPago() {
    const alert = await this.alertController.create({
      header: '¿Te interesa este contacto?',
      message: 'Una vez realices tu pago tendrás acceso completo al perfil seleccionado.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Pagar',
          handler: () => {
            console.log('Confirm Ok');
            this.pagar();
          }
        }
      ]
    });

    await alert.present();
  }

  async pagar() {
    const modal = await this.modalController.create({
      component: PagosPage,
      componentProps: {
        'profesion': this.DatosBasicos.Profesion,
        'perfil': this.DatosBasicos.perfil,
        'foto': this.URLavatar + this.DatosBasicos.foto_perfil
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
    // console.log("agitar funciona!");
  }


  cargaCampoAccion() {
    console.log('metodo de llamado datos ', this.IDEncontrado);
    // se pregunta en la base de datos por los campos de accion de la persona
    this.registro.consultarCamposAccionYCodigosUsuario(this.IDEncontrado.toString()).subscribe(respuesta => {
      this.datosPreferencias = respuesta;
      this.categorias = this.datosPreferencias.items.categorias;
      this.categoriasSeleccionadasUser = this.datosPreferencias.items.categoriasUsuario;
      // los datos son separados para manejarlos por una array
      this.datosComprobarCategoriasUser = this.categoriasSeleccionadasUser.split(',');
      this.datosComprobarCategoriasUserMuestra = this.datosComprobarCategoriasUser;
      const camposPreseleccionados = this.datosComprobarCategoriasUser.map(function (item) {
        return item.substring(0, 4);
      });

      // el elemento queda cargado con las categorias sin especificar un area VIDF
      this.datosComprobarCategoriasUser = camposPreseleccionados;
      // console.log("3 aqui llega esto lo carga despues del inicio", this.datosComprobarCategoriasUser);


    }, (error: any) => {
      console.log(error);
      console.log('ha ocurrido un error al traer las categoriasUser');
    });

    console.log('se han cargado correctamente las categorias : ');
  }


  clickElementoChip(datoCategoriasUser) {
    // this.datosComprobarCategoriasUserMuestra
   // console.log('por aqui esta el problema', datoCategoriasUser);
    const identificarSuperSeleccion = this.datosComprobarCategoriasUserMuestra.filter(function (item) {
      if (item.substring(0, 4) == datoCategoriasUser) {
        return item;
      }
    });

    const caracteristicas = identificarSuperSeleccion.toString();
    // console.log('aqui esta ciclo', identificarSuperSeleccion.toString());
    this.presentarAlerta(caracteristicas.substring(4, caracteristicas.length));
    // console.log(identificarSuperSeleccion, '                                     ', caracteristicas );

  }

  async presentarAlerta(caracteristica: string) {
    let subcategoria = '';
    this.myInputs = [];
    // aqui van todas las subcategorias
    for (let i = 0; i < caracteristica.length; i++) {
      switch (caracteristica[i]) {
        case 'C':
          subcategoria = 'Comprador';
          break;
        case 'V':
          subcategoria = 'Vendedor';
          break;
        case 'I':
          subcategoria = 'Importador';
          break;
        case 'F':
          subcategoria = 'Fabricante';
          break;
        case 'D':
          subcategoria = 'Distribuidor';
          break;
        default:
          break;

      }

      this.myInputs.push(
        {
          type: 'radio',
          label: '' + subcategoria,
          value: '' + subcategoria + '',
          disabled: true,
          checked: true
        }
      );
    }

    // console.log( '                                     ', caracteristica );
      const alert = await this.alertController.create({
        header: 'Información adicional sobre el usuario seleccionado',
        inputs: this.myInputs,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              // console.log('Confirm Ok esto regresa de ',  caracteristica);

            }
          }
        ]
      });
      await alert.present();
    }

}
