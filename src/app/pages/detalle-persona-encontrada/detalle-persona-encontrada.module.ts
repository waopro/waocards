import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
//import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallePersonaEncontradaPage } from './detalle-persona-encontrada.page';

/*
const routes: Routes = [
  {
    path: '',
    component: DetallePersonaEncontradaPage
  }
];
*/

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
    //RouterModule.forChild(routes)
  ],
  declarations: [/*DetallePersonaEncontradaPage*/]
})
export class DetallePersonaEncontradaPageModule {}
