import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallePersonaEncontradaPage } from './detalle-persona-encontrada.page';

describe('DetallePersonaEncontradaPage', () => {
  let component: DetallePersonaEncontradaPage;
  let fixture: ComponentFixture<DetallePersonaEncontradaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallePersonaEncontradaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallePersonaEncontradaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
