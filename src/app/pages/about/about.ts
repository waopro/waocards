import { Component, ViewEncapsulation, AfterContentInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { PopoverController, Platform } from '@ionic/angular';
import { AppComponent } from '../../app.component';
import { PopoverPage } from '../about-popover/about-popover';
import * as ver from '../../providers/data.version';
import * as urls from '../../providers/urls.servicios';
import { UserData } from '../../providers/user-data';
import {Valores} from '../../providers/valores';
//import { Device } from '@ionic-native/device/ngx';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
  styleUrls: ['./about.scss'],
})
export class AboutPage implements AfterContentInit {
  Fecha = ver.fechaCompilacion;
  Version = ver.version;
  Nombre = ver.nombre;
  desarrolladores = ver.desarrolladores;
  estaDesactualizado: string;
  IP = ver.serverAddress;
  dispositivoID: any;
  versNueva: any;

  constructor(public popoverCtrl: PopoverController,
              public appComponent: AppComponent,
              public userData: UserData,
              public valor: Valores,
              public platform: Platform,
              //public device: Device,
              private _translate: TranslateService) {
    this.comprobarAct();
    this.dispositivoID = this.valor.UUID;
  }

  ngAfterContentInit() {
  // console.log('plataforma: ', this.platform.platforms());
  // console.log(this.platform.version());
  }


  comprobarAct() {

    this.estaDesactualizado = 'comprobando';
    setTimeout(() => {
      this.appComponent.comprobarActualizaciones();
    }, 2000);
    setTimeout(() => {
      if (this.appComponent.EstaDesactualizado === true) {
        this.estaDesactualizado = 'true';
        this.versNueva = this.appComponent.NuevaVersion;
      } if (this.appComponent.EstaDesactualizado === false) {
        this.estaDesactualizado = 'false';
      }
    }, 5000);
  }

  presentarCuadrodial() {
    open(urls.URL_waocardsService + this.userData.datosactualizacion.url, '_system');
  }

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: PopoverPage,
      event
    });
    await popover.present();
  }
}
