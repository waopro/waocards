import { Component, AfterContentInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Ubicacion } from '../../providers/ubicacion';
import { ModalController } from '@ionic/angular';
import { AgitarPage } from '../agitar/agitar.page';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';
import { UserData } from '../../providers/user-data';
import { MapPage } from '../map/map';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { ManualPage } from '../manual/manual.page';
import { CompartirTarjetaPage } from '../compartir-tarjeta/compartir-tarjeta.page';
import { Route, Router } from '@angular/router';
import * as urls from '../../providers/urls.servicios';

import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'find-people',
  templateUrl: './find-people.page.html',
  styleUrls: ['./find-people.page.scss'],
})

export class FindPeoplePage implements AfterContentInit{
  tope: number = 2;
  intento: number = 0;
  visible: boolean;
  _i = 0;
  public intervalo: any;
  URLavatar = urls.URLimagen;

  constructor(
    public platform: Platform,
    public ubicacion: Ubicacion,
    public modalController: ModalController,
    public registro: RegistroUsuariosService,
    public valor: Valores,
    public userData: UserData,
    public locationAccuracy: LocationAccuracy,
    public router: Router,
    public userdata: UserData,
    public statusBar: StatusBar
  ) {
    // Fue necesario hacerlo porque el valor de visibilidad cambia a cero de manera extrana
    this.verificarSesion();
   
  }

  ngAfterContentInit() {
    this.statusBar.overlaysWebView(false);
    this.statusBar.styleLightContent();
    this.statusBar.backgroundColorByHexString('#6600cc'); // define el color de la barra de notificaciones

  }

  verificarSesion() {
    if (this.valor.identificacion != undefined) {
      console.log('Visibilidad en map: ' + this.userData.datos.Visible);
      /* if (this.userData.datos.Visible == '1') {
        this.visible = true;
      }
      if (this.userData.datos.Visible == '0') {
        this.visible = true;
      } */
      this.peticionUbicacion();
      this.actualizarCoords();
    }
  }

  abrirContactos() {
    this.router.navigateByUrl('/importar-contactos');
  }

  peticionUbicacion() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {

      if (canRequest) {
        // the accuracy option will be ignored by iOS
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => this.actualizarCoords(),
          error => console.log('Error requesting location permissions', error)
        );
      }

    });
  }

  actualizarCoords() {
      if (this.valor.identificacion != undefined) {
        setTimeout(() => {

          this.ubicacion.iniciarGeubicacion();
        }, 1000);
      }
  }


  finalizarRastreo() {
    this.ubicacion.cancelarUbicacion();
    clearInterval(this.intervalo);
  }

  async mapa() {
    this.router.navigateByUrl('/map');
  }

  async agitar() {
    const modal = await this.modalController.create({
      component: AgitarPage
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
   // console.log("agitar funciona!");
  }

  // FUNCION QUE DESPLIEGA UN MODLA PARA PODER REALIZAR BÍSQUEDAS POR PALABRAS CLAVES
  async manual() {
    const modal = await this.modalController.create({
      component: ManualPage
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
   // console.log("agitar funciona!");
  }

  async compartirTarjeta(){
    const modal = await this.modalController.create({
      component: CompartirTarjetaPage,
      componentProps: {
        'IdUsuario': this.userData.valor.identificacion,
        'foto': this.URLavatar + this.userData.datos.foto_perfil
      }
    });
    //modal.showBackdrop = true;
    //modal.backdropDismiss = true;
    return await modal.present();
  }
}
