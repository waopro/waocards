import { Component, OnInit, Input, Output } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { OpcionesPagos } from '../../interfaces/opciones-pagos';
import { Valores } from '../../providers/valores';
import { PagosService } from '../../providers/pagos.service';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
@Component({
  selector: 'pagos',
  templateUrl: './pagos.page.html',
  styleUrls: ['./pagos.page.scss'],
})
export class PagosPage implements OnInit {

  //CUANDO SE ESTÁ HACIENDO UN PAGO POR OBTENER LA TARJETA DE ALGUIEN:
  @Input() profesion?: string;
  @Input() perfil?: string;
  @Input() foto?: string;
  //CUANDO SE ESTÁ HACIENDO UN PAGO POR OBTENER SALDO DE TARJETAS PARA REPARTIR:
  @Input() IdentificacionUsr?: string;
  @Input() cantidadTarjetas?: string;

  valorPago: number = 0;

  deshabilitar_tarjeta: boolean = false;
  deshabilitar_trans_pse: boolean = false;
  deshabilitar_efectivo: boolean = false;
  se_habilito_tarjeta: boolean = false;
  se_habilito_trans_pse: boolean = false;
  se_habilito_efectivo: boolean = false;
  habilito_vista_tipodepago: boolean = true;
  activo_atras: boolean = true;
  activo_continuar: boolean = true;
  chulear_opcion_tarjeta = false;
  chulear_opcion_pse = false;
  chulear_opcion_efectivo = false;
  se_habilito_pse_2 = false;
  se_habilito_tarjeta2 = false;
  se_habilito_efectivo2 = false;
  se_habilito_pse_3 = false;
  tomo_dato_iduser = false;
  tomo_dato_idtarjeta = false;
  tomo_dato_fechaexp = false;
  tomo_dato_idseguridad = false;
  submitted: boolean = false;
  emailInvalido: boolean = false;
  banco_seleccionado: string = '¿cual es tu banco?';
  campo = new Array();
  dato_valido_numtarjeta: boolean = false;
  dato_valido_codseg: boolean = false;
  dato_valido_numdoc: boolean = false;
  band: boolean = false;

  cargapago: OpcionesPagos = {
    num_tarjeta: '',
    name_lastname: '',
    fecha_exp: '',
    cod_seguridad: '',
    num_doc: ''


  };

  constructor(private modalController: ModalController, private alertcontroller: AlertController, public validez: Valores, private pagosServ: PagosService) { }

  ngOnInit() {

    //Calculamos valor a pagar
    if(this.profesion && this.perfil && this.foto)
      this.valorPago = 1;
    else
      this.valorPago = Number(this.cantidadTarjetas) * 1;

    console.log('Se recibió la siguiente profesion: ' + this.profesion);
    console.log('Se recibió el siguiente perfil: ' + this.perfil);
    console.log('Se recibió la siguiente foto de perfil: ' + this.foto);
    console.log(this.dato_valido_numtarjeta);
    console.log(this.dato_valido_numdoc);
    console.log(this.dato_valido_codseg);
  }

  deshabilitar_botones(seleccionado: string) {
    if (seleccionado == 'Tarjeta') {
      this.deshabilitar_tarjeta = false;
      this.deshabilitar_trans_pse = true;
      this.deshabilitar_efectivo = true;


    } else if (seleccionado == 'Trans_pse') {
      this.deshabilitar_tarjeta = true;
      this.deshabilitar_trans_pse = false;
      this.deshabilitar_efectivo = true;

    } else if (seleccionado == 'Efec_payu') {
      this.deshabilitar_tarjeta = true;
      this.deshabilitar_trans_pse = true;
      this.deshabilitar_efectivo = false;
    }
  }

  checked_botones(event: any) {
    if (event.detail.checked == false) {
      this.deshabilitar_tarjeta = false;
      this.deshabilitar_trans_pse = false;
      this.deshabilitar_efectivo = false;
    }
  }
  habilitar_boton_PAGAR(seleccion: string) {
    if (seleccion == 'continuar') {
      this.habilito_vista_tipodepago = false;
      if (!this.deshabilitar_tarjeta && this.deshabilitar_trans_pse && this.deshabilitar_efectivo)
        this.se_habilito_tarjeta = true;
      else if (this.deshabilitar_tarjeta && !this.deshabilitar_trans_pse && this.deshabilitar_efectivo)
        this.se_habilito_trans_pse = true;
      else if (this.deshabilitar_tarjeta && this.deshabilitar_trans_pse && !this.deshabilitar_efectivo)
        this.se_habilito_efectivo = true;
    }
  }
  activa_boton_atras(select: string) {
    this.submitted = false;
    if (select == 'atras_tarjeta') {
      this.cargapago.num_doc = '';
      this.cargapago.num_tarjeta = '';
      this.cargapago.cod_seguridad = '';
      this.cargapago.name_lastname = '';
      this.se_habilito_tarjeta = false;
      this.habilito_vista_tipodepago = true;
      this.chulear_opcion_tarjeta = true;
      this.chulear_opcion_pse = false;
      this.chulear_opcion_efectivo = false;
    }
    else if (select == 'atras_pse') {
      this.se_habilito_trans_pse = false;
      this.habilito_vista_tipodepago = true;
      this.chulear_opcion_pse = true;
      this.chulear_opcion_tarjeta = false;
      this.chulear_opcion_efectivo = false;
    }
    else if (select == 'atras_efectivo') {
      this.cargapago.num_doc = '';
      this.se_habilito_efectivo = false;
      this.habilito_vista_tipodepago = true;
      this.chulear_opcion_tarjeta = false;
      this.chulear_opcion_pse = false;
      this.chulear_opcion_efectivo = true;
    }
    else if (select == 'atras_pse_2') {
      this.cargapago.num_doc = '';
      this.se_habilito_trans_pse = true;
      this.se_habilito_pse_2 = false;
    }
    else if (select == 'atras_pse_3') {
      this.se_habilito_pse_3 = false;
      this.se_habilito_pse_2 = true;
    }
    else if (select == 'atras_tarjeta2') {
      this.se_habilito_tarjeta2 = false;
      this.se_habilito_tarjeta = true;
    }
    else if (select == 'atras_efectivo2') {
      this.se_habilito_efectivo2 = false;
      this.se_habilito_efectivo = true;
      console.log('si esta pasando por aqui!!!!');
    }

    // this.se_habilito_tarjeta=false;
    // this.habilito_boton=false;
  }

  activa_boton_continuar(miseleccion: string, formulario?: NgForm) {
    if (miseleccion == 'continuar_tarjeta') {
      this.submitted = true;
      let num_tarjeta_str: string = String(this.cargapago.num_tarjeta);
      let num_doc_str: string = String(this.cargapago.num_doc);
      let cod_seguridad_str: string = String(this.cargapago.cod_seguridad);

      console.log(num_tarjeta_str);
      console.log(num_tarjeta_str.length);
      if (formulario.valid && num_tarjeta_str.length == 16 && num_doc_str.length >= 7 && num_doc_str.length <= 10 && cod_seguridad_str.length == 3) {//this.activo_continuar=true;
        this.se_habilito_tarjeta = false;
        this.se_habilito_tarjeta2 = true;

      } if (!(num_tarjeta_str.length == 16)) {
        this.dato_valido_numtarjeta = false;


      } else {
        this.dato_valido_numtarjeta = true;
      }
      if (!((num_doc_str.length >= 7) && (num_doc_str.length <= 10))) {// evaluo los valores para campos de documento que NO son validos
        this.dato_valido_numdoc = false;

      } else {
        this.dato_valido_numdoc = true;
      }
      if (!(cod_seguridad_str.length == 3)) {
        this.dato_valido_codseg = false;

      }
      else {
        this.dato_valido_codseg = true;
      }

    }
    else if (miseleccion == 'continuar_pse') {


      this.se_habilito_trans_pse = false;
      this.se_habilito_pse_2 = true;

    }
    else if (miseleccion == 'continuar_efectivo') {
      this.submitted = true;
      let num_doc_str: string = String(this.cargapago.num_doc);
      if (formulario.valid && num_doc_str.length >= 7 && num_doc_str.length <= 10) {
        this.se_habilito_efectivo = false;
        this.se_habilito_efectivo2 = true;
      }
    }
    else if (miseleccion == 'continuar_pse_2') {
      this.submitted = true;
      let num_doc_str: string = String(this.cargapago.num_doc);
      console.log(formulario.valid);
      if (formulario.valid && num_doc_str.length >= 7 && num_doc_str.length <= 10) {
        this.se_habilito_pse_2 = false;
        this.se_habilito_pse_3 = true;
      }
    }
    // else if(miseleccion=='continuar_pse3'){
    //   this.se_habilito_pse_2=false;
    //   this.se_habilito_pse_3=true;
    // }
    console.log('IMPRIME BANDERAS DE VALIDACION');
    console.log(this.dato_valido_numtarjeta);
    console.log(this.dato_valido_numdoc);
    console.log(this.dato_valido_codseg);
  }
  async presentAlert() {
    const alert = await this.alertcontroller.create({
      header: 'Confirmar Pago',

      buttons: [{
        text: 'OK', handler: () => {
          //this.dismiss();
          if (this.cantidadTarjetas && this.IdentificacionUsr){
            this.pagosServ.pagarTarjetas(this.IdentificacionUsr, this.cantidadTarjetas).subscribe(()=>{
              console.log("Se han sumado: " + this.cantidadTarjetas + " tarjetas al saldo de esta persona");
            });
          }
          this.modalController.dismiss();
        }
      }, 'CANCELAR']
    });

    await alert.present();
  }
  soloLetra(e) {
    // Se captura el código ASCII de la tecla presionada en la variable local "key"
    let key: number = e.keyCode || e.which;
    // console.log(key);

    // Se evalua si el código ASCII capturado corresponde a las letras del abcedario en mayúsculas o en minúsculas
    if (
      (key > 64 && key < 91) ||
      (key > 96 && key < 123) ||
      key == 32 ||
      (key > 127 && key < 155) ||
      (key > 159 && key < 166) ||
      (key > 223 && key < 238)
    ) {
      return true; // Se permite la escritura en el ion-input
    }
    return false; // Se bloquea la escritura en el ion-input
  }

  preregistro(lectura: string) {
    if (lectura == 'continuar_tarjeta') { }
    // SI SE LLEGA AQUÍ ES PORQUE LA VALIDACIÓN DE DATOS ESÁ COMPLETA Y SE PROCEDE A PREREGISTRAR AL USUARIO
    this.cargapago.num_tarjeta;
    this.cargapago.name_lastname;
    this.cargapago.fecha_exp;
    this.cargapago.cod_seguridad;
    this.cargapago.num_doc;
  }

  // onSignup(form: NgForm) {
  //   this.submitted = true;

  //   if (form.valid) {
  //     for(var i = 0; i < 16; this.campo[i++] = i)
  //   ;       /* empty statement */
  //   console.log(this.campo); 
  //   // [1, 2, 3, 4, 5]
  //  this.submitted = true
  //     // this.mostrarAlertConfirmacion();
  //   }
  // }

  nuevo(event) {
    console.log(event);

  }

  solo_numero(event, campo: number) {
    console.log(event);
    console.log(this.cargapago.num_tarjeta);
    console.log(campo);
    let num_string: string = '';
    let limiteCifras: number = 0;
    switch (campo) {
      case 1:
        num_string = String(this.cargapago.num_tarjeta);
        limiteCifras = 16;
        break;
      case 2:
        num_string = String(this.cargapago.cod_seguridad);
        limiteCifras = 3;
        break;

      case 3:
        num_string = String(this.cargapago.num_doc);
        limiteCifras = 10;
        break;


    }

    // Se captura el código ASCII de la tecla presionada en la variable local "key"
    let key: number = event.keyCode || event.which;
    // console.log(key);

    // Se evalua si el código ASCII capturado corresponde a las letras del abcedario en mayúsculas o en minúsculas
    if (
      (key > 47 && key < 58) && (num_string.length < limiteCifras)) {
      return true; // Se permite la escritura en el ion-input
    }
    return false; // Se bloquea la escritura en el ion-input
  }

}

