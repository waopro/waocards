import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiCVPage } from './mi-cv.page';

describe('MiCVPage', () => {
  let component: MiCVPage;
  let fixture: ComponentFixture<MiCVPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiCVPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiCVPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
