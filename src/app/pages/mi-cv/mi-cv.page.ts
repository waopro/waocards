import { Component, OnInit, ViewEncapsulation, AfterContentInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { usuarios } from '../../providers/usuarios';
import * as urls from '../../providers/urls.servicios';
import { CVUsuarioInterface } from '../../interfaces/CVUsuario-interface';
import { Valores } from '../../providers/valores';
import { ModalController, AlertController, LoadingController } from '@ionic/angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { UserData } from '../../providers/user-data';
import { Downloader, DownloadRequest, NotificationVisibility } from '@ionic-native/downloader/ngx';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';


@Component({
  selector: 'mi-cv',
  templateUrl: './mi-cv.page.html',
  styleUrls: ['./mi-cv.page.scss'],
})
export class MiCVPage implements OnInit, AfterContentInit {

  DatosBasicos: any;
  Educacion: any;
  Experiencias: any;
  Idiomas: any;
  Habilidades: any;
  Referencias: any;
  completo: string;
  URLavatar: string;
  fotografia: any;
  PDF: any;
  loading: any;
  public ResultadoSubida: any = [{
    response: ''
  }];

  constructor(
    public dataProvider: usuarios,
    private router: Router,
    private route: ActivatedRoute,
    public valor: Valores,
    public modalController: ModalController,
    private transfer: FileTransfer,
    private fileChooser: FileChooser,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public registro: RegistroUsuariosService,
    public userData: UserData,
    private downloader: Downloader,
    private document: DocumentViewer,
    private fileOpener: FileOpener
  ) { }

  ngOnInit() {
  }

  ngAfterContentInit() {
    this.completo = 'false';
    /// Timeout para los datos básicos del personaje
    setTimeout(() => {
      // console.log(this.dataProvider.cv);
      this.dataProvider.datosCVUsuario(this.valor.identificacion)
        .subscribe((RTADataUsuario: CVUsuarioInterface) => {
          // console.log(RTADataUsuario);
          this.DatosBasicos = RTADataUsuario.DatosPersonales;
          this.Educacion = RTADataUsuario.Educacion;
          this.Experiencias = RTADataUsuario.Experiencia;
          this.Habilidades = RTADataUsuario.Habilidades;
          this.Idiomas = RTADataUsuario.Idiomas;
          this.Referencias = RTADataUsuario.Referencias;

          this.URLavatar = urls.URLimagen;
          this.completo = 'true';


          console.log(this.DatosBasicos);
          console.log(this.Educacion);
          console.log(this.Experiencias);
          console.log(this.Idiomas);
          console.log(this.Habilidades);
          console.log(this.Referencias);

        });
    }, 300);

  }

  llamar(contacto) {
    // console.log('llamando a: ' + contacto);
    window.open('tel:' + contacto);
  }

  seleccionarArchivo() {
    this.fileChooser.open()
      .then(uri => {
        this.PDF = uri;
        setTimeout(() => {
          this.confirmar();
         }, 2000);
      })
      .catch(e => console.log(e));
  }

  async confirmar() {
    const alert = await this.alertController.create({
      header: 'Confirmar',
      message: 'Se subirá el archivo PDF seleccionado al servidor',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            // open(urls.URL_waocardsService + this.userData.datosactualizacion.url, '_system');
            this.mostrarSubiendo();
            this.SubirCV(this.PDF);
            // console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  async mostrarSubiendo() {
    this.loading = await this.loadingController.create({
      message: 'Subiendo..'
    });
    await this.loading.present();

  }

  SubirCV(PDF: any) {
    // create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();
    console.log('direccion: ' + urls.URL_waocardsService1 + '/uploadPDF');

    // option transfer
    let options: FileUploadOptions = {
      fileKey: 'documento',
      fileName: this.valor.identificacion + '.pdf',
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: 'application/pdf',
      headers: {}
    };

    // file transfer action
    fileTransfer
      .upload(PDF, urls.URL_waocardsService1 + '/uploadPDF', options)
      .then(
        data => {
          // alert('Correcto' + data);
          this.ResultadoSubida = data;
          console.log(data);
          setTimeout(() => {
            this.loading.dismiss();
            if (this.ResultadoSubida.response == '"Correcto"') {
              this.presentarResultadoPDF('Documento subido correctamente');
              this.registro.cambiarPDFSubido('1');
              this.userData.inicio();
            }
            if (this.ResultadoSubida.response == '"Error"') {
              this.presentarResultadoPDF('Hay problema con el servidor, intente más adelante');
            }
            if (this.ResultadoSubida.response == '"Incorrecto"') {
              this.presentarResultadoPDF('Documento inválido, revise si el documento tiene problemas');
            }
          }, 800);
        },
        err => {
          console.log(err);
          // alert('Incorrecto');
        }
      );
  }

  async presentarResultadoPDF(mensaje: string) {
   // console.log('se dispara correcto');
    const alert = await this.alertController.create({
      header: mensaje,
      buttons: ['Continuar']
    });
    alert.present();
  }

  descargarPDF() {
    const request: DownloadRequest = {
      uri: urls.CV + this.valor.identificacion + '.pdf',
      title: this.valor.identificacion + '.pdf',
      description: 'Mi CV',
      mimeType: 'application/pdf',
      visibleInDownloadsUi: true,
      notificationVisibility: NotificationVisibility.VisibleNotifyCompleted,
      destinationInExternalFilesDir: {
        dirType: 'Downloads',
        subPath: this.valor.identificacion + '.pdf'
      }
    };


    this.downloader.download(request)
      .then((locat: string) => {
        console.log('ubicacion: ' + locat);
        this.abrirPDF(locat);
      })
      .catch((error: any) =>  console.log(error));
  }

  // abrirPDF(ruta: any) {
  //   const options: DocumentViewerOptions = {
  //     title: 'Mi CV'
  //   }

  //   this.document.viewDocument(ruta, 'application/pdf', options);
  // }

  abrirPDF(rutadesc: any) {
    console.log('llego a abrir archivo: ', rutadesc);
    console.log('Se abrira: ', rutadesc);
    this.fileOpener.open(rutadesc, 'application/pdf')
      .then(() => console.log('File is opened'))
      .catch(e => console.log('Error opening file', e));
  }

  salirModal() {
    this.modalController.dismiss();
  }
}
