import { Component, ViewEncapsulation, AfterContentInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { usuarios } from '../../providers/usuarios';
import { Educacion } from '../../providers/educacion';
import { Experiencias } from '../../providers/experiencias';
import * as urls from '../../providers/urls.servicios';
import { TranslateService } from '@ngx-translate/core';
import { CVUsuarioInterface } from '../../interfaces/CVUsuario-interface';
import { ModalController, NavController, AlertController } from '@ionic/angular';
import {ListadoAmigosPage} from '../listado-amigos/listado-amigos.page';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
// import { StarRatingComponent } from 'ng-starrating';



@Component({
  selector: 'page-speaker-detail',
  templateUrl: 'speaker-detail.html',
  styleUrls: ['./speaker-detail.scss'],
})
export class SpeakerDetailPage implements AfterContentInit {

  speaker: any;
  educa: any;
  expe: any;
  formaciones: any[] = [];
  experiencias: any[] = [];
  DatosBasicos: any;
  Educacion: any;
  Experiencias: any;
  Idiomas: any;
  Habilidades: any;
  Referencias: any;
  completo: string;
  URLavatar: string;
  categoria: any;
  contreferencias: any[] = ['1', '1', '1', '1'];
  // elementos del idioma
  DesliceParaRecargar: string;
  ObteniendoSus: string;
  ratingObtenido: any;
  public Calificaciones: any = [{
    promedio: '',
    mensaje: ''
  }];
  datosPreferencias: any;
  categorias: any;
  categoriasSeleccionadasUser: any;
  datosComprobarCategoriasUser: any;
  datosComprobarCategoriasUserMuestra: any;
  myInputs = [];
  constructor(
    public dataProvider: usuarios,
    private educacionProveedor: Educacion,
    private expProveedor: Experiencias,
    public router: Router,
    private route: ActivatedRoute,
    private _translate: TranslateService,
    private modalController: ModalController,
    private registro: RegistroUsuariosService,
    public navCtrl: NavController,
    private AlertController: AlertController 
  ) {
    this.cargarIdiomaElementos();
  }



  logRatingChange(rating) {
    console.log('changed rating: ', rating);
    // do your stuff
  }

  cargarIdiomaElementos() {
    this._translate.stream('PAGES.SPEAKER-LIST.DESLICE_PARA_RECARGAR').subscribe((text: string) => {
      this.DesliceParaRecargar = text;
    });
    this._translate.stream('PAGES.SPEAKER-DETAIL.OBTENIENDO_SUS_CONT').subscribe((text: string) => {
      this.ObteniendoSus = text;
    });
  }

  ngAfterContentInit() {
    this.completo = 'false';
    /// Timeout para los datos básicos del personaje
      setTimeout(() => {
        const speakerId = this.route.snapshot.paramMap.get('speakerId');
        // console.log(this.dataProvider.cv);
        this.dataProvider.datosCVUsuario(speakerId)
          .subscribe((RTADataUsuario: CVUsuarioInterface) => {
            // console.log(RTADataUsuario);
            this.DatosBasicos = RTADataUsuario.DatosPersonales;
            this.Educacion = RTADataUsuario.Educacion;
            this.Experiencias = RTADataUsuario.Experiencia;
            this.Habilidades = RTADataUsuario.Habilidades;
            this.Idiomas = RTADataUsuario.Idiomas;
            this.Referencias = RTADataUsuario.Referencias;

            this.URLavatar = urls.URLimagen;
            this.completo = 'true';
            this.cargaCampoAccion(); // este metodo  carga informacion de Campos de accion

          //  console.log(this.DatosBasicos.identificacion);
            // console.log(this.Educacion);
            // console.log(this.Experiencias);
            // console.log(this.Idiomas);
            // console.log(this.Habilidades);
            // console.log(this.Referencias);

          });

        this.registro.obtenerCalificacionComentarios(speakerId)
          .subscribe(respuesta => {
            console.log(respuesta);
            this.Calificaciones = respuesta;
            console.log('Calificaciones', this.Calificaciones);
          }, (error: any) => {
            console.log(error);
          });






        this.dataProvider.load().subscribe((data: any) => {
          this.completo = 'true';
          this.URLavatar = urls.URLimagen; // URL de acceso a las imagenes
          const speakerId = this.route.snapshot.paramMap.get('speakerId');
          if (data && data.records) {
            for (const speaker of data.records) {
              if (speaker && speaker.Identificacion === speakerId) {
                this.speaker = speaker;
                break;
              }
            }
          }
        },
          (err: any) => {
            this.completo = 'error';
            //// console.log(JSON.stringify(err));
          });
    }, 300);
//////////////////
// Timeout para formacipon academica
    setTimeout(() => {
      this.educacionProveedor.load().subscribe((datos: any) => {
        const speakerId = this.route.snapshot.paramMap.get('speakerId');
        if (datos && datos.records) {
          for (const educa of datos.records) {
            if (educa && educa.Identificacion === speakerId) {
              this.educa = educa;
              break;
            }
          }
        }
      });
///////////////////
      this.educacionProveedor.getFormacion().subscribe((formaciones: any[]) => {
        this.formaciones = formaciones;
      });

    }, 600);

    setTimeout(() => {
      this.expProveedor.load().subscribe((datos: any) => {
        const speakerId = this.route.snapshot.paramMap.get('speakerId');
        if (datos && datos.records) {
          for (const expe of datos.records) {
            if (expe && expe.Identificacion === speakerId) {
              this.expe = expe;
              break;
            }
          }
        }
      });

      this.expProveedor.getExperiencia().subscribe((experiencias: any[]) => {
        this.experiencias = experiencias;
      });
    }, 900);

    

  }

  llamar(contacto) {
    // console.log('llamando a: ' + contacto);
    window.open('tel:' + contacto);
  }

  doRefrescar(event) {
    // console.log('Empieza a refrescar todo');

    setTimeout(() => {
      // console.log('Se refrescó correctamente');
      this.ngAfterContentInit();
      event.target.complete();
    }, 2000);
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ListadoAmigosPage,
      componentProps: {
        'identificacion': this.DatosBasicos.Identificacion,
        'user': this.DatosBasicos.Nombre
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
  }


  // pagina para presentar seccion de calificaciones
  cambioPaginaCalificaciones() {
    // this.navCtrl.pop();  // cerrar la pagina actual
    this.navCtrl.navigateForward(['/calificaciones', this.DatosBasicos.Identificacion]);
    // this.navCtrl.pop();
  }


// este metodo se encarga de cargar toda la informacion de campos de accion 
// y los campos de accion a los que esta suscrito la persona
  cargaCampoAccion() {
    this.registro.consultarCamposAccionYCodigosUsuario(this.DatosBasicos.Identificacion).subscribe(respuesta => {
      this.datosPreferencias = respuesta;
      this.categorias = this.datosPreferencias.items.categorias;
      // console.log(this.categorias);
      this.categoriasSeleccionadasUser = this.datosPreferencias.items.categoriasUsuario;

      this.datosComprobarCategoriasUser = this.categoriasSeleccionadasUser.split(',');
      // console.log('mas omenos esta por aqui',this.datosComprobarCategoriasUser);
      this.datosComprobarCategoriasUserMuestra = this.datosComprobarCategoriasUser;
      const camposPreseleccionados = this.datosComprobarCategoriasUser.map(function (item) {
        return item.substring(0, 4);
      });

      // el elemento queda cargado con las categorias sin especificar un area VIDF
      this.datosComprobarCategoriasUser = camposPreseleccionados;

      // console.log('esto tiene en user', this.datosComprobarCategoriasUserMuestra);

    }, (error: any) => {
      console.log(error);
    });


  }

  clickElementoChip(datoCategoriasUser) {
    // this.datosComprobarCategoriasUserMuestra
   // console.log('por aqui esta el problema', datoCategoriasUser);
    const identificarSuperSeleccion = this.datosComprobarCategoriasUserMuestra.filter(function (item) {
      if (item.substring(0, 4) == datoCategoriasUser) {
        return item;
      }
    });

    const caracteristicas = identificarSuperSeleccion.toString();
    // console.log('aqui esta ciclo', identificarSuperSeleccion.toString());
    this.presentarAlerta(caracteristicas.substring(4, caracteristicas.length));
    // console.log(identificarSuperSeleccion, '                                     ', caracteristicas );

  }

  async presentarAlerta(caracteristica: string) {
    let subcategoria = '';
    this.myInputs = [];
    // aqui van todas las subcategorias
    for (let i = 0; i < caracteristica.length; i++) {
      switch (caracteristica[i]) {
        case 'C':
          subcategoria = 'Comprador';
          break;
        case 'V':
          subcategoria = 'Vendedor';
          break;
        case 'I':
          subcategoria = 'Importador';
          break;
        case 'F':
          subcategoria = 'Fabricante';
          break;
        case 'D':
          subcategoria = 'Distribuidor';
          break;
        default:
          break;

      }

      this.myInputs.push(
        {
          // type: 'text',
          label: '' + subcategoria,
          value: '' + subcategoria + '',
          // checked: true
          type: 'radio',
          // label: '' + subcategoria,
          // value: '' + subcategoria + '',
          disabled: true,
          checked: true
        }
      );
    }

    console.log( '                                     ', caracteristica );
      const alert = await this.AlertController.create({
        header: 'Información adicional sobre el usuario seleccionado',
        inputs: this.myInputs,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              // console.log('Confirm Ok esto regresa de ',  caracteristica);

            }
          }
        ]
      });
      await alert.present();
    }
}


