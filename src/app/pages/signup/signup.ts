import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Valores } from '../../providers/valores';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
  styleUrls: ['./signup.scss']

})
export class SignupPage implements OnInit, OnDestroy {

  constructor(
    public router: Router,
    public valid: Valores,
    public loadingController: LoadingController
  ) {}

  ngOnInit() {
    // this.app.setTitle('Schedule');
  }

  ionViewDidLoad() {
  }

  abrirUsuario() {
    this.presentLoadingWithOptions();
    setTimeout(() => {
      this.router.navigateByUrl('/signup-person');
    }, 300);
  }

  abrirEmpresa() {
    this.presentLoadingWithOptions();
    setTimeout(() => {
      this.router.navigateByUrl('/signup-enterprise');
    }, 300);
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      duration: 5000,
      message: 'Un momento, por favor',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  ngOnDestroy() {
    console.log('Página-Login destruida!');
    // this.presentLoadingWithOptions();
  }
}
