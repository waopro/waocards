import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SignupEnterprisePage } from './signup-enterprise.page';

const routes: Routes = [
  {
    path: '',
    component: SignupEnterprisePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SignupEnterprisePage]
})
export class SignupEnterprisePageModule {}
