import { Component, OnInit } from '@angular/core';

import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Keyboard } from '@ionic-native/keyboard/ngx';

import { UserData } from '../../providers/user-data';

import { UserOptions } from '../../interfaces/user-options';

import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Location } from '@angular/common';
import { Valores } from '../../providers/valores';
import { EnterpriseOptions } from '../../interfaces/enterprise-options';


@Component({
  selector: 'signup-enterprise',
  templateUrl: './signup-enterprise.page.html',
  styleUrls: ['./signup-enterprise.page.scss'],
})
export class SignupEnterprisePage implements OnInit {
  esempresa = false;
  signup: UserOptions = {
    username: '',
    id_user: '',
    tel_cel: '',
    email: '',
    bornDate: '',
    password: ''
  };
  signupenterprise: EnterpriseOptions = {
    enterprisename: '',
    nit_enterprise: '',
    Enterprise_tel_cel: '',
    Enterpriseemail: '',
    ciiu: ''
  };
  segment = 'Usuario';
  submitted: boolean = false;
  emailInvalido: boolean = false;
  FaltaTelefono: boolean = false;
  FaltaNombre: boolean = false;
  FaltaID: boolean = false;
  faltaDOB: boolean = false;
  keybaordShowSub: any;
  keybaordHideSub: any;

  constructor(
    public router: Router,
    public userData: UserData,
    public registro: RegistroUsuariosService,
    public valid: Valores,
    public keyboard: Keyboard,
    public location: Location
  ) { }

  ngOnInit() {
    // this.app.setTitle('Schedule');
    this.updateType();
    this.esempresa = false;
  }

  ionViewDidLoad() {
    this.addKeyboardListeners();
  }

  updateType() {
    // Close any open sliding items when the schedule updates
    this.esempresa = !this.esempresa;
  }
  addKeyboardListeners() {
    this.keybaordShowSub = this.keyboard.onKeyboardShow().subscribe((e: any) => {
      const deviceHeight = window.innerHeight;
      console.log(deviceHeight);
      const keyboardHeight = e.keyboardHeight;
      let deviceHeightAdjusted = deviceHeight - keyboardHeight; // device height adjusted
      deviceHeightAdjusted = deviceHeightAdjusted < 0 ? (deviceHeightAdjusted * -1) : deviceHeightAdjusted; // only positive number
      document.getElementById('page').style.height = deviceHeightAdjusted + 'px'; // set page height
      document.getElementById('page').setAttribute('keyBoardHeight', keyboardHeight); // save keyboard height
    });
    this.keybaordHideSub = this.keyboard.onKeyboardHide().subscribe(() => {
      setTimeout(() => {
        document.getElementById('page').style.height = 100 + '%'; // device  100% height
      }, 300);
    });
  }

  soloLetra(e) {
    // Se captura el código ASCII de la tecla presionada en la variable local "key"
    let key: number = e.keyCode || e.which;
    // console.log(key);

    // Se evalua si el código ASCII capturado corresponde a las letras del abcedario en mayúsculas o en minúsculas
    if (
      (key > 64 && key < 91) ||
      (key > 96 && key < 123) ||
      key == 32 ||
      (key > 127 && key < 155) ||
      (key > 159 && key < 166) ||
      (key > 223 && key < 238)
    ) {
      return true; // Se permite la escritura en el ion-input
    }
    return false; // Se bloquea la escritura en el ion-input
  }
  /* 
    EnSignup(form: NgForm) {
      this.submitted = true;
  
      if (form.valid) {
        let indiceArroba = this.signupenterprise.Enterpriseemail.indexOf('@');
        if (
          indiceArroba == -1 ||
          indiceArroba == 0 ||
          indiceArroba == this.signupenterprise.Enterpriseemail.length - 1
        ) {
          this.emailInvalido = true;
          return;
        } else {
          let indiceOtroArroba = this.signupenterprise.Enterpriseemail.indexOf('@', indiceArroba + 1);
          if (indiceOtroArroba === -1) {
            this.emailInvalido = false;
          } else {
            this.emailInvalido = true;
            return;
          }
        }
  
        // SI SE LLEGA AQUÍ ES PORQUE LA VALIDACIÓN DE DATOS ESÁ COMPLETA Y SE PROCEDE A PREREGISTRAR AL USUARIO
       this.registro.preregistroEmpresa(
         this.signupenterprise.enterprisename,
         this.signupenterprise.nit_enterprise,
         this.signupenterprise.Enterprise_tel_cel,
         this.signupenterprise.Enterpriseemail,
         this.signupenterprise.ciiu
       );
        this.valid.guardarid(this.signupenterprise.nit_enterprise);
        this.router.navigateByUrl('/signup-confirmation');
        }
    } */

  onSignup(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      let indiceArroba = this.signup.email.indexOf('@');
      if (
        indiceArroba == -1 ||
        indiceArroba == 0 ||
        indiceArroba == this.signup.email.length - 1
      ) {
        this.emailInvalido = true;
        return;
      } else {
        let indiceOtroArroba = this.signup.email.indexOf('@', indiceArroba + 1);
        if (indiceOtroArroba === -1) {
          this.emailInvalido = false;
        } else {
          this.emailInvalido = true;
          return;
        }
      }

      // SI SE LLEGA AQUÍ ES PORQUE LA VALIDACIÓN DE DATOS ESÁ COMPLETA Y SE PROCEDE A PREREGISTRAR AL USUARIO
      this.registro.preregistrar(
        this.signup.username,
        this.signup.tel_cel,
        this.signup.email,
        this.signup.bornDate,
        this.signup.id_user
      );
      this.valid.guardarid(this.signup.id_user);
      this.router.navigateByUrl('/signup-confirmation');
    }
  }
}

