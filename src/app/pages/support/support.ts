import { Component, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AlertController, ToastController } from '@ionic/angular';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';


@Component({
  selector: 'page-support',
  templateUrl: 'support.html',
  styleUrls: ['./support.scss'],
})
export class SupportPage {
  submitted = false;
  supportMessage: string;
  areaTexto  = '';
  banderaBloqueoBotonEnvio: boolean = false;
  constructor(
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private registroUsuariosService: RegistroUsuariosService,
    private toastController: ToastController
  ) { }

  

  async submit(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.supportMessage = '';
      this.submitted = false;

      const toast = await this.toastCtrl.create({
        message: 'Your support request has been sent.',
        duration: 3000
      });
      await toast.present();
    }
  }

  // If the user enters text in the support question and then navigates
  // without submitting first, ask if they meant to leave the page
  // async ionViewCanLeave(): Promise<boolean> {
  //   // If the support message is empty we should just navigate
  //   if (!this.supportMessage || this.supportMessage.trim().length === 0) {
  //     return true;
  //   }

  //   return new Promise((resolve: any, reject: any) => {
  //     const alert = await this.alertCtrl.create({
  //       title: 'Leave this page?',
  //       message: 'Are you sure you want to leave this page? Your support message will not be submitted.',
  //       buttons: [
  //         { text: 'Stay', handler: reject },
  //         { text: 'Leave', role: 'cancel', handler: resolve }
  //       ]
  //     });

  //     await alert.present();
  //   });
  // }

// metodo para enviar los comentarios a un correo especifico

  EnviarComentariosNuevaCategoria() {
    // aun no se ha implementad debe ser usada
    // areaTexto es una variable
    const Asunto: string = 'Sugerencia WAOcards:';
    if ((this.banderaBloqueoBotonEnvio == false) && (this.areaTexto != '')) {
      this.banderaBloqueoBotonEnvio = true;
      this.registroUsuariosService.envioCorreoSugerenciaCampoAccion(Asunto, this.areaTexto)
        .subscribe(respuesta => {
          console.log(respuesta);
          this.salidaPage2(respuesta);
        }, (error: any) => {
          console.log(error);
        });
    } else {
      this.presentToastWithOptions('lo sentimos, su sugerencia no puede ser procesada.', 'error', true, 3000);
    }
  }

  salidaPage2(operacionCorrrecta: any) {

    if (operacionCorrrecta.error == false) {
      this.presentToastWithOptions('Envio exitoso', 'correcto', true, 2000);
      this.areaTexto = '';
    } else {
      this.presentToastWithOptions('ha ocurrido un error, inténtalo mas tarde', 'error', true, 2000);
    }
  }


  async presentToastWithOptions(mensaje: string, tipo: string, cerrar: boolean, tiempo: number) {
    // solo tomamos de tipo si es error o correcto
    // cerrar es para que se cierre solo poniendo true
    // tiempo el tiempo deseado de visibilidad en pantalla
    if (tipo == 'error') {
      tipo = 'danger';
    } else {
      tipo = 'success';
    }
    let posicionlugar;
    if (tipo == 'danger') {
      posicionlugar = 'bottom';
    } else {
      posicionlugar = 'top';
    }
    const toast = await this.toastController.create({
      message: mensaje,
      showCloseButton: cerrar,
      position: posicionlugar,
      closeButtonText: 'Listo',
      color: tipo,
      duration: tiempo
    });
    toast.present();
  }



}
