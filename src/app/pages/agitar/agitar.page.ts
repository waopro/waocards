import { Component, AfterContentInit, OnDestroy} from '@angular/core';
import { NavParams, NavController, Platform } from '@ionic/angular';
import { DeviceMotion, DeviceMotionAccelerationData, DeviceMotionAccelerometerOptions } from '@ionic-native/device-motion/ngx';
import { Ubicacion } from '../../providers/ubicacion';
import { usuarios } from '../../providers/usuarios';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { MapPage } from '../map/map';
import { ModalController } from '@ionic/angular';
import * as urls from '../../providers/urls.servicios';
import { TimeoutError } from 'rxjs';
import { Vibration } from '@ionic-native/vibration/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { TranslateService } from '@ngx-translate/core';
import { DetallePersonaEncontradaPage } from '../detalle-persona-encontrada/detalle-persona-encontrada.page';
import { Valores } from '../../providers/valores';

@Component({
  selector: 'agitar',
  templateUrl: './agitar.page.html',
  styleUrls: ['./agitar.page.scss'],
})
export class AgitarPage implements AfterContentInit, OnDestroy {
  value: number;
  estaAgitando = 'false'; // bandera de que se está agitando el dispositivo
  agiteahora = false; // bandera que le dice al usuario que agite ahora
  lastX: number;
  lastY: number;
  lastZ: number;
  x = 0;
  y = 0;
  z = 0;
  mov = 0;
  buscando = 0;
  usr: any [] = [];
  subscription;
  URLavatar = urls.URLimagen;
  numeroContactos: any;
  acelera: string;
  contactos_encontrados: string;
  contacto_cercanos: string;
  contacto_cercano: string;
  lo_sentimos: string;

  constructor(navParams: NavParams,
              naviController: NavController,
              private deviceMotion: DeviceMotion,
              public ubicacion: Ubicacion,
              public usuario: usuarios,
              public registro: RegistroUsuariosService,
              public modalController: ModalController,
              public vibration: Vibration,
              public platform: Platform,
              public localNotifications: LocalNotifications,
              public translate: TranslateService,
              public valor: Valores
              ) {
                this.obtenertextonotif();
              }

  

  ngAfterContentInit(){
    setTimeout(() => {
      this.acelerar();
    }, 800);
  }

  obtenertextonotif() {
    this.translate.stream('PAGES.AGITAR.CONTACTOS_ENCONTRADOS').subscribe((text: string) => {
      this.contactos_encontrados = text;
    });
    this.translate.stream('PAGES.AGITAR.CONTACTOS_CERCANOS').subscribe((text: string) => {
      this.contacto_cercanos = text;
    });
    this.translate.stream('PAGES.AGITAR.CONTACTO_CERCANO').subscribe((text: string) => {
      this.contacto_cercano = text;
    });
    this.translate.stream('PAGES.AGITAR.LO_SENTIMOS').subscribe((text: string) => {
      this.lo_sentimos = text;
    });
  }

  PruebaAgiteahora() {
    this.agiteahora = true;
    this.estaAgitando = 'false';
    this.buscando = 0;
  }
  async detalles(idusr) {
    /*
    console.log('LISTO PARA MOSTRAR DETALLES');
    this.presentAlert();
    */

   const modal = await this.modalController.create({
      component: DetallePersonaEncontradaPage,
      componentProps: {
        IDEncontrado: idusr,
        solicitante: 'false'
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
  }

  PruebaAgitando() {
  this.estaAgitando = 'true';
  this.agiteahora = false;
  this.buscando = 0;
  }

  PruebaBuscando() {
   this.buscando = 1;
   this.estaAgitando = 'false';
   this.agiteahora = false;
  }

  PruebaCorrecto() {
    this.agiteahora = false;
    this.estaAgitando = 'correcto';
    this.buscando = 0;
  }

  acelerar() {
    this.agiteahora = true;
    try {
      const opcion: DeviceMotionAccelerometerOptions = {
        frequency: 500
      };

      this.deviceMotion.getCurrentAcceleration().then(
        (acceleration: DeviceMotionAccelerationData) => console.log(acceleration),
        (error: any) => console.log(error)
      );

      // Watch device acceleration
      this.subscription = this.deviceMotion.watchAcceleration(opcion).subscribe((acceleration: DeviceMotionAccelerationData) => {
        if (!this.lastX) {
          this.lastX = acceleration.x;
          this.lastY = acceleration.y;
          this.lastZ = acceleration.z;
          return;
        }
        this.x = Math.abs(acceleration.x - this.lastX);
        this.y = Math.abs(acceleration.y - this.lastY);
        this.z = Math.abs(acceleration.z - this.lastZ);
        this.acelera = '' + this.x + this.y + this.z;
        if (this.x + this.y + this.z > 15) {
          this.mov = 1;
          this.PruebaAgitando();
        } else {
          this.mov = 0;
        }
        if (this.mov === 1) {
      setTimeout(() => {
        this.buscarPersonas();
        this.PruebaBuscando();
        this.subscription.unsubscribe();
      }, 3000);

    }
      });
    } catch (err) {
      alert('Error en: ' + err);
    }
  }


  detener() {
    this.subscription.unsubscribe();
    this.estaAgitando = 'false';
    this.agiteahora = false;
  }

 async mapa() {
   const modal = await this.modalController.create({
     component: MapPage
   });
   modal.showBackdrop = true;
   modal.backdropDismiss = true;
   return await modal.present();
  }

  buscarPersonas() {
  this.registro.BuscarPersonas(this.ubicacion.latitud, this.ubicacion.longitud, this.valor.identificacion, '50')
  .subscribe((respuesta) => {
      this.registro.PersonasCercanas = respuesta;
      this.PruebaCorrecto();
      this.numeroContactos = this.registro.PersonasCercanas.cercanos.length;
      this.vibration.vibrate([400, 200, 400]);
    }
  );
    setTimeout(() => {
      this.PruebaCorrecto();
      this.numeroContactos = this.registro.PersonasCercanas.cercanos.length;
      this.vibration.vibrate([400, 200, 400]);
      setTimeout(() => {
        // console.log('aa', this.registro.PersonasCercanas);
      }, 1000);

     });
    setTimeout(() => {

     /*  if (this.numeroContactos !== 0 && this.numeroContactos !== 1) {
        this.vibration.vibrate([400, 200, 400]);
        this.localNotifications.schedule({
          id: 1,
          title: this.contactos_encontrados,
          priority: 2,
          text: this.numeroContactos + ' ' + this.contacto_cercanos,
          sound: this.setSound(),
          icon: 'file://assets/img/appicon.png',
          led: '6600CC',
          data: { secret: 2 }
        });
      }
      if (this.numeroContactos !== 0 && this.numeroContactos === 1) {
        this.vibration.vibrate([400, 200, 400]);
        this.localNotifications.schedule({
          id: 1,
          title: this.contactos_encontrados,
          text: this.numeroContactos + ' ' + this.contacto_cercano,
          sound: this.setSound(),
          priority: 2,
          icon: 'file://assets/img/appicon.png',
          color: '6600CC',
          led: '6600CC',
          data: { secret: 2 }
        });
      }
      if (this.numeroContactos === 0 ) {
        this.vibration.vibrate([600, 300, 600]);
        this.localNotifications.schedule({
          id: 3,
          text: this.lo_sentimos,
          sound: this.errSound(),
          priority: 2,
          icon: 'file://assets/img/appicon.png',
          led: '6600CC',
          data: { secret: 2 }
        });
      } */

    }, 2000);
  }

  setSound() {
    if (this.platform.is('android')) {
      return 'file://assets/sounds/notifsound.mp3';
    } else {
      return 'file://assets/sounds/notifsound.mp3';
    }
  }
  errSound() {
    if (this.platform.is('android')) {
      return 'file://assets/sounds/notifsound.mp3';
    } else {
      return 'file://assets/sounds/notifsound.mp3';
    }
  }

  ngOnDestroy() {
  console.log('¡Agitar destruido!');
    this.subscription.unsubscribe();
  }
}

