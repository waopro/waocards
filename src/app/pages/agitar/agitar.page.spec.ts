import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgitarPage } from './agitar.page';

describe('AgitarPage', () => {
  let component: AgitarPage;
  let fixture: ComponentFixture<AgitarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgitarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgitarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
