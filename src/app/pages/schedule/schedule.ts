import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, IonList, LoadingController, ModalController, ToastController } from '@ionic/angular';

import { ScheduleFilterPage } from '../schedule-filter/schedule-filter';
import { ConferenceData } from '../../providers/conference-data';
import { UserData } from '../../providers/user-data';
import { TranslateService } from '@ngx-translate/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { Clasificados } from '../../providers/clasificados';
import { FeriasEventosService } from '../../providers/ferias-eventos.service';
import { AppComponent } from '../../app.component';
import * as urls from '../../providers/urls.servicios';

import { BusquedaFeriasEventosCiudad } from '../../interfaces/busqueda-ferias-eventos-ciudad';


@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
  styleUrls: ['./schedule.scss'],
})
export class SchedulePage implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  
  URL_waoLogo: string = 'assets/img/waoLogoSimbolo.PNG';
  // Gets a reference to the list element
  @ViewChild('scheduleList') scheduleList: IonList;
  @ViewChild('clasificadosList') clasificadosList: IonList;

  dayIndex = 0;
  cadenaBusquedaEventoCiudad = '';
  clasificadoQuery: string = ''; 
  segment = 'favorites';
  excludeTracks: any = [];
  shownSessions: any = [];
  groups: any = [];
  confDate: string;
  Clasificados: boolean;
  afin: string;
  general: string;
  trabajos: string;
  comidas: string; 
  URLAvatar: string = urls.URLimgClasificado; 
  formarImagenInicial: string = 'background-image: url("';
  formarImagenFinal: string = '");';
  // Datos necesarios para cargar la data
  completo: string;
  sinResultados: boolean;
  mensajesSvr: string;
  // Datos discriminatorios
   RefinarCategorias: string = 'Todos';
   RefinarPaises: string = 'Todo el mundo';
  Pequeno: any[] = [];
  Grande2: any[] = [];
  LargoV: any[] = [];
  ExtralargoH: any[] = [];
  GrandeXL: any[] = [];
  LargoH: any[] = [];
  clasificadoData:  any[]  = [];
  feriaEventoData:  any[]  = [];
  contreferencias: any[] = ['1'];
  paises: any[];
  ciudades: any[];
  banderasPaises:any[];
  URLBanderas: string = 'assets/img/banderas/';
  //categoriaSeleccionada: boolean = false;
  paisSeleccionado: boolean = false;
  RefinarCiudad:string;
  poster: string; 

  public stories = [
    {
      id: 1,
      img: 'assets/img/clasificados/afin.jpg',
      user_name: this.afin,
      funcion: 'filtrarafin()'
    },
    {
      id: 2,
      img: 'assets/img/clasificados/general.png',
      user_name: this.general,
      funcion: 'general()'
    },
    {
      id: 3,
      img: 'assets/img/clasificados/trabajo.png',
      user_name: this.trabajos,
      funcion: 'trabajos()'
    },
    {
      id: 4,
      img: 'assets/img/clasificados/comidas.png',
      user_name: this.comidas,
      funcion: 'comidas()'
    },
    {
      id: 5,
      img: 'https://avatars1.githubusercontent.com/u/1024025?v=3&s=120',
      user_name: 'linus_torvalds',
      funcion: 'filtrarafin()'
    }

  ];
  

  constructor(
    public alertPosterFE: AlertController,
    public confData: ConferenceData,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public router: Router,
    public toastCtrl: ToastController,
    public user: UserData,
    public translate: TranslateService,
    public clasificados: Clasificados,
    public feriasEventos: FeriasEventosService,
    public AppComponente: AppComponent
    //public localNotifications: LocalNotifications
  ) {
    //this.Clasificados = true;
    this.cargar_slide_texto();
    if(!this.Clasificados)
      this.cargarFeriasEventos();
    else
      this.cargarClasificados();
   }
   

  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.contreferencias.length <= 3) {
        this.contreferencias.push('1');
      }
      if (this.contreferencias.length === 3) {
        event.target.disabled = true;
        this.contreferencias.push('1');
      }
    }, 2000);
  }
   
  refrescarClasificados(event) {
   
    this.clasificadoData = [null];
    
    setTimeout(() => {
      //this.constructor();
       this.cargarClasificados();
      event.target.complete();
    }, 2500);
  }

  refrescarFeriasEventos(event){
    this.clasificadoData = [null];
    this.sinResultados = false;
    
    setTimeout(() => {
      //this.constructor();
       this.cargarFeriasEventos();
      event.target.complete();
    }, 2500);
  }

  cargarClasificados(){
    
    this.sinResultados = false;

    //while(this.clasificadoData.length > 0) {
      this.clasificadoData.pop();
      this.clasificadoData.length = 0;
      this.Pequeno.pop();
      this.Pequeno.length=0;
      this.Grande2.pop();
      this.Grande2.length=0;
      this.LargoV.pop();
      this.LargoV.length=0;
      this.ExtralargoH.pop();
      this.ExtralargoH.length=0;
      this.GrandeXL.pop();
      this.GrandeXL.length=0;
      this.LargoH.pop();
      this.LargoH.length=0;
  //}
  
    this.completo='false';
    setTimeout(() => {
      this.clasificados.load().subscribe((clasificado: any['records']) => {
        this.clasificadoData = clasificado.records;
       console.log(this.clasificadoData);
       this.completo = 'true';
      }, (err: any) => {
        this.completo = 'error';
        console.log('error: ',this.completo);
          this.AppComponente.presentarToastError();
      }
      );    
    }, 500);
         
    setTimeout(() => {        
    this.discriminarTipo(this.clasificadoData);
    }, 2000);
  }

  cargarFeriasEventos(){
    
    
    console.log(this.sinResultados);
    //if((this.feriaEventoData.length > 0) || (this.Clasificados == false)) {
      console.log('VACIANDO');
      this.feriaEventoData.pop();
      this.feriaEventoData.length = 0;
      this.Pequeno.pop();
      this.Pequeno.length=0;
      this.Grande2.pop();
      this.Grande2.length=0;
      this.LargoV.pop();
      this.LargoV.length=0;
      this.ExtralargoH.pop();
      this.ExtralargoH.length=0;
      this.GrandeXL.pop();
      this.GrandeXL.length=0;
      this.LargoH.pop();
      this.LargoH.length=0;

      this.sinResultados = false;
  //}
  
    this.completo='false';
    setTimeout(() => {
      //Se cargan ferias y eventos de manera aleatoria
      this.feriasEventos.load().subscribe((feriaseventos: any['records']) => {
        console.log(feriaseventos);
        this.feriaEventoData = feriaseventos['feriasEventosAleatorios'];
       console.log(this.feriaEventoData);
       //this.completo = 'true';
      }, (err: any) => {
        this.completo = 'error';
        console.log('error: ',this.completo);
          this.AppComponente.presentarToastError();
      }
      );
      //Se cargan los paises donde hay eventos o ferias
      this.feriasEventos.loadPaises().subscribe((paises: any['records']) => {
        console.log(paises);
        this.paises= paises['paises'];
        //this.banderasPaises = paises['imagenPais'];
       console.log(this.paises);
       this.completo = 'true';
      }, (err: any) => {
        this.completo = 'error';
        console.log('error: ',this.completo);
          this.AppComponente.presentarToastError();
      }
      );
      
    }, 500);
         
    setTimeout(() => {        
    this.discriminarTipo(this.feriaEventoData);
    }, 2000);
  }

  discriminarTipo(publicacion){
    
    for (let i of publicacion) {
      if(i.Tipo==='Pequeño') {
        this.Pequeno.push(i);
        
        console.log('los pequeños: ', i.Tipo);
        console.log(this.Pequeno); 
        
      }
      if(i.Tipo==='Grande2') {
        this.Grande2.push(i);
        console.log('los Grande2: ', i.Tipo);
        console.log(this.Grande2);  
      }
      if(i.Tipo==='LargoV') {
        this.LargoV.push(i);
        console.log('los LargoV: ', i.Tipo);
        console.log(this.LargoV);  
      }
      if(i.Tipo==='ExtralargoH') {
        this.ExtralargoH.push(i);
        console.log('los extralargoH: ', i.Tipo);
        console.log(this.ExtralargoH);  
      }
      if(i.Tipo==='GrandeXL') {
        this.GrandeXL.push(i);
        console.log('los GrandeXL: ', i.Tipo);
        console.log(this.GrandeXL);  
      }
      if(i.Tipo==='LargoH') {
        this.LargoH.push(i);
        console.log('los LargoH: ', i.Tipo);
        console.log(this.LargoH);  
      }
   }
   //console.log(this.Pequeno);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }
  ngOnInit() {
    // this.app.setTitle('Schedule');
    this.updateSchedule();
    this.Clasificados = false;
    console.log('LA VARIABLE CLASIFICADOS EN ngOnInit() ES: ');
    console.log(this.Clasificados);
  }

  cargar_slide_texto() {
    setTimeout(() => {
      this.stories = [
        {
          id: 1,
          img: 'assets/img/clasificados/afin.jpg',
          user_name: this.afin,
          funcion: 'filtrarafin()'
        },
        {
          id: 2,
          img: 'assets/img/clasificados/general.png',
          user_name: this.general,
          funcion: 'filtrarGeneral()'
        },
        {
          id: 3,
          img: 'assets/img/clasificados/trabajo.png',
          user_name: this.trabajos,
          funcion: 'filtrarafin()'
        },
        {
          id: 4,
          img: 'assets/img/clasificados/comidas.png',
          user_name: this.comidas,
          funcion: 'filtrarafin()'
        },
        {
          id: 5,
          img: 'https://avatars1.githubusercontent.com/u/1024025?v=3&s=120',
          user_name: 'linus_torvalds',
          funcion: 'filtrarafin()'
        }

      ];
    }, 800);

  }

  updateSchedule() {
    // Close any open sliding items when the schedule updates
    this.Clasificados=!this.Clasificados;
    console.log('LA VARIABLE CLASIFICADOS EN updateSchedule() ES: ');
    console.log(this.Clasificados);
    if (this.scheduleList) {
      this.scheduleList.closeSlidingItems();
    }

    this.confData.getTimeline(this.dayIndex, this.cadenaBusquedaEventoCiudad, this.excludeTracks, this.segment).subscribe((data: any) => {
      this.shownSessions = data.shownSessions;
      this.groups = data.groups;
    });
  }

  //FUNCIÓN PARA ACTUALIZAR LITADO DE CLASIFICADOS
  /*
  updateClasificados() {
    // Close any open sliding items when the schedule updates
    if (this.scheduleList) {
      this.scheduleList.closeSlidingItems();
    }

    this.confData.getTimeline(this.dayIndex, this.cadenaBusquedaEventoCiudad, this.excludeTracks, this.segment).subscribe((data: any) => {
      this.shownSessions = data.shownSessions;
      this.groups = data.groups;
    });
  }
  */
  ////////////////////////////////////////////////
  ////////////////////////////////////////////////
  ///////////////////////////////////////////////
  ////////////////////////////////////////////////
  ////////////////////////////////////////////////
  ///////////////////////////////////////////////
  filtrarafin() {
   //console.log('Afin Seleccionado');
    setTimeout(()=>{
      this.RefinarCategorias='Afin';
      console.log(this.RefinarCategorias);
    }, 500);
  }

  filtrargeneral() {
    //console.log('General Seleccionado');
    setTimeout(()=>{
      this.RefinarCategorias='General';
      console.log(this.RefinarCategorias);
    }, 550);
   }

  filtrartrabajos(){
    //console.log('Trabajos Seleccionado');
    setTimeout(()=>{
      this.RefinarCategorias='Trabajo';
      console.log(this.RefinarCategorias);
    }, 550);
  }

  filtrartrcomida(){
    //console.log('Comidas Seleccionado');
    setTimeout(()=>{
      this.RefinarCategorias='Comidas';
      console.log(this.RefinarCategorias);
    }, 550);
  }

  filtrartodos(){
    //console.log('Todos Seleccionado');
    setTimeout(()=>{
      this.RefinarCategorias='Todos';
      console.log(this.RefinarCategorias);
    }, 550);
  }
///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
///////////////////////////////////////////////
  async presentFilter() {
    
    if (this.Clasificados === true) {
      const modal = await this.modalCtrl.create({
        component: ScheduleFilterPage,
        componentProps: {
          pauta: 'Clasificado'
        }
      });
      await modal.present(); 
    } 
    else {


      const modal = await this.modalCtrl.create({
        component: ScheduleFilterPage,
        componentProps: {
          pauta: 'Feria_Evento'
        }
      });
      await modal.present(); 
      /*
      const modal = await this.modalController.create({
        component: DetallePersonaEncontradaPage,
        componentProps: {
          IDEncontrado: idusr,
          solicitante: 'false'
        }
      });
      modal.showBackdrop = true;
      modal.backdropDismiss = true;
      return await modal.present();
      */    
    }
  }

 

  async presentarPosterFE(posterFE) {
    console.log(posterFE);

    this.poster = this.feriaEventoData[0]['imagen'];
    const alert = await this.alertPosterFE.create({
      //header: title,
      message: "<img no-margin no-padding class='poster' src=" + urls.URLimgPosterFE + posterFE +">",
      cssClass: 'alertaPoster'
      //message: this.feriaEventoData[0]['imagen'],
      //message: urls.URLimgPosterFE + this.poster,
      //buttons: ['OK']
    });
    // now present the alert on top of all other content
    await alert.present();
  }

  buscarEventoCiudad(){

    //while(this.feriaEventoData.length > 0) {
      this.feriaEventoData.pop();
      this.feriaEventoData.length = 0;
      this.Pequeno.pop();
      this.Pequeno.length=0;
      this.Grande2.pop();
      this.Grande2.length=0;
      this.LargoV.pop();
      this.LargoV.length=0;
      this.ExtralargoH.pop();
      this.ExtralargoH.length=0;
      this.GrandeXL.pop();
      this.GrandeXL.length=0;
      this.LargoH.pop();
      this.LargoH.length=0;
  //}

    this.presentLoading();
      setTimeout(() => {
        this.feriasEventos.buscarFeriasEventosCiudad(this.cadenaBusquedaEventoCiudad)
      .subscribe((RTABusqueda: BusquedaFeriasEventosCiudad)=>{
        console.log(RTABusqueda);
        if(RTABusqueda.FEEncontrados === true){

          this.feriaEventoData = RTABusqueda.feriasEventosCiudad;
          this.completo = 'true';
          this.sinResultados = false;
        }
        else {
          console.log(RTABusqueda.mensaje);
          this.sinResultados = true;
          this.completo = 'false';
          this.mensajesSvr = RTABusqueda.mensaje;
        }
        
       this.discriminarTipo(this.feriaEventoData);
      });
    }, 2000);
  }

  async presentLoading() {
    const loading = await this.loadingCtrl.create({
      message: 'Buscando ferias y eventos en ' + this.cadenaBusquedaEventoCiudad,
      duration: 2000
    });
    await loading.present()/*.then(() => {
      console.log('cargando');
      if(this.PEncontradas != null)
      {
        loading.dismiss(); 
      }
      
    })*/;

    const { role, data } = await loading.onDidDismiss();

    //console.log('Loading dismissed!');
  }

  filtrarCiudadesEventosPais(pais:string){
    console.log(pais);
    console.log(this.paises);
    setTimeout(()=>{
      if(pais === 'todos'){
        
        this.RefinarPaises = 'Todo el mundo';
      }
      else{
        this.RefinarPaises = pais;
        for (let nacion = 0; nacion < this.paises.length; nacion++) {
           
            if (pais === this.paises[nacion].Pais) {
              this.ciudades = this.paises[nacion].Ciudades;
              this.RefinarCiudad = this.ciudades[0].ciudad_municipio;      
            }
        }
        this.paisSeleccionado = true;
        
        console.log(this.RefinarCiudad);
        console.log(this.ciudades[0].ciudad_municipio);
      }
        
      
    }, 550);
    
  }

  filtrarCiudadesEventosPaisCiudad(ciudad:string){
    console.log(ciudad);
    console.log(this.RefinarCategorias);
    setTimeout(()=>{
      this.RefinarCiudad = ciudad;
      this.cadenaBusquedaEventoCiudad = ciudad;
      this.buscarEventoCiudad();
    }, 550);
    
  }

  irAtrasCiudades(){
    this.RefinarPaises = 'Todo el mundo';
    this.paisSeleccionado = false;
    this.cargarFeriasEventos();
  }



 /*  async notificacion(){
    this.localNotifications.schedule([{
      id: 1,
      text: '¡Bienvenido a WAOCards!',
      sound: isAndroid ? '../../../assets/sounds/notifsound.mp3' : 'file://beep.caf',
      data: { secret: key }
    }, {
      id: 2,
      title: 'Local ILocalNotification Example',
      text: 'Multi ILocalNotification 2',
      icon: 'http://example.com/icon.png'
    }]);
  } */
}
