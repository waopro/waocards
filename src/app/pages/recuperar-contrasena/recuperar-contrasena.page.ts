import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';
import { Router } from '@angular/router';

@Component({
  selector: 'recuperar-contrasena',
  templateUrl: './recuperar-contrasena.page.html',
  styleUrls: ['./recuperar-contrasena.page.scss'],
})
export class RecuperarContrasenaPage implements OnInit {
  campcontrasena: string;
  contrasenaIngresada = false;
  pinIngresado = false;
  correoEnviado = false;
  campcorreo: string;
  campPIN: string;
  campID: string;
  constructor(
    public alertController: AlertController,
    public loadingController: LoadingController,
    public registro: RegistroUsuariosService,
    public valor: Valores,
    public router: Router
  ) { }

  ngOnInit() {
  }

  ingresoContrasena() {
    this.registro.GuardarContrasena(this.campcontrasena, this.campID);
    //this.contrasena = false;
    if (this.registro.verificacionContrasena === false) {

      this.presentLoadingWithOptions(2800);
      this.registro.contrasenaCambiada('1', this.campID);
      setTimeout(() => {
       // this.carga_completa = true;
        this.contrasenaIngresada = true;
        this.presentarConfirmado();
        this.router.navigateByUrl('/login');
      }, 3000);
    }
  }

  async mostrarAlertConfirmacion() {
    console.log('se dispara correcto');
    const alert = await this.alertController.create({
      header: 'Confirmar',
      message: 'Se enviará a <strong>' + this.campcorreo +
                '</strong> un pin de confirmación para proceder a cambiar la contraseña olvidada',
      buttons: [{
        text: 'Confirmar',
        role: 'confirmar',
        cssClass: 'secondary',
        handler: (blah) => {
          // console.log('Confirm Cancel: blah');
          this.presentLoadingWithOptions(3000);
          setTimeout(() => {
            this.generarPIN();
          }, 1000);
          setTimeout(() => {
            this.correoEnviado = true;
          }, 2800);
        }
      }]
    });
    alert.present();
  }

  async presentarConfirmado() {
    console.log('se dispara correcto');
    const alert = await this.alertController.create({
      header: '¡Contraseña cambiada correctamente!',
      message: 'Se han cambiado la contraseña correctamente, puede iniciar sesión',
      buttons: ['Continuar']
    });
    alert.present();
  }

  generarPIN() {
    this.registro.generarPIN(this.campcorreo, this.campID);
  }

  verificarPin() {
    setTimeout(() => {

      if (this.registro.PINLlegado.mensaje == 'PIN Incorrecto') {
        this.pinIngresado = false;
      }

      if (this.registro.PINLlegado.mensaje == 'PIN Correcto') {
        this.pinIngresado = true;
      }

    }, 700);
  }

  ingresoPIN() {
    this.registro.confirmarPIN(this.campPIN, this.campID);
    this.verificarPin();
    this.presentLoadingWithOptions(700);
  }


  async presentLoadingWithOptions(tiempo) {
    const loading = await this.loadingController.create({
      duration: tiempo,
      message: 'Un momento, por favor...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }
}
