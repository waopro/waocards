import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';
import * as urls from '../../providers/urls.servicios';
import { TranslateService } from '@ngx-translate/core';
import { DetallePersonaEncontradaPage } from '../detalle-persona-encontrada/detalle-persona-encontrada.page';
import { Ubicacion } from '../../providers/ubicacion';



@Component({
  selector: 'listado-amigos',
  templateUrl: './listado-amigos.page.html',
  styleUrls: ['./listado-amigos.page.scss'],
})
export class ListadoAmigosPage implements OnInit {
  @Input() identificacion: string;
  @Input() user: string;
  contactos: any = [{  }];
  URLavatar: string;
  _translate: TranslateService;
  verHojaVida: string;
  telefon: string;
  profession: string;


  constructor(private modalController: ModalController,
    private registro: RegistroUsuariosService,
    private valor: Valores,
    private ubicacion: Ubicacion) {

  }

  ngOnInit() {
    this.registro.VerAmigos(this.identificacion, this.ubicacion.latitud, this.ubicacion.longitud);
    setTimeout(() => {
      console.log(this.registro.Amigos);
      this.contactos = this.registro.Amigos;
      console.log('replica de registro ' +  this.valor.data);
    }, 400);
    this.URLavatar = urls.URLimagen;
    this.cargarIdiomaElementos();
    this.acotarNombre();

  }
  // las traducciones que existen se ponen en caso de ser necesario
  cargarIdiomaElementos() {
    // this._translate.stream('PAGES.SPEAKER-LIST.ABRIR_CV').subscribe((text: string) => {
    //   this.verHojaVida = text;
    // });
    // this._translate.stream('PAGES.SPEAKER-LIST.TELEFONO').subscribe((text: string) => {
    //   this.telefon = text;
    // });
    // this._translate.stream('PAGES.SPEAKER-LIST.CARGO/PROFESION').subscribe((text: string) => {
    //   this.profession = text;
    // });
  }

  dismiss(data?: boolean) {

    this.modalController.dismiss(data);
  }

  async presentModal(ID: string) {
    const modal = await this.modalController.create({
      component: DetallePersonaEncontradaPage,
      componentProps: {
        'IDEncontrado': ID, // this.identificacion
        'solicitante': 'false'
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
  }

  acotarNombre() {
    const letra = ' ';
    for (let i = 0; i < this.user.length; i++ ) {
      if (letra.indexOf(this.user.charAt(i)) != -1) {
        this.user = this.user.substr( 0 , i );
      }
    }


  }

}

