import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportarContactosPage } from './importar-contactos.page';

describe('ImportarContactosPage', () => {
  let component: ImportarContactosPage;
  let fixture: ComponentFixture<ImportarContactosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportarContactosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportarContactosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
