import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Contactos } from '../../providers/contactos';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts/ngx';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';
import * as urls from '../../providers/urls.servicios';
import { LoadingController } from '@ionic/angular';
import { DetallePersonaEncontradaPage } from '../detalle-persona-encontrada/detalle-persona-encontrada.page';
import { ModalController } from '@ionic/angular';
import { Paises } from '../../providers/paises';


@Component({
  selector: 'importar-contactos',
  templateUrl: './importar-contactos.page.html',
  styleUrls: ['./importar-contactos.page.scss'],
})
export class ImportarContactosPage implements AfterContentInit {

  libreta: any = [];
  listaContactos: any = [];
  NuevaListaContactos: any = [];
 /*  public listaContactos: any = [{
    displayName: '',
    phoneNumbers: ''
  }]; */
  cargaCompleta = false;
  avatar = urls.URLimagen;
  public numerosTelefono: any = [];
  constructor(public contactos: Contactos,
              public contacts: Contacts,
              public registro: RegistroUsuariosService,
              public valor: Valores,
              public loadingController: LoadingController,
              public modalController: ModalController,
              public paises: Paises) {
   
   }


 ngAfterContentInit() {
   setTimeout(() => {

     this.cargarLibreta();
   }, 500);
 }

  async verPerfil(Identificacion) {
    console.log('Entra al perfil async de: ' + Identificacion);
    const modal = await this.modalController.create({
      component: DetallePersonaEncontradaPage,
      componentProps: {
        IDEncontrado: Identificacion,
        solicitante: 'false'
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
  } 

  cargarLibreta() {
      this.contacts.find(['displayName', 'name', 'phoneNumbers'], { filter: "", multiple: true })
        .then(data => {

          this.listaContactos = data;
          this.libreta =data;

          console.log(data);
          console.log('La libreta es:');
          console.log(this.libreta);
          console.log('listaContactos es:');
          console.log(this.listaContactos);
        });

    this.presentLoadingWithOptions();
    setTimeout(() => {
     // this.imprimirContactos();
      //this.contarContactos();
      this.evaluarcodigos();
      this.generaryEnviarNumeros();
       this.cargaCompleta = true;
    }, 3000);
  }

  contarContactos(){
    var ccnt = 0;
    var cccnntt = 0;
    this.listaContactos.forEach(l => {
      if (l.phoneNumbers !== null) {
      console.log(l.phoneNumbers[0].value);
      ccnt++;
      console.log('ccnt:', ccnt);
      }
      if (l.phoneNumbers === null){
        cccnntt++;
        console.log('cccnntt:', cccnntt);
      }
    });
  }

  evaluarcodigos() {
    var conta=0;
   // var contnull=0;
    for (var i = 0; i < this.paises.paises.length; i++) {
      for (var j = 0; j < this.listaContactos.length; j++) {
        if (this.listaContactos[j].phoneNumbers !== null) {
          if (this.listaContactos[j].phoneNumbers[0].value !== null || this.listaContactos[j].phoneNumbers[0].value !== undefined) {
            if (this.listaContactos[j].phoneNumbers[0].value.indexOf(this.paises.paises[i].dial_code) !== -1) {
              console.log('dial code:', this.paises.paises[i].dial_code);
              console.log(this.listaContactos[j].phoneNumbers[0].value.split(this.paises.paises[i].dial_code).join(''));
              this.NuevaListaContactos.push(this.listaContactos[j].phoneNumbers[0].value.split(this.paises.paises[i].dial_code).join(''));
              this.listaContactos.splice(j, 1);
              console.log('indice: ', j);
              j = 0;
              conta++;
              console.log('Contador: ', conta);
            }
          }
        } 
       }
    }
   // console.log(this.listaContactos);

     this.listaContactos.forEach(k => {
        if (k.phoneNumbers !== null) {
          if (k.phoneNumbers[0].value !== null || k.phoneNumbers[0].value !== undefined) {
            this.NuevaListaContactos.push(k.phoneNumbers[0].value);
        }
      }
      });

      console.log('LA NUEVA LISTA DE CONTACTOS...');
    console.log(this.NuevaListaContactos);
     // console.log(this.listaContactos);
  }

  async generaryEnviarNumeros() {
    var cont = 0;
    var replaced;
    setTimeout(() => {
      console.log('La data que llego lista de contactos');
      console.log(this.NuevaListaContactos);
      for (var m = 0; m < this.NuevaListaContactos.length; m++) {
        replaced = this.NuevaListaContactos[m].split(' ').join('');
        this.numerosTelefono.push(replaced);
        console.log(cont);
        cont++;
      }



      /* this.NuevaListaContactos.forEach(element => {
        console.log('foreach');
        if (element.phoneNumbers !== undefined){
        if (element.phoneNumbers !== null) {
          console.log('entró al coso if', cont);
          if (element.phoneNumbers[0].value !== null || element.phoneNumbers[0].value !== undefined) {
            console.log('se cumple la condicion phoneNumbers');
            console.log(element.phoneNumbers[0].value);
            replaced = element.phoneNumbers[0].value.split(' ').join('');
            this.numerosTelefono.push(replaced);
            console.log(cont);
            cont++;
          }
        }
      }

    }, 1000); */
    });
   // this.numerosTelefono = this.numerosTelefono.unique();
    setTimeout(() => {
      console.log('Los telefonos: ');
      console.log(this.numerosTelefono);
      this.registro.enviarNumeros(this.numerosTelefono, this.valor.identificacion);
      setTimeout(() => {
        this.listarlosContactos();
      }, 1500);
    }, 200);
  }

  listarlosContactos() {
  this.cargaCompleta = true;
  }

  imprimirContactos() {
    console.log(this.listaContactos);
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      duration: 8000,
      message: 'Un momento, por favor',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

}
