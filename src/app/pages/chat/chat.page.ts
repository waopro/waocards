import { Component, Input, OnInit, AfterContentInit, ɵConsole, ViewChild } from '@angular/core';
import * as urls from '../../providers/urls.servicios';
import { ModalController, ActionSheetController, IonInfiniteScroll } from '@ionic/angular';
import { UserData } from '../../providers/user-data';
import { WAOChatService } from '../../providers/waochat.service';
import { MensajeWAOChat } from '../../interfaces/mensaje-waochat';
import { Valores } from '../../providers/valores';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
// import { IonInfiniteScroll } from '@ionic/angular';
import { Events } from '@ionic/angular';
import { PDFService } from '../../providers/pdf.service';


@Component({
  selector: 'chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit, AfterContentInit {

  @Input() speakerName: string;
  @Input() speakerID: string;
  @Input() speakerPhoto: string;
  @ViewChild(IonInfiniteScroll)

  mensaje: string = '';
  mensajes1: MensajeWAOChat[] = [];
  mensajes2: MensajeWAOChat[] = [];
  URLavatar: string = urls.URLimagen;
  limiteMensajes: number = 20;
  firstTime1  = true;
  firstTime2 = true;
  ultNumeroMsg: number;
  cargaCompleta = false;
  mensajesOrganizados: MensajeWAOChat[] = [];
  mostrar_boton_moveralfinal = false;
  // data: any[]= Array(20);

  fototomada  = false;
  PDFusado = false;
  nombrearchivoPDF: any;
  nombrearchivoIMG: any;

  conversation = [];

  constructor(
    public modalCtrl: ModalController,
    public userData: UserData,
    public waoChat: WAOChatService,
    public valor: Valores,
    public actionsheet: ActionSheetController,
    public camera: Camera,
    public pdf: PDFService
  ) {

  }

   ngOnInit() {
     this.waoChat.cargarmensajes1( this.speakerID,
                                  this.valor.identificacion,
                                  this.limiteMensajes).subscribe((mensajes: MensajeWAOChat[]) => {

       console.log(this.limiteMensajes);
       console.log(mensajes);
       if (mensajes.length !== 0) {
         if (this.firstTime1) {
          this.mensajes1 = mensajes;
           this.firstTime1 = false;
         }
         else if (mensajes[0].imagenMsg) {
           this.conversation.push(
             // tslint:disable-next-line: max-line-length
             { text: mensajes[0].mensaje, sender: mensajes[0].nombre.substr(0, mensajes[0].nombre.indexOf(' ')), image: this.URLavatar + mensajes[0].UsrFoto, imagenMensaje: urls.fotosChat + '/' + mensajes[0].imagenMsg }
           );
         }
         else{
           this.conversation.push(
             // tslint:disable-next-line: max-line-length
             { text: mensajes[0].mensaje, sender: mensajes[0].nombre.substr(0, mensajes[0].nombre.indexOf(' ')), image: this.URLavatar + mensajes[0].UsrFoto }
           );
         }

         console.log('holaaaaaaaaaaaaaaa');
         console.log(this.conversation);

         // this.ultNumeroMsg = mensajes[0].numeroMsg;

       }
       else {
        // tslint:disable-next-line: max-line-length
        console.log('no existe conversasión estre estos presonajes, por lo tanto se deb proceder a crearlo en la base de datos de firebase');
         this.waoChat.iniciarChat(this.speakerID, this.valor.identificacion, this.userData.datos.Nombre, 0);
       }

       setTimeout(() => {
         this.scrollToBottom();
       }, 100);
     });

     this.waoChat.cargarmensajes2(this.speakerID, this.valor.identificacion, this.limiteMensajes).subscribe((mensajes: MensajeWAOChat[]) =>{
       console.log(this.limiteMensajes);
       console.log(mensajes);
       if (mensajes.length !== 0) {
         if (this.firstTime2) {

           this.mensajes2 = mensajes;
           this.firstTime2 = false;
         }
         else if (mensajes[0].imagenMsg) {
           this.conversation.push(
             // tslint:disable-next-line: max-line-length
             { text: mensajes[0].mensaje, sender: mensajes[0].nombre.substr(0, mensajes[0].nombre.indexOf(' ')), image: this.URLavatar + mensajes[0].UsrFoto, imagenMensaje: urls.fotosChat + '/' + mensajes[0].imagenMsg }
           );
         }
         else {
           this.conversation.push(
             // tslint:disable-next-line: max-line-length
             { text: mensajes[0].mensaje, sender: mensajes[0].nombre.substr(0, mensajes[0].nombre.indexOf(' ')), image: this.URLavatar + mensajes[0].UsrFoto }
           );
         }

         console.log('holaaaaaaaaaaaaa22222ioni');
         console.log(this.conversation);
         // this.ultNumeroMsg = mensajes[0].numeroMsg;

       }
       else {
         // tslint:disable-next-line: max-line-length
         console.log('no existe conversasión estre estos presonajes, por lo tanto se deb proceder a crearlo en la base de datos de firebase');
         this.waoChat.iniciarChat(this.speakerID, this.valor.identificacion, this.userData.datos.Nombre, 1);
        }

       setTimeout(() => {
         this.scrollToBottom();
       }, 100);
     });

     // this.limiteMensajes = 1;
     console.log('ESTÁ PASANDO POR ACÁ CADA QUE SE ACTUALIZA LA BASE DE DATOS DE FIREBASE????');
   }

   ngAfterContentInit() {
     this.pdf.DescargandoPDFBandera = false;
     setTimeout(() => {
       this.organizarMensajes(this.mensajes1, this.mensajes2);
       console.log('VA A HACER UN SCROLL EN EL AFTERCONTENTINIT()');
       }, 2000);

     setTimeout(() => { this.scrollToBottom(); }, 3000);
   }

  enviar() {
    if (this.mensaje !== '' && this.fototomada === false && this.PDFusado === false) {// solo envia texto
      console.log('Se enviará texto solamente');
      // this.mensaje = '';
      this.waoChat.enviarMensaje(this.speakerID, this.valor.identificacion, this.userData.datos.Nombre, this.mensaje);
      this.mensaje = '';
    }
    else if (this.fototomada === true) {// envia imagen con o sin texto
      // if(this.mensaje === undefined) {this.mensaje = ' '}
      this.nombrearchivoIMG = this.valor.identificacion + '_' + String(new Date().getTime()) + '.jpg';
      console.log('Se enviará una imagen');
      this.mensaje = '';
      this.waoChat.enviarMensaje(this.speakerID, this.valor.identificacion, this.userData.datos.Nombre, this.mensaje, this.nombrearchivoIMG , '');
      this.mensaje = '';
      this.fototomada = false;
    }
    else if (this.PDFusado === true) {// envia PDF con o sin texto
      // if(this.mensaje === undefined) {this.mensaje = ' '}
      console.log('Se enviará un PDF');
      this.nombrearchivoPDF =  this.valor.identificacion + '_' + String(new Date().getTime()) + '.pdf';
      this.mensaje = '';
      this.waoChat.enviarMensaje(this.speakerID, this.valor.identificacion, this.userData.datos.Nombre, this.mensaje, '', this.nombrearchivoPDF);
     this.subePDF();
      this.mensaje = '';
      this.PDFusado = false;
    }

  }

  enviarImagen(){

    this.waoChat.galeriaImagen().then(
      imageData => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.waoChat.fotografia = 'data:image/jpeg;base64,' + imageData;
        // console.log(imageData);
        this.fototomada = true;
      },
      err => {
        // Handle error
        this.fototomada = false;
      }
    );

  }

  seleccionaPDF() {
    this.PDFusado = false;
    this.pdf.seleccionarArchivoWaoChat()
      .then(uri => {
        this.pdf.PDF = uri;
          this.pdf.RutaPDFSeleccionada = true;
          this.PDFusado = true;
          console.log('Ubicación del PDF Seleccionada', this.pdf.PDF);
      })
      .catch(e => console.log(e));
  }

  subePDF() {
    setTimeout(() => {
      console.log('Enviar PDF:', this.pdf.PDF, this.nombrearchivoPDF);
      this.pdf.SubirPDFWaoChat(this.pdf.PDF, this.nombrearchivoPDF)
        .then(
          data => {
            this.pdf.ResultadoSubida = data;
            console.log(data);
            setTimeout(() => {
              // this.loading.dismiss();
              if (this.pdf.ResultadoSubida.response == '"Correcto"') {
                // this.pdf.presentarResultadoPDF('Documento subido correctamente');
                this.PDFusado = true;
              }
              if (this.pdf.ResultadoSubida.response == '"Error"') {
                this.pdf.presentarResultadoPDF('Hay problema con el servidor, intente más adelante');
              }
              if (this.pdf.ResultadoSubida.response == '"Incorrecto"') {
                this.pdf.presentarResultadoPDF('Documento inválido, revise si el documento tiene problemas');
              }
            }, 800);
          },
          err => {
            console.log(err);
            // alert('Incorrecto');
          }
        );
    }, 700);
  }



  organizarMensajes(msgs1: MensajeWAOChat[], msgs2: MensajeWAOChat[]) {
    if (msgs1 !== [] && msgs2 !== []) {
      let msgsdesorganizados: MensajeWAOChat[] = [];
      var fechas: number[] = [];
      //  console.log("CONCATENADOS: ", msgs1.concat(msgs2));
      // console.log('ENTRO A ORGANIUZAR LOS MENSAJES');
      msgsdesorganizados = [].concat(msgs2, msgs1);

      for (var i = 0; i < msgsdesorganizados.length; i++) {
        fechas.push(msgsdesorganizados[i].fecha);
      }

      fechas.sort();

      for (var i = 0; i < fechas.length; i++) {
        for (var k = 0; k < msgsdesorganizados.length; k++) {
          if (msgsdesorganizados[k].mensaje !== null) {
            if (fechas[i] === msgsdesorganizados[k].fecha && msgsdesorganizados[k].mensaje !== '') {
              this.mensajesOrganizados.push(msgsdesorganizados[k]);
            }
          }
        }
      }

      for (let msg = 0 ; msg < this.mensajesOrganizados.length; msg++) {
        if(this.mensajesOrganizados[msg].imagenMsg) {
          this.conversation.push(
            // tslint:disable-next-line: max-line-length
            { text: this.mensajesOrganizados[msg].mensaje, sender: this.mensajesOrganizados[msg].nombre.substr(0, this.mensajesOrganizados[msg].nombre.indexOf(' ')), image: this.URLavatar + this.mensajesOrganizados[msg].UsrFoto, imagenMensaje: urls.fotosChat + '/' + this.mensajesOrganizados[msg].imagenMsg }
          );
        }
        else if (this.mensajesOrganizados[msg].PDFMsg) {
          this.conversation.push(
            // tslint:disable-next-line: max-line-length
            { text: this.mensajesOrganizados[msg].mensaje, sender: this.mensajesOrganizados[msg].nombre.substr(0, this.mensajesOrganizados[msg].nombre.indexOf(' ')), image: this.URLavatar + this.mensajesOrganizados[msg].UsrFoto, PDFMensaje: urls.PDFChat + '/' + this.mensajesOrganizados[msg].PDFMsg }
          );
        }
        else {
          this.conversation.push(
            // tslint:disable-next-line: max-line-length
            { text: this.mensajesOrganizados[msg].mensaje, sender: this.mensajesOrganizados[msg].nombre.substr(0, this.mensajesOrganizados[msg].nombre.indexOf(' ')), image: this.URLavatar + this.mensajesOrganizados[msg].UsrFoto }
          );
        }
      }

      // this.conversation = [].concat(this.mensajesOrganizados);
      this.cargaCompleta = true;
      console.log('mensajes REUNIDOS:', msgsdesorganizados);
      console.log('"FECHAS"', fechas);
      console.log('"MENSAJES ORGANIZADOS: "', this.conversation);

    }/*
    else if (msgs1 !== []){

    }
    else if (msgs2 !== []){

    }
    */
  }

  dismiss(data?: boolean) {
    // using the injected ModalController this page
    // can "dismiss" itself and pass back data
    this.modalCtrl.dismiss(data);
    // this.suscritoChat = false;
    console.log('chat cerrado', data);
  }

  scrollToBottom() {
    let content = document.getElementById("chat-container");
    console.log('EL OFFHEIGHT ES: ', content.offsetHeight);
    if(content.offsetHeight !== null){

      let parent = document.getElementById("chat-parent");
      let scrollOptions = {
        left: 0,
        top: content.offsetHeight
      }

      parent.scrollTo(scrollOptions);
    }

  }


  // esta funcion crea un menu con opcion de tomar foto o subir foto
  async presentActionSheet() {
    const actionSheet = await this.actionsheet.create({

      buttons: [

      {
        text: 'Subir foto',
          icon: 'camera',
        handler: () => {
          console.log('vamos a subir una foto');
          this.enviarImagen();
        }
      }, {
        text: 'Tomar foto',
          icon: 'aperture',
        handler: () => {
          console.log('vamos a tomar una foto');
          this.tomarFoto();
        }
        }, {
          text: 'Subir documento en PDF',
          icon: 'document',
          handler: () => {
            console.log('vamos a subir un PDF');
            this.seleccionaPDF();
          }
        }
    ]
    });
    await actionSheet.present();
  }
  tomarFoto() {
    this.waoChat.TomarFotoWaoChat().then(
      imageData => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.waoChat.fotografia = 'data:image/jpeg;base64,' + imageData;
        this.fototomada = true;
        // setTimeout(() => {
        //   //this.SubirFotoWaoChat(imgName);
        //   this.fototomada = true;
        // }, 500);

      },
      err => {
        this.fototomada = false;
        // Handle error
      }
    );
  }

  cargar_mensajes_anteriores() {

    this.waoChat.cargar_mensajes_anteriores(document.getElementById("chat-container").scrollTop);

    }

cargar_mensajes_finales() {
  // this.waoChat.cargar_mensajes_finales(document.getElementById ("chat-container").scrollToBottom);
  this.scrollToBottom();
}

mostrarbotonmoverfinal() {
  this.mostrar_boton_moveralfinal = true;
  console.log('moviendo scroll');
  console.log(event);
}
    // loadData(event){
    //   console.log('cargando siguientes');

    //   setTimeout(()=> {

    //     if(this.data.length>50){
    //       event.target.complete();
    //       return;
    //     }
    //     const nuevoArr= Array(20);
    //     this.data.push(...nuevoArr);
    //     event.target.complete();

    //   }, 1000 );
    // }

}
