import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CompleteCvPage } from './complete-cv.page';
import { AutoCompliteModule } from '../../components/auto-complite-input/auto-complite.module';

const routes: Routes = [
  {
    path: '',
    component: CompleteCvPage
  }
];

@NgModule({
  imports: [
    AutoCompliteModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [],
  declarations: [CompleteCvPage]
})
export class CompleteCvPageModule {}
