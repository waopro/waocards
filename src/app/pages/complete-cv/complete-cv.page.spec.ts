import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteCvPage } from './complete-cv.page';

describe('CompleteCvPage', () => {
  let component: CompleteCvPage;
  let fixture: ComponentFixture<CompleteCvPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleteCvPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteCvPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
