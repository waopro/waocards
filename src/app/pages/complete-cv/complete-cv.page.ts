import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, ToastController, ModalController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

import { PostUserOptions } from '../../interfaces/post-user-options';
import { PostProfesionOptions } from '../../interfaces/post-proesion-options';
import { PostEducaOptions } from '../../interfaces/post-educa-options';
import { PostEmpresasOptions } from '../../interfaces/post-empresas-options';
import { PostExpericenciaOptions } from '../../interfaces/post-experiencia-options';
import { PostIdiomasOptions } from '../../interfaces/post-idiomas-options';
import { PostHabilidadOptions } from '../../interfaces/post-habilidad-options';

import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';

import { Valores } from '../../providers/valores';
import { Idiomas } from '../../providers/idiomas';
import { Profesion  } from '../../providers/profesion';
import { Empresas } from '../../providers/empresas';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransfer,  FileUploadOptions,  FileTransferObject} from '@ionic-native/file-transfer/ngx';
import * as urls from '../../providers/urls.servicios';
import { PostReferenciasOptions } from '../../interfaces/post-referencias-options';
import { SignupConfirmationPage } from '../signup-confirmation/signup-confirmation.page';
import { Paises } from '../../providers/paises';
import { PDFService } from '../../providers/pdf.service';
import { VisibilidadDeseadaPage } from '../visibilidad-deseada/visibilidad-deseada.page';
import { AutoCompliteInputComponent } from '../../components/auto-complite-input/auto-complite-input.component';



@Component({
  selector: 'complete-cv',
  templateUrl: './complete-cv.page.html',
  styleUrls: ['./complete-cv.page.scss']
})
export class CompleteCvPage {

  // VARIABLES PARA LA SECCIÓN DE FORMACIÓN ACADÉMICA
  @ViewChild('ACIProfesional') ACIProfesional: AutoCompliteInputComponent;
  @ViewChild('ACIEspecializacion') ACIEspecializacion: AutoCompliteInputComponent;
  @ViewChild('ACIMaestria') ACIMaestria: AutoCompliteInputComponent;
  @ViewChild('ACIDoctorado') ACIDoctorado: AutoCompliteInputComponent;
  @ViewChild('ACITecnicoTecnologo') ACITecnicoTecnologo: AutoCompliteInputComponent;
  @ViewChild('ACIInstEduPregrado') ACIInstEduPregrado: AutoCompliteInputComponent;
  @ViewChild('ACIInstEduEspecializacion') ACIInstEduEspecializacion: AutoCompliteInputComponent;
  @ViewChild('ACIInstEduMaestria') ACIInstEduMaestria: AutoCompliteInputComponent;
  @ViewChild('ACIInstEduDoctorado') ACIInstEduDoctorado: AutoCompliteInputComponent;
  @ViewChild('ACIInstEduTecnicoTecnologo') ACIInstEduTecnicoTecnologo: AutoCompliteInputComponent;

  // VARIABLES PARA CURSOS Y DIPLOMADOS
  @ViewChild('ACIInstEduCursoDiplomado') ACIInstEduCursoDiplomado: AutoCompliteInputComponent;

  // VARIABLES PARA LA SECCIÓN DE EXPERIENCIAS LABORALES
  @ViewChild('ACIEmpresa') ACIEmpresa: AutoCompliteInputComponent;

  signup: PostUserOptions = {
    id_user: '',
    Tipo_Perfil: '',
    Cargo_Actual: '',
    Ciudad_Residencia: '',
    Direccion_Residencia: '',
    Perfil: '',
    Nivel_Academico: '',
    Twitter: '',
    Instagram: '',
    Linkedin: '',
    Github: '',
    WWW: '',
    Profesion: '',
    Institucion_Educativa: '',
    Ha_Trabajado: '',
    Cuantos_Idiomas: '',
    referidos: ''
  };


  prof: PostProfesionOptions = {
    Profesion: '',
    Descripcion: ''
  };

  educa: PostEducaOptions = {
    Nivel_Academico: '',
    Institucion_Educativa: '',
    Institucion_Educativa_Pregrado: '',
    Institucion_Educativa_Especializacion: '',
    Institucion_Educativa_Maestria: '',
    Institucion_Educativa_Doctorado: '',
    Identificacion: '',
    Titulo_Obtenido: '',
    Titulo_ObtenidoDoctorado: '',
    Titulo_ObtenidoEspecializacion: '',
    Titulo_ObtenidoPregrado: '',
    Titulo_ObtenidoTecnico: '',
    Titulo_ObtenidoMaestria: '',
    Ciudad: '',
    Fecha_Inicio: '',
    Fecha_Fin: '',
    
  };

  empresa: PostEmpresasOptions = {
    RazonSocial: '',
    Direccion: '',
    Telefonos: '',
    Correos: '',
    Ciudad: ''
  };

  exper: PostExpericenciaOptions = {
    Identificacion: '',
    Cargo: '',
    Empresa: '',
    Funciones_Cargo: '',
    Fecha_Inicio: '',
    Fecha_Fin: '',
    Telefono1: '',
    Telefono2: ''
  };

  idiom: PostIdiomasOptions = {
    Certificado: '',
    idioma: '',
    Nivel: ''
  };

  habil: PostHabilidadOptions = {
    Aptitud: '',
    Descripcion: ''
  };

  ref: PostReferenciasOptions = {
    Tipo: '',
    Nombre_Referencia: '',
    Ocupacion: '',
    Telefono_Celular: '',
    Identificacion: ''
  };
  TieneReferido= false;
  TieneCargoActual = false;
  submitted = false;
  emailInvalido = false;
  fotografia: any;
  fototomada = false;
  conteducas: any[] = ['1'];
  contexper: any[] = ['1'];
  conthabilidades: any[] = ['1'];
  contreferencias: any[] = ['1'];
  usr: any;
  profesiones: any; // arreglo de obtención
  lasempresas: any; // arreglo de obtención
  losidiomas: any; // arreglo de obtención
  // lospaises: any; // arreglo de obtencion de codigos
  selectedObject: any;
  NuevanombreProfesion: string;
  NuevanombreEmpresa: string;
  NuevanombreHabilidad: string;
  NuevadescripProfesion: string;
  NuevadescripEmpresa: string;
  /// Variables que verifican si se guardó los datos al oprimir el boton de añadir mas
  Guardo_FormacionN = false;
  Guardo_ExperienciaN = false;
  Guardo_HabilidadN = false;
  Guardo_ReferenciaN = false;
//////////////////////////////
  Guardo_Basico = false;
  Guardo_Formacion = false;
  Guardo_Experiencia = false;
  Guardo_Habilidad = false;
  Guardo_Referencia = false;
  idiomaactivado = true;
  certificadoactivado = false;
  haTrabajado = false;
  haEstudiado = false;
  cuantosIdiomas = 0;
  NivelAcademia: any;
  carga_completa = false;
  contrasenaIngresada = false;
  contrasena;
  campcontrasena: any;
  campconfirmacion: any;
  contrasenaPrueba= false;
  arreglo_prueba: any;
  EmpresaNuevaAgregada;
  ProfesionNuevaAgregada;
  DoctoradoSeleccionado = false; EspecializacionSeleccionado = false; PregradoSeleccionado = false; MaestriaSeleccionada = false;
  TecnicoSeleccionado = false; TecnologoSeleccionado = false;

  // Interfaces personalizadas para los ion-select
  customPopoverOptions: any = {
    header: 'Título',
    subHeader: 'Deslice y seleccione el título deseado',
    message: 'Si no aparece, seleccione la opción de "añadir otro"',
    translucent: true
  };

  customActionSheetOptions: any = {
    header: 'Seleccionar empresa',
    subHeader: 'Si no aparece, seleccione la opción de "añadir otra"'
  };

  customPopoverOptionIdioma: any = {
    header: 'Seleccionar idioma',
    subHeader: 'Deslice y seleccione el idioma',
    translucent: true
  };
  selectedObjectDoctorado: any;
  selectedObjectMaestria: any;
  selectedObjectEspecializacion: any;
  selectedObjectPregrado: any;

  

  constructor(
    public router: Router,
    public registro: RegistroUsuariosService,
    public valor: Valores,
    private camera: Camera,
    private transfer: FileTransfer,
    public loadingController: LoadingController,
    public profesion: Profesion,
    public empresas: Empresas,
    public idiomas: Idiomas,
    public paises: Paises,
    public alertController: AlertController,
    public toastController: ToastController,
    public pdf: PDFService,
    // public login: LoginPage,
    public signconf: SignupConfirmationPage,// private file: File
    public modalController: ModalController
  ) {
 // this.selector();
  }

  ionViewWillEnter() {
    this.valor.identificacion = this.signconf.Identificacion;
    this.contrasena = false;
    console.log('el ID que llega aqui es: ' + this.valor.identificacion);
    // Carga de los datos de ID recién registrado con retardo de 100ms
    setTimeout(() => {
     this.usr = this.signconf.usr;
     this.contrasena = true;

      this.valor.cargar_id().subscribe((data: any[]) => {
        this.usr = data;
        this.contrasena = true;
      }, (err: any) => {
        this.contrasena = true; /// se debe cambiar a false
      });
    }, 3000);
  }

  seleccionarArchivo() {
    this.pdf.seleccionarArchivo();
  }

  async cargarEmpresas() {
    // Carga de las empresas para luego mostrarlas en los ion-select del formulario
    setTimeout(() => {
      this.empresas.cargarEmpresas().subscribe(
        dato => {
          console.log(dato);
          this.lasempresas = dato['records'];
        },
        error => {
          this.carga_completa = null;
        }
      );
    }, 1000);
   // this.await
  }

  async cargarProfesiones() {
    // Carga de las profesiones para luego mostrarlas en los ion-select del formulario
    setTimeout(() => {
      this.profesion.cargarProfesiones().subscribe(
        dato => {
          console.log("PROFESIONES: ",dato);
          this.profesiones = dato['records']; // Almacenamiento en array para los ion-select
        },
        error => {
          this.carga_completa = null;
        }
      );
    }, 3000);
  }

  async cargarIdiomas() {
    setTimeout(() => {
      this.idiomas.cargarIdiomas().subscribe(
        dato => {
          this.losidiomas = dato['idiomas'];
          console.log(this.losidiomas);
        },
        error => {
          console.error(error);
        }
      );
    }, 5000);


  }

  async mostrarGuardadoExitoso(valor) {
    const toast = await this.toastController.create({
      message: valor + '. Puede agregar más',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async mostrarGuardadoError(valor) {
    const toast = await this.toastController.create({
      message: valor + '. Verifique los campos',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  seleccionarTipoPerfil(tipo) {
    this.cargarEmpresas();
    this.cargarIdiomas();
    this.cargarProfesiones();
    this.DoctoradoSeleccionado = false;
    this.EspecializacionSeleccionado = false;
    this.PregradoSeleccionado = false;
    this.MaestriaSeleccionada = false;
    this.TecnicoSeleccionado = false;
    this.TecnologoSeleccionado = false;
    this.signup.Tipo_Perfil = tipo;
    this.signup.Ha_Trabajado = '';
    this.haEstudiado = false;
    this.haTrabajado = false;
    this.exper.Cargo = '';
    this.exper.Empresa = '';
    this.exper.Funciones_Cargo = '';
    this.exper.Fecha_Inicio = '';
    this.exper.Fecha_Fin = '';
    this.exper.Telefono1 = '';
    this.exper.Telefono2 = '';
    this.idiom.Certificado = '';
    this.idiom.idioma = '';
    this.idiom.Nivel = '';
    this.habil.Aptitud = '';
    this.habil.Descripcion = '';
    this.signup.Cargo_Actual = '';
    this.signup.Direccion_Residencia = '';
    this.signup.Perfil = '';
    this.signup.Nivel_Academico = '';
    this.signup.Twitter = '';
    this.signup.Instagram = '';
    this.signup.Linkedin = '';
    this.signup.Github = '';
    this.signup.WWW = '';
    this.educa.Nivel_Academico = '';
    this.educa.Institucion_Educativa = '';
    this.educa.Institucion_Educativa_Pregrado = '';
    this.educa.Institucion_Educativa_Especializacion = '';
    this.educa.Institucion_Educativa_Maestria = '';
    this.educa.Institucion_Educativa_Doctorado = '';
    this.educa.Titulo_ObtenidoDoctorado = '';
    this.educa.Titulo_ObtenidoEspecializacion = '';
    this.educa.Titulo_ObtenidoPregrado = '';
    this.educa.Titulo_ObtenidoTecnico = '';
    this.educa.Titulo_ObtenidoMaestria = '';
    this.idiomaactivado = true;
    this.selectedObjectPregrado = '';
    this.selectedObjectEspecializacion = '';
    this.selectedObjectMaestria = '';
    this.selectedObjectDoctorado = '';
    this.selectedObject = '';
    console.log('seleccionado el tipo: ' + tipo);
    this.registro.registroTipoPerfil(this.valor.identificacion, tipo);

    if (tipo !== '3') {
      console.log('Ha estudiado porque no es empirico');
      this.signup.Nivel_Academico = 'Nada';
      this.haEstudiado = true;
    }
  }

  ingresoContrasena() {
    this.registro.GuardarContrasena(this.campcontrasena, this.valor.identificacion);
    this.contrasena = false;
    if (this.registro.verificacionContrasena === false) {
      console.log(this.campcontrasena);
    setTimeout(() => {
      this.carga_completa = true;
      this.contrasenaIngresada = true;
  }, 3000);
  

   }else{
    this.campcontrasena='';
    console.log(this.campcontrasena);
    this.carga_completa = false;
    this.contrasenaIngresada = false;
   }
}
  // Metodo que permite capturar la data de los ion-select y tomar la decisión cuando se selecciona la opción de añadir otro
  updateSelectedValue(event): void {
    console.log("IMPRIMIENDO EL VALOR DEL EVENTO IONCHANGE: ");
    console.log(event);

    this.selectedObject = event.detail.value;
    if (this.selectedObject === 'Doctorado') {
      console.log('Se seleccionó añadir otro doctorado');
      this.DoctoradoSeleccionado = true;
      this.EspecializacionSeleccionado = false;
      this.PregradoSeleccionado = false;
      this.TecnicoSeleccionado = false;
      this.TecnologoSeleccionado = false;
      this.MaestriaSeleccionada = false;
    }
    if (this.selectedObject === 'Especialización') {
      console.log('Se seleccionó añadir otra especializacion');
      this.DoctoradoSeleccionado = false;
      this.EspecializacionSeleccionado = true;
      this.PregradoSeleccionado = false;
      this.TecnicoSeleccionado = false;
      this.TecnologoSeleccionado = false;
      this.MaestriaSeleccionada = false;
    }
    if (this.selectedObject === 'Pregrado') {
      console.log('Se seleccionó añadir otra profesion');
      this.DoctoradoSeleccionado = false;
      this.EspecializacionSeleccionado = false;
      this.PregradoSeleccionado = true;
      this.TecnicoSeleccionado = false;
      this.TecnologoSeleccionado = false;
      this.MaestriaSeleccionada = false;
    }
    if (this.selectedObject === 'Tecnólogo') {
      console.log('Se seleccionó TECNÓLOGO');
      this.DoctoradoSeleccionado = false;
      this.EspecializacionSeleccionado = false;
      this.PregradoSeleccionado = false;
      this.TecnicoSeleccionado = false;
      this.TecnologoSeleccionado = true;
      this.MaestriaSeleccionada = false;

    }
    if (this.selectedObject === 'Técnico') {
      console.log('Se seleccionó TÉCNICO');
      this.DoctoradoSeleccionado = false;
      this.EspecializacionSeleccionado = false;
      this.PregradoSeleccionado = false;
      this.TecnicoSeleccionado = true;
      this.TecnologoSeleccionado = false;
      this.MaestriaSeleccionada = false;
    }
    if(this.selectedObject === 'Maestria'){
      console.log('Se seleccionó añadir otra maestria');
      this.DoctoradoSeleccionado = false;
      this.EspecializacionSeleccionado = false;
      this.PregradoSeleccionado = false;
      this.TecnicoSeleccionado = false;
      this.TecnologoSeleccionado = false;
      this.MaestriaSeleccionada = true;
    }

    if (this.selectedObject === 'trabajado') {
      this.haTrabajado = !this.haTrabajado;
      if (this.haTrabajado) {
        console.log('Sí ha trabajado');
        this.signup.Ha_Trabajado = '1';
        this.Guardo_Experiencia = false;
      }
      if (this.haTrabajado === false) {
        console.log('No tiene trabajo');
        this.signup.Ha_Trabajado = '0';
        this.exper.Cargo = '';
        this.exper.Empresa = 'Ninguna';
        this.exper.Funciones_Cargo = '';
        this.exper.Fecha_Inicio = '';
        this.exper.Fecha_Fin = '';
        this.exper.Telefono1 = '';
        this.exper.Telefono2 = '';
      }
    }
    if (this.selectedObject === 'estudiado') {
      this.haEstudiado = !this.haEstudiado;
      if (this.haEstudiado) {
        console.log('Sí ha trabajado');
        // this.signup.Ha_Trabajado = '1';
        this.Guardo_Formacion = true;
      }
      if (this.haEstudiado === false) {
        console.log('No tiene trabajo');
        this.educa.Nivel_Academico = '';
        this.educa.Institucion_Educativa = '';
        this.educa.Titulo_Obtenido = '';
        this.educa.Ciudad = '';
        this.educa.Fecha_Inicio = '';
        this.educa.Fecha_Fin =  '';
      }
    }
    if (this.selectedObject === 'Cargoactual') {
      this.TieneCargoActual = !this.TieneCargoActual;
      if (this.TieneCargoActual) {
        console.log('Sí tiene cargo');
        // this.signup.Ha_Trabajado = '1';
        // this.Guardo_Formacion = true;
      }
      if (this.TieneCargoActual === false) {
        console.log('No tiene cargo');
        this.signup.Cargo_Actual = 'Ninguno';
      }
    }

    if (this.selectedObject=== 'diplomado_añadido'){

    }

    if(this.selectedObject === 'referido1'){
      this.TieneReferido= !this.TieneReferido;
    }
    if(this.selectedObject === 'campo'){
      this.contrasenaIngresada= false;
    }
    console.log('selectedObject: ' + this.selectedObject);
  }
   habilita_siguiente(){
     if(this.campcontrasena=''){
      
       this.contrasenaIngresada=false;

     }else{
     
      this.contrasenaIngresada=true;
     }
   }

   comparando_campos(){
     console.log("SE ESTÁ DISPARANDO LA ACCION DE COMPARAR CAMPOS");
     console.log(this.campcontrasena, ' ', this.campconfirmacion);

     if((this.campcontrasena === '' && this.campconfirmacion === '' )|| (this.campcontrasena !== this.campconfirmacion)){
        console.log('se deshabilita boton');
        this.contrasenaPrueba=false;
     }
     else{
       if(this.campcontrasena === this.campconfirmacion){
        console.log('se habilita boton');
        this.contrasenaPrueba=true;
       }
     }
   }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Hellooo',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }

  // ventana de loading que permite almacenar la data nueva en la base de datos
  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      duration: 6500,
      message: 'Registrando...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  // ventana de verificacion
  async presentAlert(cabecera, mensaje) {
    const alert = await this.alertController.create({
      header: cabecera,
      message: mensaje,
      buttons: ['Continuar']
    });

    await alert.present();
  }


  // Captura de la data para enviarla a la base de datos con identificación seleccionada
  anadireducacion() {
    /*
    console.log(this.ACIProfesional.cadena);
    console.log(this.ACIEspecializacion.cadena);
    console.log(this.ACIMaestria.cadena);
    console.log(this.ACIDoctorado.cadena);
    console.log(this.ACIInstEduPregrado.cadena);
    console.log(this.ACIInstEduEspecializacion.cadena);
    console.log(this.ACIInstEduMaestria.cadena);
    console.log(this.ACIInstEduDoctorado.cadena);
    console.log(this.ACIInstEduTecnicoTecnologo.cadena);
    console.log(this.ACIInstEduCursoDiplomado.cadena);
    console.log(this.ACIEmpresa.cadena);
    */
    switch(this.signup.Tipo_Perfil){
      case  '1':
        if (this.ACIProfesional.cadena === '' || this.ACIEspecializacion.cadena === '' || this.ACIMaestria.cadena === '' || this.ACIDoctorado.cadena === '' || this.ACIInstEduPregrado.cadena === '' || this.ACIInstEduEspecializacion.cadena === '' || this.ACIInstEduMaestria.cadena === '' || this.ACIInstEduDoctorado.cadena === '')
        {
          this.presentAlert('Campos de formación académica faltantes', 'Verifique que estén corractamente diligenciados.');
        }
        else {
          console.log("AÑADIENDO FORMACIONES DE PERFIL PROFESIONAL");
          let institucionesProfesional: string[] = [];
          let titulosProfesional: string[] = [];
          institucionesProfesional.push(this.ACIInstEduPregrado.cadena, this.ACIInstEduEspecializacion.cadena, this.ACIInstEduMaestria.cadena, this.ACIInstEduDoctorado.cadena);
          //titulosProfesional.push();
          console.log(institucionesProfesional);
          console.log(titulosProfesional);
        }
        break;
      case '2':
        console.log('AÑADIENDO FORMACIONES DE PERFIL TECNICO');
        const institucionesTecnico: string[] = [];
        const titulosTecnico: string[] = [];
        institucionesTecnico.push(this.educa.Institucion_Educativa);
        titulosTecnico.push(this.educa.Titulo_ObtenidoTecnico);
        console.log(institucionesTecnico);
        console.log(titulosTecnico);
        break;
      case '3':
        console.log("AÑADIENDO FORMACIONES DE PERFIL EMPIRICO");
        let titulosEmpirico: string[] = [];
        titulosEmpirico.push(this.educa.Titulo_Obtenido);
        console.log(titulosEmpirico);
        break;
    }
  }

  anadircursodiplomado(){
    if (this.educa.Titulo_Obtenido !== '' && this.educa.Institucion_Educativa !== '') {
      this.registro.registroEduucacion(
        this.educa.Nivel_Academico,
        this.educa.Institucion_Educativa,
        this.valor.identificacion,
        this.educa.Titulo_Obtenido,
        this.educa.Ciudad,
        this.educa.Fecha_Inicio,
        this.educa.Fecha_Fin
      );
      this.NivelAcademia = this.educa.Nivel_Academico; // Variable empleada para mostrarlos en el frontend
      this.Guardo_Formacion = true; // permite mostrar la tarjeta de confirmación que se agregó correctamente la formación
      this.Guardo_FormacionN = true;
      this.ProfesionNuevaAgregada = true;
      this.mostrarGuardadoExitoso('Formación académica añadida');
      // Limpieza de la data para ingresar datos nuevos
      this.educa.Nivel_Academico = '';
      this.educa.Institucion_Educativa = '';
      this.educa.Titulo_Obtenido = '';
      this.educa.Ciudad = '';
      this.educa.Fecha_Inicio = '';
      this.educa.Fecha_Fin = '';
    }
    else {
      this.mostrarGuardadoError('Datos faltantes');
    }
  }

  anadirexperiencia() {
    console.log(this.exper.Empresa);
    console.log(this.exper.Funciones_Cargo);
    if (this.exper.Empresa !== '') { // aca podria ir && this.exper.Funciones_Cargo !== ''
        console.log(JSON.stringify(this.exper));
      this.registro.registroExperiencia(
        this.valor.identificacion,
        this.exper.Cargo,
        this.exper.Empresa,
        this.exper.Funciones_Cargo,
        this.exper.Fecha_Inicio,
        this.exper.Fecha_Fin,
        this.exper.Telefono1,
        this.exper.Telefono2
      );
      this.registro.cargarEmpresaUsuarios(this.exper.Empresa);
      this.mostrarGuardadoExitoso('Experiencia laboral añadida');
      this.NuevanombreEmpresa = this.exper.Empresa;
      this.NuevadescripEmpresa = this.exper.Cargo;
      this.Guardo_Experiencia = true;
      this.Guardo_ExperienciaN = true;
      this.exper.Cargo = '';
      this.exper.Empresa = '';
      this.exper.Funciones_Cargo = '';
      this.exper.Fecha_Inicio = '';
      this.exper.Fecha_Fin = '';
      this.exper.Telefono1 = '';
      this.exper.Telefono2 = '';
      this.EmpresaNuevaAgregada = false;
    }
    else {
      this.mostrarGuardadoError('Datos faltantes');
    }
  }

  anadirhabilidad() {
    if (this.idiomaactivado === true) {
      this.registro.registroIdioma(this.idiom.Certificado, this.valor.identificacion, this.idiom.idioma, this.idiom.Nivel);
      this.mostrarGuardadoExitoso('Idioma añadido');
      if (this.habil.Aptitud !== '' && this.habil.Descripcion !== '') {
      this.registro.registroHabilidad(
        'Idiomas',
        this.idiom.idioma + ' (' + this.idiom.Nivel + ')',
        this.valor.identificacion
      );
      this.NuevanombreHabilidad = 'Idiomas';
      this.cuantosIdiomas++;
      console.log('Tiene: ' + this.cuantosIdiomas + ' idiomas');
      this.Guardo_HabilidadN = true;
    }
  }}

    anadirreferencia() {
    if (this.ref.Nombre_Referencia !== '' && this.ref.Ocupacion !== '' && this.ref.Telefono_Celular !== '') {
      this.registro.registroReferencias(
        this.ref.Tipo,
        this.ref.Nombre_Referencia,
        this.ref.Ocupacion,
        this.ref.Telefono_Celular,
        this.valor.identificacion
      );
        this.Guardo_Referencia = true;
        this.Guardo_ExperienciaN = true;
        this.mostrarGuardadoExitoso('Referencia añadida');
        setTimeout(() => {
          this.ref.Tipo = '';
          this.ref.Nombre_Referencia = '';
          this.ref.Ocupacion = '';
          this.ref.Telefono_Celular = '';
        }, 300);
    }
    else {
      this.mostrarGuardadoError('Datos faltantes');
    }
  }
  

  tomarFoto() {
    const options: CameraOptions = {
      quality: 95,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: false,
      allowEdit: true,
      targetWidth: 600,
      targetHeight: 600
    };

    this.camera.getPicture(options).then(
      imageData => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.fotografia = 'data:image/jpeg;base64,' + imageData;
        this.fototomada = true;
      },
      err => {
        this.fototomada = false;
        // Handle error
      }
    );
  }

  GaleriaImagen() {
    const options: CameraOptions = {
      quality: 95,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      allowEdit: true,
      targetWidth: 600,
      targetHeight: 600
    };

    this.camera.getPicture(options).then(
      imageData => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.fotografia = 'data:image/jpeg;base64,' + imageData;
        this.fototomada = true;
      },
      err => {
        // Handle error
        this.fototomada = false;
      }
    );
  }

  SubirFoto() {
    // create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();
    console.log('direccion: ' + urls.fotos + 'uploadPhoto.php');

    // option transfer
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: this.valor.identificacion + '.jpg',
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: 'image/jpeg',
      headers: {}
    };

    // file transfer action
    fileTransfer
      .upload(this.fotografia, urls.fotos + 'uploadPhoto.php', options)
      .then(
        data => {
          // alert('Correcto');
        },
        err => {
          console.log(err);
          // alert('Incorrecto');
        }
      );
  }

  soloLetra(e) {
    // Se captura el código ASCII de la tecla presionada en la variable local 'key'
    let key = e.keyCode || e.which;
    // console.log(key);

    // Se evalua si el código ASCII capturado corresponde a las letras del abcedario en mayúsculas o en minúsculas
    if ((key > 64 && key < 91) || (key > 96 && key < 123) || key == 32) {
      return true; // Se permite la escritura en el ion-input
    }
    return false; // Se bloquea la escritura en el ion-input
  }

  onSignup(form: NgForm) {
    this.submitted = true;

      this.SubirFoto();
      // SI TODOS LOS CAMPOS ESTÁ LLENOS ENTONCES...
      console.log('Profesion obtenida es: ');
      console.log(this.prof.Profesion);
      console.log( this.educa.Titulo_Obtenido);

      if(this.signup.referidos!= '' && this.TieneReferido == true){
        this.registro.Registro_referidos(this.valor.identificacion, this.signup.referidos);
      }

      if(this.signup.referidos== '' && this.TieneReferido == true){
        this.presentAlert('Campo de referidos faltante', 'Verifique que estén corractamente diligenciados.');
      }

      if (this.signup.Tipo_Perfil === '3') {
        console.log('Se define nueva profesion porque empirico');
        this.educa.Titulo_Obtenido = this.signup.Cargo_Actual;
      }

      if (this.prof.Profesion === '') {
        console.log('El valor es nulo: ' + this.educa.Titulo_Obtenido );
        this.prof.Profesion = this.educa.Titulo_Obtenido;
      }
      setTimeout(() => {
        if (this.signup.Cargo_Actual !== '' && this.signup.Perfil !== '') {

          console.log('cuantosIdiomas: ', this.cuantosIdiomas.toString());
          this.registro.registrar(
            this.signup.Tipo_Perfil,
            this.signup.Cargo_Actual,
            this.signup.Ciudad_Residencia,
            this.signup.Direccion_Residencia,
            this.signup.Perfil,
            this.signup.Nivel_Academico,
            this.signup.Twitter,
            this.signup.Instagram,
            this.signup.Linkedin,
            this.signup.Github,
            this.signup.WWW,
            this.prof.Profesion,
            this.educa.Institucion_Educativa,
            this.signup.Ha_Trabajado,
            this.cuantosIdiomas.toString(),
            this.exper.Empresa,
            this.valor.identificacion
          );
          this.Guardo_Basico = true;


          if (this.signup.Tipo_Perfil === '' && this.signup.Cargo_Actual === '' && this.signup.Perfil === '' )
           {
            this.Guardo_Basico = false;
            this.presentAlert('Campos de información básica faltantes', 'Verifique que estén corractamente diligenciados.');
            console.log('Hay algún campo de información básica sin llenar');
            return;
          }
        }
      }, 1000);


  /// VERIFICACION DE FORMACION ACADEMICA
    if (this.haEstudiado === true && this.Guardo_FormacionN === false) {
      if (this.educa.Titulo_Obtenido !== '' && this.educa.Institucion_Educativa !== '' ) {

        this.registro.registroEduucacion(
          this.educa.Nivel_Academico,
          this.educa.Institucion_Educativa,
          this.valor.identificacion,
          this.educa.Titulo_Obtenido,
          this.educa.Ciudad,
          this.educa.Fecha_Inicio,
          this.educa.Fecha_Fin
        );
          this.Guardo_Formacion = true;
      }
      if (this.educa.Titulo_Obtenido === '' && this.educa.Institucion_Educativa === '') {
        this.Guardo_Formacion = false;
        this.presentAlert('Campos de formación académica faltantes', 'Verifique que estén corractamente diligenciados.');
        console.log('Hay algún campo de formación sin llenar');
        return;
      }
    }

// VERIFICACION DE EXPERIENCIAS LABORALESF
if (this.haTrabajado === true  && this.Guardo_ExperienciaN === false) {
  if (this.exper.Empresa !== '' && this.exper.Funciones_Cargo !== '' ) {
    this.registro.registroExperiencia(
      this.valor.identificacion,
      this.exper.Cargo,
      this.exper.Empresa,
      this.exper.Funciones_Cargo,
      this.exper.Fecha_Inicio,
      this.exper.Fecha_Fin,
      this.exper.Telefono1,
      this.exper.Telefono2
    );
      this.Guardo_Experiencia = true;
  }

  if (this.exper.Empresa === ''  && this.exper.Funciones_Cargo === '' ) {
      this.Guardo_Experiencia = false;
    this.presentAlert('Campos de experiencia faltantes', 'Verifique que estén corractamente diligenciados.');
    console.log('Hay algún campo de experiencia sin llenar');
    return;
  }
}

    if (this.Guardo_HabilidadN === false) { // verificación: si no se ha oprimido alguna vez el botón de + permite guardarlo

      if (this.idiomaactivado === true) {
        this.registro.registroIdioma(
          this.idiom.Certificado,
          this.valor.identificacion,
          this.idiom.idioma,
          this.idiom.Nivel
        );
        this.registro.registroHabilidad(
          'Idiomas',
          this.idiom.idioma + ' (' + this.idiom.Nivel + ')',
          this.valor.identificacion
        );
        this.NuevanombreHabilidad = 'Idiomas';
        this.cuantosIdiomas++;
        console.log('Tiene: ' + this.cuantosIdiomas + ' idiomas');
        this.Guardo_Habilidad = true;
      }
      if (this.idiomaactivado === false) {
        if (this.habil.Aptitud !==  '' && this.habil.Descripcion !== '') {
            this.registro.registroHabilidad(
              this.habil.Aptitud,
              this.habil.Descripcion,
              this.valor.identificacion
            );
            this.Guardo_Habilidad = true;
        }
        if (this.habil.Aptitud == '' && this.habil.Descripcion == '') {
          this.Guardo_Habilidad = false;
          this.presentAlert('Campos de habilidad faltantes', 'Verifique que estén corractamente diligenciados.');
          console.log('Hay algún campo de habilidad sin llenar');
          return;
        }
        this.NuevanombreHabilidad = this.habil.Aptitud;
      }
    }

      // VALIDAR REFERENCIAS
    
      if (this.ref.Nombre_Referencia !== '' && this.ref.Ocupacion !== '' && this.ref.Telefono_Celular !== '') {
        this.registro.registroReferencias(
          this.ref.Tipo,
          this.ref.Nombre_Referencia,
          this.ref.Ocupacion,
          this.ref.Telefono_Celular,
          this.valor.identificacion
        );
        this.Guardo_Referencia = true;
      }

      if (this.ref.Nombre_Referencia === '' && this.ref.Ocupacion === '' && this.ref.Telefono_Celular === '') {
        this.Guardo_Referencia = false;
        this.presentAlert('Campos de referencia faltantes', 'Verifique que estén corractamente diligenciados.');
        console.log('Hay algún campo de referencia sin llenar');
        return;
      }
      this.presentLoadingWithOptions();

    setTimeout(() => {
   // this.login.login.username = this.valor.correo;
    this.router.navigateByUrl('/login');
    this.presentAlert('¡Registro Correcto!', 'Se han registrado correctamente sus datos, puede iniciar sesión');
    }, 7000);

  }
// metodo para guardar la visibilidad deseada
  async presentModalVisibilidadDeseada() {
    const modal = await this.modalController.create({
      component: VisibilidadDeseadaPage,
      componentProps: {
        'modoPreferencias': 'visibilidadDeseada'
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
  }
 // metodo para presentar el modal de Campo de acción
   async presentarCampoAccion() {

    const modal = await this.modalController.create({
      component: VisibilidadDeseadaPage,
      componentProps: {
        'modoPreferencias': 'campoAccion'
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
  }
}


