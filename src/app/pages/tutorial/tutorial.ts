import { Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { Router } from '@angular/router';

import { MenuController, IonSlides } from '@ionic/angular';

import { AppComponent } from '../../app.component';
import { Storage } from '@ionic/storage';
import { MapPage } from '../map/map';
import { Actualizaciones } from '../../providers/actualizaciones';


@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
  styleUrls: ['./tutorial.scss'],
})
export class TutorialPage {
  showSkip = true;
  

  @ViewChild('slides') slides: IonSlides;

  constructor(
    public menu: MenuController,
    public router: Router,
    public storage: Storage,
    public map: MapPage,
    public componenteAPP: AppComponent,
    public actualizaciones: Actualizaciones
  ) {}

  getSignedUp() {
    this.router
      .navigateByUrl('/signup');
    // .then(() => this.storage.set('ion_did_tutorial', 'true'));
  }

  onSlideChangeStart(event) {
    event.target.isEnd().then(isEnd => {
      this.showSkip = !isEnd;
    });
  }

  ionViewWillEnter() {
/*     this.storage.get('ion_did_tutorial').then(res => {
      if (res === true) { */
    /*     this.router.navigateByUrl('/tutorial');
        this.map._i = 9999; */
/*       }
    }); */

/*     this.menu.enable(false); */
  }


}
