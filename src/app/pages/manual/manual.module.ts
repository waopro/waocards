import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ManualPage } from './manual.page';
import { DetallePersonaEncontradaPage } from '../detalle-persona-encontrada/detalle-persona-encontrada.page';
import { DetallePersonaEncontradaPageModule } from '../detalle-persona-encontrada/detalle-persona-encontrada.module';

// Componentes externos que realizan peticiones
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// Carga archivos de idioma escritos en formato JSON.
export function customTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}



const routes: Routes = [
  {
    path: '',
    component: ManualPage
  }
];

@NgModule({
  entryComponents: [
    DetallePersonaEncontradaPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    DetallePersonaEncontradaPageModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: customTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [/*ManualPage*/]
})
export class ManualPageModule {}
