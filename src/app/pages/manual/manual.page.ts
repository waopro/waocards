import { Component} from '@angular/core';
import { Router } from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ActionSheetController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { AlertController} from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ThrowStmt } from '@angular/compiler';
//import { AppComponent } from '../../app.component';

import { DetallePersonaEncontradaPage } from '../detalle-persona-encontrada/detalle-persona-encontrada.page';

import { usuarios } from '../../providers/usuarios';
import { Educacion } from '../../providers/educacion';
import { Experiencias } from '../../providers/experiencias';
import { BusquedaManual } from '../../providers/busqueda-manual.service';
import * as urls from '../../providers/urls.servicios';

import { BusquedaManualInterface } from '../../interfaces/busqueda-manual';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';



@Component({
  selector: 'manual',
  templateUrl: './manual.page.html',
  styleUrls: ['./manual.page.scss'],
})
export class ManualPage {
  enviada = 'indefinido';
  PEncontradas: any;
  PSugeridas: any;
  sinResultados: boolean = false;
  formaciones: any[] = [];
  experiencias: any[] = [];
  amigoscomun: any[] = [];
  completo: string;
  URLavatar = urls.URLimagen;
  contreferencias: any[] = ['1', '1', '1', '1'];
  // elementos del idioma
  DesliceParaRecargar: string;
  ObteniendoSus: string;
  ImgSrc: string;
  cadenaBusquedaManual: string;
  mensajesSvr: string;
  

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public confData: usuarios,
    public edd: Educacion,
    public exxp: Experiencias,
    public inAppBrowser: InAppBrowser,
    public modalController: ModalController,
    public router: Router,
    public toastController: ToastController,
    public _translate: TranslateService,
    public alertController: AlertController,
    public realizandoBusqueda: LoadingController,
    public busquedaManual: BusquedaManual,
    public registro: RegistroUsuariosService,
    public valor: Valores,
    public detalle: DetallePersonaEncontradaPage
  ) {
    this.cargarIdiomaElementos();
    this.completo = 'false';
    console.log("los speakers antes: " + this.completo);
    console.log(this.PEncontradas);
    console.log(this.PSugeridas);
   
  }

  cargarIdiomaElementos() {
    this._translate.stream('PAGES.SPEAKER-LIST.DESLICE_PARA_RECARGAR').subscribe((text: string) => {
      this.DesliceParaRecargar = text;
    });
    this._translate.stream('PAGES.SPEAKER-LIST.OBTENIENDO_SUS_CONT').subscribe((text: string) => {
      this.ObteniendoSus = text;
    });
    this._translate.stream('PAGES.SPEAKER-LIST.IMG_SRC').subscribe((text: string) => {
      this.ImgSrc = text;
    });
  }

  async presentarErrorToast() {
    const toast = await this.toastController.create({
      message: 'No hay conexión al servidor, ¡Datos limitados!',
      showCloseButton: true,
      position: 'bottom',
      closeButtonText: 'Listo',
      color: 'danger'
    });
    toast.present();
  }

  ionViewDidEnter() {
    if (this.completo === 'error') {
    }
   // this.comprobarSolicitud();
  }

  goToSpeakerTwitter(speaker: any) {
    this.inAppBrowser.create(
      `https://twitter.com/${speaker.twitter}`,
      '_blank'
    );
  }

  async openSpeakerShare(speaker: any) {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Share ' + speaker.name,
      buttons: [
        {
          text: 'Copy Link',
          handler: () => {
            console.log(
              'Copy link clicked on https://twitter.com/' + speaker.twitter
            );
            if (
              (window as any)['cordova'] &&
              (window as any)['cordova'].plugins.clipboard
            ) {
              (window as any)['cordova'].plugins.clipboard.copy(
                'https://twitter.com/' + speaker.twitter
              );
            }
          }
        },
        {
          text: 'Share via ...'
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });

    await actionSheet.present();
  }

  async openContact(speaker: any) {
    const mode = 'ios'; // this.config.get('mode');

    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Contact ' + speaker.Nombre,
      buttons: [
        {
          text: `Email ( ${speaker.Email} )`,
          icon: mode !== 'ios' ? 'mail' : null,
          handler: () => {
            window.open('mailto:' + speaker.Email);
          }
        },
        {
          text: `Call ( ${speaker.Telefono_Celular} )`,
          icon: mode !== 'ios' ? 'call' : null,
          handler: () => {
            window.open('tel:' + speaker.Telefono_Celular);
          }
        }
      ]
    });

    await actionSheet.present();
  }

  doRefrescar(event) {
    console.log('Empieza a refrescar todo');


    this.buscarManual();
  }

  cargarAmigosComun() {
    this.registro.CargaAmigosComunCompleta = false;
    this.URLavatar = urls.URLimagen;
    setTimeout(() => {
     // this.registro.VerAmigosComun(this.valor.identificacion, this.DetalleClasificadoData.Identificacion);
    }, 2500);
    }

  buscarManual() {
    this.presentLoading();
      setTimeout(() => {
        this.busquedaManual.buscarManual(this.cadenaBusquedaManual, this.valor.identificacion)
      .subscribe((RTABusqueda: BusquedaManualInterface) => {
        console.log(RTABusqueda);
        if (RTABusqueda.Encontrados === true) {

          this.PEncontradas = RTABusqueda.PEncontradas;
          this.PSugeridas = RTABusqueda.PSugeridas;
          this.URLavatar = urls.URLimagen;
          /* for (let i of this.PEncontradas) {
            this.registro.VerAmigosComun(this.valor.identificacion, i.Identificacion);
            setTimeout(() => {
            i.AmigosComunes = this.registro.AmigosEnComun.amigoscomun;
            console.log( i.AmigosComunes);

            }, 1000);
          } */
          this.completo = 'true';
          this.sinResultados = false;
          console.log(this.PEncontradas);
          console.log(this.PSugeridas);

          // los dos siguientes metodos sirven para filtrar los datos repetidos que arroje la busqueda de Psugeridas
          const identificacionComprobar = this.PSugeridas.map(function (item) {
            return item.Identificacion;
          });
          const PsugeridasResultantes = this.PSugeridas.filter(function (item, index) {
            if (identificacionComprobar.indexOf(item.Identificacion) >= index) {
              // console.log("comprobacion ", identificacionComprobar.indexOf(item.Identificacion));
              return item;
            }
          });

          this.PSugeridas = PsugeridasResultantes;

          // los dos siguientes metodos sirven para filtrar los datos repetidos que arroje la busqueda de Pencontradas
          const identificacionComprobarA = this.PEncontradas.map(function (item) {
            return item.Identificacion;
          });
          const PsugeridasResultantesA = this.PEncontradas.filter(function (item, index) {
            if (identificacionComprobarA.indexOf(item.Identificacion) >= index) {
              // console.log("comprobacion ", identificacionComprobar.indexOf(item.Identificacion));
              return item;
            }
          });

          this.PEncontradas = PsugeridasResultantesA;


        }
        else {
          console.log(RTABusqueda.Mensaje);
          this.sinResultados = true;
          this.completo = 'false';
          this.mensajesSvr = 'Ningún resultado que cumpla con su criterio de búsqueda';
        }
      });
    }, 2000);
  }

  //ESTO POR AHORA NO SE ESTÁ USANDO (11/06/2019)
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'BUSQUEDA',
      subHeader: 'MANUAL',
      message: 'LISTO PARA MOSTRAR DETALLES.',
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentLoading() {
    const loading = await this.realizandoBusqueda.create({
      message: 'Buscando a su profesional..',
      duration: 2000
    });
    await loading.present()/*.then(() => {
      console.log('cargando');
      if(this.PEncontradas != null)
      {
        loading.dismiss();
      }

    })*/;

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }

  async detalles(idusr) {
    /*
    console.log('LISTO PARA MOSTRAR DETALLES');
    this.presentAlert();
    */
   // this.detalle.solicitante = false;
   const modal = await this.modalController.create({
      component: DetallePersonaEncontradaPage,
      componentProps: {
        IDEncontrado: idusr,
        solicitante: 'false'
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
  }
}
