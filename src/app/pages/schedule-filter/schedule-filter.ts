
import { AfterViewInit, Component, ViewEncapsulation, Input, ɵConsole } from '@angular/core';
import { ModalController, Platform, ToastController, LoadingController, AlertController } from '@ionic/angular';

import { ConferenceData } from '../../providers/conference-data';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { PostClasificadosOptions } from '../../interfaces/post-clasificados-options';
import { Valores } from '../../providers/valores';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer/ngx';
import * as urls from '../../providers/urls.servicios';
import { Router } from '@angular/router';
import { SliderTamanosPage } from '../slider-tamanos/slider-tamanos.page';
import { FeriasEventosService } from '../../providers/ferias-eventos.service';
import { runInThisContext } from 'vm';


@Component({
  selector: 'page-schedule-filter',
  templateUrl: 'schedule-filter.html',
  styleUrls: ['./schedule-filter.scss'],
})

export class ScheduleFilterPage implements AfterViewInit {

  @Input() pauta: string;
  tracks: { name: string, isChecked: boolean }[] = [];
  clasi: PostClasificadosOptions = {
    Identificacion: '',
    Titulo: '',
    Descripcion: '',
    Contacto: '',
    PalabrasClave: '',
    Tipo: ''
  };

  FeriEvent = {
    Identificacion: '',
    Ciudad: '',
    Pais: '',
    Titulo: '',
    Direccion: '',
    fechaIni: '',
    fechaFin: '',
    precio: '',
    PalabrasClave: '',
    Tipo: '',
    Expositores: ''
  };

  preciosTamPautas: any[] = [];

  CiudadesPaisesBD: any[];

  fotografia: any;
  fototomada: boolean = false;
  IDClasificado: number = null;
  IDFeriaEvento: number = null;
  Color: any;
  tamanoPublicidad: string = ' ';
  tamanoFoto: string = 'ion-col33';
  anchoFoto: string = '4';
  precioPauta: number = 0;

  fechaInicialMinFeriaEvento: string = '2019';
  fechaFinalMinFeriaEvento: string = '2019';
  fechaInicialMaxFeriaEvento: string = '2020-12-31';
  fechaFinalMaxFeriaEvento: string = '2020-12-31';
  disableFechaFin: boolean = true;
  validFecha: boolean = true;
  validFoto: boolean = true;

  constructor(
    public confData: ConferenceData,
    public modalCtrl: ModalController,
    public registro: RegistroUsuariosService,
    public valor: Valores,
    public camera: Camera,
    public transfer: FileTransfer,
    public platform: Platform,
    public router: Router,
    public toastcontroller: ToastController,
    public alertSelectTamano: AlertController,
    public loadingController: LoadingController,
    public feriasEventos: FeriasEventosService
  ) {
    setTimeout(() => {

      this.feriasEventos.loadCiudades().subscribe((ciudades: any['records']) => {
        this.CiudadesPaisesBD = ciudades['ciudades'];
        console.log('Array de ciudades y paises: ');
        console.log(this.CiudadesPaisesBD);
      }, (err: any) => {

      }
      );
      console.log(this.pauta);
      this.cargarID(this.pauta);
    }, 500);

  }

  // TODO use the ionViewDidEnter event
  ngAfterViewInit() {
    //console.log(this.pauta);

    this.CargarPreciosTamanosPautas();
    //this.cargarID('Feria_Evento');

    console.log(this.IDClasificado);
    console.log(this.IDFeriaEvento);
  }

  async presentarToastOK() {
    const toast = await this.toastcontroller.create({
      message: 'Clasificado almacenado correctamente',
      showCloseButton: true,
      position: 'top',
      closeButtonText: 'Listo',
      color: 'success',
      duration: 3500
    });
    toast.present();
  }

  async presentarToastError(error:string) {
    const toast = await this.toastcontroller.create({
      message: error,
      showCloseButton: true,
      position: 'bottom',
      closeButtonText: 'Listo',
      color: 'danger',
      duration: 3500
    });
    toast.present();
  }

  async presentarLoading() {
    const loading = await this.loadingController.create({
      message: 'Cargando',
      duration: 2000,
      translucent: true
    });
    await loading.present();
  }

  cargarID(tipoPauta: string) {
    /*
    console.log(this.valor.identificacion);
    console.log(tipoPauta);
    */
    this.registro.UltimoID(this.valor.identificacion, tipoPauta);
    console.log('entra a cargar ids');
    setTimeout(() => {
      /* console.log(this.registro.IDRetornado.Identificacion['0'].ID);
      console.log(JSON.stringify(this.registro.IDRetornado.Identificacion));   */
      if (tipoPauta === 'Clasificado') {
        this.IDClasificado = parseInt(this.registro.IDRetornado.Identificacion['0'].ID) + 1;
        console.log('entra a setear el id del clasificado');
        console.log(this.registro.IDRetornado.Identificacion['0'].ID);
      } else {
        this.IDFeriaEvento = parseInt(this.registro.IDRetornado.Identificacion['0'].ID_feria_Evento) + 1;
        /*
        console.log('entra a setear el id de la feria o evento');
        console.log(this.registro.IDRetornado.Identificacion['0'].ID_feria_Evento);
        */
      }
    }, 500);
  }

  ObtenerColor(palabraClave, tipo) {
    console.log('La palabrasClave es: ', palabraClave);
    console.log('El tipo es: ', tipo);
    if (palabraClave == 'Afin') {
      if ((tipo == 'Pequeño') || (tipo == 'ExtralargoH') || (tipo == 'LargoH')) {
        this.Color = 'ion-col31';
      }
      if ((tipo == 'Grande2') || (tipo == 'GrandeXL')) {
        this.Color = 'ion-col6x61';
      }
      if (tipo == 'LargoV') {
        this.Color = 'ion-col6x31';
      }
    }

    if (palabraClave == 'General') {
      if ((tipo == 'Pequeño') || (tipo == 'ExtralargoH') || (tipo == 'LargoH')) {
        this.Color = 'ion-col32';
      }
      if ((tipo == 'Grande2') || (tipo == 'GrandeXL')) {
        this.Color = 'ion-col6x62';
      }
      if (tipo == 'LargoV') {
        this.Color = 'ion-col6x32';
      }
    }

    if (palabraClave == 'Trabajo') {
      if ((tipo == 'Pequeño') || (tipo == 'ExtralargoH') || (tipo == 'LargoH')) {
        this.Color = 'ion-col33';
      }
      if ((tipo == 'Grande2') || (tipo == 'GrandeXL')) {
        this.Color = 'ion-col6x63';
      }
      if (tipo == 'LargoV') {
        this.Color = 'ion-col6x33';
      }
    }

    if (palabraClave == 'Comidas') {
      if ((tipo == 'Pequeño') || (tipo == 'ExtralargoH') || (tipo == 'LargoH')) {
        this.Color = 'ion-col34';
      }
      if ((tipo == 'Grande2') || (tipo == 'GrandeXL')) {
        this.Color = 'ion-col6x64';
      }
      if (tipo == 'LargoV') {
        this.Color = 'ion-col6x34';
      }
    }

    console.log('El color es:', this.Color);
  }

  applyFilters(tipoPauta: string) {
    if (tipoPauta === 'Clasificado') {
      this.ObtenerColor(this.clasi.PalabrasClave, this.clasi.Tipo);
    } else {
      this.ObtenerColor(this.FeriEvent.PalabrasClave, this.FeriEvent.Tipo);
    }

    setTimeout(() => {
      if (tipoPauta === 'Clasificado') {
        this.registro.RegistrarClasificado(
          this.valor.identificacion,
          this.clasi.Titulo,
          this.clasi.Descripcion,
          this.clasi.Contacto,
          this.clasi.PalabrasClave,
          this.clasi.Tipo,
          this.Color,
          String(this.IDClasificado));
        if (this.platform.is('android')) {
          this.SubirFoto(this.IDClasificado, 'uploadPhotoClas.php');
        }
        this.presentarLoading();
        setTimeout(() => {
          this.comprobarPautaRealizada(this.registro.RTA_Clasificado);
        }, 2000);
      }
      //Si no es un clasificado entonces es un evento
      else {

        var fechaHoy = new Date();
        //console.log(fechaHoy.getDate());
        //console.log(fechaHoy.getMonth());
        //console.log(fechaHoy.getFullYear());

        let FF = this.FeriEvent.fechaFin.split('-');
        let FI = this.FeriEvent.fechaIni.split('-');
        //console.log(FF[0]);
        //console.log(FF[1]);
        //console.log(FF[2]);

        //VALIDANDO QUE LAS FECHAS PUESTAS NO HAYAN PASADO YA

        if(Number(FF[0]) === fechaHoy.getFullYear()){
          if (((Number(FF[1]) === (fechaHoy.getMonth() + 1)) && (Number(FF[2]) > fechaHoy.getDate())) || (Number(FF[1]) > (fechaHoy.getMonth() + 1))){
            this.validFecha = true;
            console.log('ENTRÓ A PONER EN VERDADERO1');
          }
          else{
            this.validFecha = false;
            //console.log('ENTRÓ A PONER EN FALSO1');
          }
        }
        else if (Number(FF[0]) > fechaHoy.getFullYear() && (Number(FI[0]) < Number(FF[0]))) {
          this.validFecha = true;
          console.log('ENTRÓ A PONER EN VERDADERO2');
          console.log(FF[0]);
          console.log(fechaHoy.getFullYear());
        }
        else{
          this.validFecha = false;
          //console.log('ENTRO A PONER EN FALSO2');
        }

        //VALIDANDO QUE SE HAYA ELEGIDO TAMAÑO DE FOTO
        console.log('ESTE ES EL TIPO (TAMAÑO) A VALIDAR');
        console.log(this.FeriEvent.Tipo);
        if(this.FeriEvent.Tipo !== ''){
          this.validFoto = true;
        }
        else{
          this.validFoto = false;
        }

        //MOSTRANDO MENSAJES DE ERROR EN BASE A LAS POSIBLES INVALIDACIONES

        if(!this.validFecha && this.precioPauta <= 0 && !this.validFoto){
          this.validFoto = false;
          this.presentarToastError('Falta elegir fechas válidas y tamaño de la puata !');
          return;
        }
        else if(!this.validFecha){
          this.presentarToastError('Rango de Fechas No Válidas!');
          return;
        }
        else if(this.precioPauta <= 0){
          console.log('AQUÍ ESTÁ EL PRECIO DE LA PAUTA:');
          console.log(this.precioPauta);
          this.validFoto = false;
          this.presentarToastError('Debe elegir un tamaño de puta!');
          return;
        }

        //Si llega a este punto es porque son válidos todos los campos requeridos
        let ciudadPaisTemp = this.FeriEvent.Ciudad.split(' - ');

        this.FeriEvent.Ciudad = ciudadPaisTemp[0];
        this.FeriEvent.Pais = ciudadPaisTemp[1];
/*
        console.log('IDENTIFICACION: ' + this.valor.identificacion);
        console.log('CIUDAD: ' + this.FeriEvent.Ciudad);
        console.log('PAIS: ' + this.FeriEvent.Pais);
        console.log('TITULO: ' + this.FeriEvent.Titulo);
        console.log('DIRECCION: ' + this.FeriEvent.Direccion);
        console.log('FECHA INICIAL: ' + this.FeriEvent.fechaIni);
        console.log('FECHA FINAL: ' + this.FeriEvent.fechaFin);
        console.log('PALABRASCLAVE: ' + this.FeriEvent.PalabrasClave);
        console.log('TIPO: ' + this.FeriEvent.Tipo);
        console.log('TAMAÑO FOTO: ' + this.tamanoFoto);
        console.log('ID FERIA: ' + this.IDFeriaEvento);
*/
        this.registro.RegistrarFeriaEvento(
          this.valor.identificacion,
          this.FeriEvent.Ciudad,
          this.FeriEvent.Pais,
          this.FeriEvent.Titulo,
          this.FeriEvent.Direccion,
          this.FeriEvent.fechaIni,
          this.FeriEvent.fechaFin,
          this.precioPauta,
          this.FeriEvent.PalabrasClave,
          this.FeriEvent.Tipo,
          this.FeriEvent.Expositores,
          this.tamanoFoto,
          String(this.IDFeriaEvento));
        if (this.platform.is('android')) {
          this.SubirFoto(this.IDFeriaEvento, 'uploadPhotoFeriaEvento.php');
        }
        this.presentarLoading();
        setTimeout(() => {
          this.comprobarPautaRealizada(this.registro.RTA_Feria_evento);
        }, 2000);
      }

    }, 350);

  }
  comprobarPautaRealizada(RTA_Pauta: boolean) {
    if (RTA_Pauta === true) {
      this.dismiss();
      this.presentarToastOK();
    }
    if (RTA_Pauta === false) {
      this.presentarToastError('Ocurrió un error al subir el clasificado, intente más tarde de nuevo');
    }
  }
  dismiss(data?: any) {
    // using the injected ModalController this page
    // can "dismiss" itself and pass back data
    this.modalCtrl.dismiss(data);
  }

  GaleriaImagen() {
    const options: CameraOptions = {
      quality: 99,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      allowEdit: true,
      targetWidth: 600
    };

    this.camera.getPicture(options).then(
      imageData => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.fotografia = 'data:image/jpeg;base64,' + imageData;
        this.fototomada = true;
      },
      err => {
        // Handle error
        this.fototomada = false;
      }
    );
  }
  SubirFoto(IDPauta, servicio) {
    // create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();
    console.log('direccion: ' + urls.fotos + servicio);

    // option transfer
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: IDPauta + this.valor.identificacion + '.jpg',
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: 'image/jpeg',
      headers: {}
    };

    // file transfer action
    fileTransfer
      .upload(this.fotografia, urls.fotos + servicio, options)
      .then(
        data => {
          this.presentarToastOK();
        },
        err => {
          console.log(err);
          alert('Incorrecto');
        }
      );
  }

  async elegirTamanoFeriaEvento() {

    const modal = await this.modalCtrl.create({
      component: SliderTamanosPage,
    });
    await modal.present();

    const { data } = await modal.onDidDismiss();

    if(data.tamanoPFE){
      this.FeriEvent.Tipo = data.tamanoPFE;
      this.validFoto = true;
    }
    else{
      this.validFoto = false;
    }

    switch (data.tamanoPFE) {
      case 'Pequeño':
        this.tamanoFoto = 'ion-col3';
        this.anchoFoto = '4';
        break;
      case 'Grande2':
        this.tamanoFoto = 'ion-col6x6';
        this.anchoFoto = '8';
        break;
      case 'GrandeXL':
        this.tamanoFoto = 'ion-col6x6';
        this.anchoFoto = '12';
        break;
      case 'largoH':
        this.tamanoFoto = 'ion-col3';
        this.anchoFoto = '8';
        break;
      case 'ExtralargoH':
        this.tamanoFoto = 'ion-col3';
        this.anchoFoto = '12';
        break;
      case 'largoV':
        this.tamanoFoto = 'ion-col6x3';
        this.anchoFoto = '4';
        break;
      default:
        this.tamanoFoto = 'ion-col32';
        this.anchoFoto = '4';
    }
    this.tamanoPublicidad = data.tamanoPFE;

    this.CalcularPrecioPauta();
  }

  CargarPreciosTamanosPautas() {
    this.registro.cargarPreciosTamanosPautas().subscribe((PTPautas: any['records']) => {
      console.log(PTPautas);
      this.preciosTamPautas = PTPautas['Precios'];
      console.log(this.preciosTamPautas);
      //this.completo = 'true';
    }, (err: any) => {
    });
  }

  CalcularPrecioPauta() {

    //Procedimiento para ajustar rangos de fecha de finalización válidos
    let FInicial = this.FeriEvent.fechaIni.split('-');

    let anoFinal = String(Number(FInicial[0]) + 1);
    let mesFinal = String(Number(FInicial[1]) + 12);
    let diaFinal = FInicial[2];

    this.fechaFinalMinFeriaEvento = this.FeriEvent.fechaIni;
    this.fechaFinalMaxFeriaEvento = anoFinal + '-' + mesFinal + '-' + diaFinal;
    
    if (this.FeriEvent.fechaIni !== '' && this.disableFechaFin){
      this.disableFechaFin = false;
     

      console.log(this.fechaFinalMaxFeriaEvento);
      console.log(this.fechaFinalMinFeriaEvento);

    }
    else if (this.FeriEvent.fechaIni !== '' && this.FeriEvent.fechaFin !== '' && !this.disableFechaFin) {
      let FInicial = this.FeriEvent.fechaIni.split('-');
      let FFinal = this.FeriEvent.fechaFin.split('-');

      let diferenciaDias: number = Number(FFinal[2]) - Number(FInicial[2]);
      let diferenciaMeses: number = Number(FFinal[1]) - Number(FInicial[1]);
      let diferenciaAnos: number = Number(FFinal[0]) - Number(FInicial[0]);

      let totTiempo = ((diferenciaDias) + (diferenciaMeses * 30) + (diferenciaAnos * 365));
      console.log(totTiempo);
      if(totTiempo <= 0){
        //this.FeriEvent.fechaFin = 'Seleccione una fecha';
        this.validFecha = false;
        this.disableFechaFin = true;
        this.disableFechaFin = false;
        console.log('PASA POR AQUI A VACIAR LA FECHA SELECCIONADA DEBIDO A INCONSISTENCIA DE SELECCIÓN!!');
        return;
      }
      /*
            console.log(diferenciaDias);
            console.log(diferenciaMeses);
            console.log(diferenciaAnos);
            console.log((diferenciaDias) + (diferenciaMeses * 30) + (diferenciaAnos * 365));
            
            if(diferenciaDias < 0)
              diferenciaDias += 30;
            if(diferenciaMeses < 0)
              diferenciaMeses += 12;
      */
      console.log('LOS VALORES DEL ARRGLO QUE RECORRE EL FOR SON ESTOS:');
      console.log(this.preciosTamPautas[0].tamano);
      console.log(this.preciosTamPautas[0].precio);

      for (let i = 0; i < this.preciosTamPautas.length; i++) {
        if (this.preciosTamPautas[i].tamano === this.FeriEvent.Tipo) {
          this.precioPauta = Number(this.preciosTamPautas[i].precio);
        }
      }
      console.log('ESTE ES EL VALOR DE PRECIO PAUTA: ' + this.precioPauta);
      this.validFecha = true;
      let valorPauta = (this.precioPauta) * totTiempo;

      this.precioPauta = valorPauta;
      console.log(this.FeriEvent.fechaIni);
      console.log(this.FeriEvent.fechaFin);
      console.log(FInicial);
      console.log(diferenciaDias);
      console.log(diferenciaMeses);
      console.log(diferenciaAnos);
      console.log((diferenciaDias) + (diferenciaMeses * 30) + (diferenciaAnos * 365));
      console.log('EL VALOR PARA ESTA PAUTA ES: ');
      console.log(valorPauta);
      console.log(this.precioPauta);

    }
  }

}
