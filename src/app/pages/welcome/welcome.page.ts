import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, LoadingController } from '@ionic/angular';
import * as ver from '../../providers/data.version';
import { Valores } from '../../providers/valores';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  
  version = ver.version;
  public loading: any;

  constructor(public router: Router,
              public menu: MenuController,
              public valor: Valores,
              public loadingController: LoadingController) {
                this.menu.enable(true);
               }

  ngOnInit() {
  }

  ionViewWillEnter() {
    console.log(this.valor.identificacion);
  if (this.valor.identificacion !== null) {
    console.log('sesion iniciada');
    this.router.navigateByUrl('/app/tabs/find-people');
  }
  }

  abrirUsuario() {
    this.presentLoadingWithOptions();
    setTimeout(() => {
      this.router.navigateByUrl('/signup-person');
    }, 300);
  }

  abrirEmpresa() {
    this.presentLoadingWithOptions();
    setTimeout(() => {
      this.router.navigateByUrl('/signup-enterprise');
    }, 300);
  }

  async presentLoadingWithOptions() {
    this.loading = await this.loadingController.create({
      duration: 1500,
      message: 'Un momento, por favor',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    
    return await this.loading.present();
  }

  openTutorial() {
    this.router
    .navigateByUrl('/tutorial');
  }

  startApp() {
    this.router
      .navigateByUrl('/app/tabs/find-people');
     // .then(() => this.storage.set('ion_did_tutorial', 'true'));
  }

  login() {
    this.router
      .navigateByUrl('/login');
  }

  getSignedUp() {
    this.router.navigateByUrl('/signup');
  }

  ionViewDidLeave() {
    // enable the root left menu when leaving the tutorial page
   // this.menu.enable(true);
  }
}
