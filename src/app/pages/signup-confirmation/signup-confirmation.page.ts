import { Component, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { UserData } from '../../providers/user-data';
import { ToastController, LoadingController } from '@ionic/angular';
import { Location } from '@angular/common';

import { UserOptions } from '../../interfaces/user-options';
import { Valores } from '../../providers/valores';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';

@Component({
  selector: 'signup-confirmation',
  templateUrl: './signup-confirmation.page.html',
  styleUrls: ['./signup-confirmation.page.scss'],
})
export class SignupConfirmationPage {
  signup: UserOptions = { username: '', id_user: '', tel_cel: '', email: '', bornDate: '', password: '',};
  submitted = false;
  usr: any;
  carga_completa: boolean;
  pinInvalido: boolean = true;
  PIN: String;
  Identificacion: any;
  campPIN: string;
  pinIngresado: boolean;

  constructor(
    public router: Router,
    public valor: Valores,
    public userData: UserData,
    private route: ActivatedRoute,
    public keyboard: Keyboard,
    public toastController: ToastController,
    private location: Location,
    public registro: RegistroUsuariosService,
    public loadingController: LoadingController
  ) {
    console.log('el ID que llega aqui Signup Confirmation ' + this.valor.identificacion);
    this.Identificacion = this.valor.identificacion;
  }

    ionViewWillEnter() {
    this.carga_completa = false;
        setTimeout(() => {
      // console.log('Se refrescó correctamente 1');

      this.valor.cargar_id().subscribe((data: any[]) => {
      this.usr = data;
        this.carga_completa = true;
    }, (err: any) => {
      this.carga_completa = null;
      this.presentarErrorToast();
      console.log('error de registro!');
      // this.location.back(); //DESCOMENTAR
       // this.router.navigateByUrl('/tutorial');
    });
    }, 3000);
      console.log(this.Identificacion);

  }

  async presentarErrorToast() {
    const toast = await this.toastController.create({
      message: 'No hay conexión al servidor, ¡Registro erróneo!',
      showCloseButton: true,
      position: 'bottom',
      color: 'danger',
      duration: 5000
    });
    toast.present();
  }

  soloNumero(e) {
    // Se captura el código ASCII de la tecla presionada en la variable local "key"
    let key: number = e.keyCode || e.which;
    // console.log(key);

    // Se evalua si el código ASCII capturado corresponde a las letras del abcedario en mayúsculas o en minúsculas
    if ((key > 47 && key < 58) ) {
      return true; // Se permite la escritura en el ion-input
    }
    return false; // Se bloquea la escritura en el ion-input
  }

  onFollowingSignup(form: NgForm) {
    this.submitted = true;
    this.valor.identificacion = this.Identificacion;
    console.log('ValorID: ' + this.Identificacion);
    if (form.valid) {
    // this.valor.identificacion = this.Identificacion;
      this.registro.confirmarPIN(this.signup.password, this.valor.identificacion);
      this.verificarPin();
      this.presentLoadingWithOptions(700);
    }
   
  }

  async presentLoadingWithOptions(tiempo) {
    const loading = await this.loadingController.create({
      duration: tiempo,
      message: 'Un momento, por favor...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  verificarPin() {
    setTimeout(() => {

      if (this.registro.PINLlegado.mensaje == 'PIN Incorrecto') {
         this.pinIngresado = false;
      }

      if (this.registro.PINLlegado.mensaje == 'PIN Correcto') {
      //  this.pinIngresado = true;
        this.router.navigateByUrl('/complete-cv');
      }

    }, 700);
  }

  }
