import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SliderTamanosPage } from './slider-tamanos.page';

const routes: Routes = [
  {
    path: '',
    component: SliderTamanosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [/*SliderTamanosPage*/
  ]
})
export class SliderTamanosPageModule {}
