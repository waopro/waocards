import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderTamanosPage } from './slider-tamanos.page';

describe('SliderTamanosPage', () => {
  let component: SliderTamanosPage;
  let fixture: ComponentFixture<SliderTamanosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SliderTamanosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderTamanosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
