import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'slider-tamanos',
  templateUrl: './slider-tamanos.page.html',
  styleUrls: ['./slider-tamanos.page.scss'],
})
export class SliderTamanosPage implements OnInit {

  tamano:string = "Pequeño";
  conslider: boolean = false;
  //seleccionado:boolean = false;

  //index:any;

  constructor(private modalCtrlSelecionTamano: ModalController) { }

  cambio(event){

    event.target.getActiveIndex().then(index => {
      console.log(index);
      switch(index){
        case 0:
          this.tamano = "Pequeño";
          break;
        case 1:
          this.tamano = "Grande2";
          break;
        case 2:
          this.tamano = "GrandeXL";
          break;
        case 3:
          this.tamano = "LargoH";
          break;
        case 4:
          this.tamano = "ExtralargoH";
          break;
        case 5:
          this.tamano = "LargoV";
          break;
      }
    });
  }

  tamanoElegido(index){
    //this.seleccionado = true;
    if(!this.conslider){
      switch (index) {
        case 0:
          this.tamano = "Pequeño";
          break;
        case 1:
          this.tamano = "LargoH";
          break;
        case 2:
          this.tamano = "LargoV";
          break;
        case 3:
          this.tamano = "Grande2";
          break;
        case 4:
          this.tamano = "ExtralargoH";
          break;
        case 5:
          this.tamano = "GrandeXL";
          break;
      }
    }
    
    console.log('ESTE ES EL VALOR QUE PASA ENTES DE CERRAR EL MODAL DE SELECCION DE TAMAÑO: ' + this.tamano);
    setTimeout(() => {
      this.modalCtrlSelecionTamano.dismiss({
        tamanoPFE: this.tamano,
      });
    }, 500);

  }

  salir(){
    //this.seleccionado = false;
    this.modalCtrlSelecionTamano.dismiss({
      tamanoPFE: ' ',
      //tamanoSeleccionado: this.seleccionado
    });
  }

  ngOnInit() {
  }

}
