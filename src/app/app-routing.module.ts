import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/welcome',
    pathMatch: 'full'
  },
  {
    path: 'account',
    loadChildren: './pages/account/account.module#AccountModule'
  },
  {
    path: 'support',
    loadChildren: './pages/support/support.module#SupportModule'
  },
  {
    path: 'login',
    loadChildren: './pages/login/login.module#LoginModule'
  },
  {
    path: 'signup',
    loadChildren: './pages/signup/signup.module#SignUpModule'
  },
  {
    path: 'app',
    loadChildren: './pages/tabs-page/tabs-page.module#TabsModule'
  },
  {
    path: 'tutorial',
    loadChildren: './pages/tutorial/tutorial.module#TutorialModule'
  },
  {
    path: 'signup-confirmation',
    loadChildren: './pages/signup-confirmation/signup-confirmation.module#SignupConfirmationPageModule'
  },
  {
    path: 'complete-cv',
    loadChildren: './pages/complete-cv/complete-cv.module#CompleteCvPageModule'
  },
  {
    path: 'agitar',
    loadChildren: './pages/agitar/agitar.module#AgitarPageModule'
  },
  {
    path: 'about',
    loadChildren: './pages/about/about.module#AboutModule'
  },
  {
    path: 'find-people',
    loadChildren: './pages/find-people/find-people.module#FindPeoplePageModule'
  },
  {
    path: 'map',
    loadChildren: './pages/map/map.module#MapModule'
  },
  {
    path: 'detalle-clasificado',
    loadChildren: './pages/detalle-clasificado/detalle-clasificado.module#DetalleClasificadoPageModule'
  },
  {
    path: 'importar-contactos',
    loadChildren: './pages/importar-contactos/importar-contactos.module#ImportarContactosPageModule'
  },
  {
    path: 'manual',
    loadChildren: './pages/manual/manual.module#ManualPageModule'
  },
  {
    path: 'detallePersonaEncontrada',
    loadChildren: './pages/detalle-persona-encontrada/detalle-persona-encontrada.module#DetallePersonaEncontradaPageModule'
  },
  {
    path: 'Preferences',
    loadChildren: './pages/preferences/preferences.module#PreferencesPageModule'
  },
  {
    path: 'welcome',
    loadChildren: './pages/welcome/welcome.module#WelcomePageModule'
  },
  { path: 'solicitudes-pendientes',
  loadChildren: './pages/solicitudes-pendientes/solicitudes-pendientes.module#SolicitudesPendientesPageModule' 
  },
  { path: 'signup-person',
  loadChildren: './pages/signup-person/signup-person.module#SignupPersonPageModule' 
  },
  { path: 'signup-enterprise',
  loadChildren: './pages/signup-enterprise/signup-enterprise.module#SignupEnterprisePageModule'
  },
  {
    path: 'sliderTamanos',
    loadChildren: './pages/slider-tamanos/slider-tamanos.module#SliderTamanosPageModule'
  },
  { path: 'MiCV', loadChildren: './pages/mi-cv/mi-cv.module#MiCVPageModule' },
  { path: 'recuperar-contrasena', loadChildren: './pages/recuperar-contrasena/recuperar-contrasena.module#RecuperarContrasenaPageModule' },
  { path: 'listado-amigos', loadChildren: './pages/listado-amigos/listado-amigos.module#ListadoAmigosPageModule' },
  { path: 'compartirTarjeta', loadChildren: './pages/compartir-tarjeta/compartir-tarjeta.module#CompartirTarjetaPageModule' },
  { path: 'pagos', loadChildren: './pages/pagos/pagos.module#PagosPageModule' },
  { path: 'visibilidad-deseada', loadChildren: './pages/visibilidad-deseada/visibilidad-deseada.module#VisibilidadDeseadaPageModule' },
  { path: 'calificaciones/:Identificacion', loadChildren: './pages/calificaciones/calificaciones.module#CalificacionesPageModule' }





];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
