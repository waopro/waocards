import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicModule } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage';
import { Animation, NavOptions } from '@ionic/core';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AgitarPage } from './pages/agitar/agitar.page';
import { CompartirTarjetaPage } from './pages/compartir-tarjeta/compartir-tarjeta.page';
import { DeviceMotion} from '@ionic-native/device-motion/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Vibration } from '@ionic-native/vibration/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

// NUESTROS SERVICIOS
import { RegistroUsuariosService } from './providers/registro-usuarios.service';
import { Valores } from './providers/valores';
import { Ubicacion } from './providers/ubicacion';
import { MapPage } from './pages/map/map';
import { FindPeoplePage } from './pages/find-people/find-people.page';
import { UserData } from './providers/user-data';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Globalization } from '@ionic-native/globalization/ngx';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { Contactos } from './providers/contactos';
import { Contacts } from '@ionic-native/contacts/ngx';
import { FormsModule } from '@angular/forms';
import { BusquedaManual } from './providers/busqueda-manual.service';
import { DetallePersonaEncontradaPage } from './pages/detalle-persona-encontrada/detalle-persona-encontrada.page';
import { ManualPage } from './pages/manual/manual.page';
import { TabsPage } from './pages/tabs-page/tabs-page';
import { Downloader } from '@ionic-native/downloader/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { TitleCasePipe } from '@angular/common';
import { LoginPage } from './pages/login/login';
import { CompleteCvPage } from './pages/complete-cv/complete-cv.page';
import { SignupConfirmationPage } from './pages/signup-confirmation/signup-confirmation.page';
import { SignupPage } from './pages/signup/signup';
import { SliderTamanosPage } from './pages/slider-tamanos/slider-tamanos.page';
import { MiCVPage } from './pages/mi-cv/mi-cv.page';
import { ChatPage } from './pages/chat/chat.page';
import { Device } from '@ionic-native/device/ngx';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { WAOChatService } from './providers/waochat.service';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { ListadoAmigosPage } from './pages/listado-amigos/listado-amigos.page';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { PDFService } from './providers/pdf.service';
import { PagosPage } from './pages/pagos/pagos.page';
import { WelcomePage } from './pages/welcome/welcome.page';
import { VisibilidadDeseadaPage } from './pages/visibilidad-deseada/visibilidad-deseada.page';
// import { SharedModuleModule } from './shared-module.module';
// import { RatingModule } from 'ng-starrating';
import { StarRating } from 'ionic4-star-rating';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AutoCompliteInputComponent } from './components/auto-complite-input/auto-complite-input.component';
import { AutoCompliteModule } from './components/auto-complite-input/auto-complite.module';

// Carga archivos de idioma escritos en formato JSON.
export function customTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    AutoCompliteModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    // SharedModuleModule,
    IonicModule.forRoot({
      navAnimation: MyTransitionAnimation,
    }),
    IonicStorageModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production
    }),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: customTranslateLoader,
        deps: [HttpClient]
      }
    }),
  ],
  exports: [],
  declarations: [AppComponent, AgitarPage, ManualPage, DetallePersonaEncontradaPage, SliderTamanosPage,
     MiCVPage, ChatPage, ListadoAmigosPage, CompartirTarjetaPage, PagosPage, VisibilidadDeseadaPage],
  providers: [InAppBrowser,
              UserData,
              SplashScreen,
              StatusBar,
              RegistroUsuariosService,
              PDFService,
              Ubicacion,
              Valores,
              Geolocation,
              DeviceMotion,
              MapPage,
              FindPeoplePage,
              LocationAccuracy,
              Vibration,
              LocalNotifications,
              Globalization,
              Device,
              TabsPage,
              UniqueDeviceID,
              Contactos,
              Contacts,
              DetallePersonaEncontradaPage,
              Downloader,
              FileOpener,
              TitleCasePipe,
              LoginPage,
              SignupPage,
              WelcomePage,
              SignupConfirmationPage,
              CompleteCvPage,
              AppComponent,
              WAOChatService,
              ChatPage,
              FileChooser,
              DocumentViewer,
              SocialSharing
                ],
  bootstrap: [AppComponent],
  entryComponents: [AgitarPage, ManualPage, DetallePersonaEncontradaPage, SliderTamanosPage, MiCVPage, ChatPage, ListadoAmigosPage,
                    CompartirTarjetaPage, PagosPage, CompartirTarjetaPage, VisibilidadDeseadaPage
                  ],
})
export class AppModule {}
const DURATION = 500;
const EASING = 'cubic-bezier(0.36,0.66,0.04,1)';
const OPACITY = 'opacity';
const TRANSFORM = 'transform';
const TRANSLATEX = 'translateX';
const CENTER = '0%';
const OFF_OPACITY = 0.8;
export function shadow(el) {
  return el.shadowRoot || el;
}
export function MyTransitionAnimation(AnimationC, navEl, opts) {
  const isRTL = document.dir === 'rtl';
  const OFF_RIGHT = isRTL ? '-99.5%' : '99.5%';
  const OFF_LEFT = isRTL ? '33%' : '-33%';
  const enteringEl = opts.enteringEl;
  const leavingEl = opts.leavingEl;
  const rootTransition = new AnimationC();
  rootTransition
    .addElement(enteringEl)
    .duration(opts.duration || DURATION)
    .easing(opts.easing || EASING)
    .beforeRemoveClass('ion-page-invisible');
  if (leavingEl && navEl) {
    const navDecor = new AnimationC();
    navDecor
      .addElement(navEl);
    rootTransition.add(navDecor);
  }
  const backDirection = (opts.direction === 'back');
  const contentEl = enteringEl.querySelector(':scope > ion-content');
  const headerEls = enteringEl.querySelectorAll(':scope > ion-header > *:not(ion-toolbar), :scope > ion-footer > *');
  const enteringToolBarEle = enteringEl.querySelector(':scope > ion-header > ion-toolbar');
  const enteringContent = new AnimationC();
  if (!contentEl && !enteringToolBarEle && headerEls.length === 0) {
    enteringContent.addElement(enteringEl.querySelector(':scope > .ion-page, :scope > ion-nav, :scope > ion-tabs'));
  }
  else {
    enteringContent.addElement(contentEl);
    enteringContent.addElement(headerEls);
  }
  rootTransition.add(enteringContent);
  if (backDirection) {
    enteringContent
      .beforeClearStyles([OPACITY])
      .fromTo(TRANSLATEX, OFF_LEFT, CENTER, true)
      .fromTo(OPACITY, OFF_OPACITY, 1, true);
  }
  else {
    enteringContent
      .beforeClearStyles([OPACITY])
      .fromTo(TRANSLATEX, OFF_RIGHT, CENTER, true);
  }
  if (enteringToolBarEle) {
    const enteringToolBar = new AnimationC();
    enteringToolBar.addElement(enteringToolBarEle);
    rootTransition.add(enteringToolBar);
    const enteringTitle = new AnimationC();
    enteringTitle.addElement(enteringToolBarEle.querySelector('ion-title'));
    const enteringToolBarItems = new AnimationC();
    enteringToolBarItems.addElement(enteringToolBarEle.querySelectorAll('ion-buttons,[menuToggle]'));
    const enteringToolBarBg = new AnimationC();
    enteringToolBarBg.addElement(shadow(enteringToolBarEle).querySelector('.toolbar-background'));
    const enteringBackButton = new AnimationC();
    const backButtonEl = enteringToolBarEle.querySelector('ion-back-button');
    enteringBackButton.addElement(backButtonEl);
    enteringToolBar
      .add(enteringTitle)
      .add(enteringToolBarItems)
      .add(enteringToolBarBg)
      .add(enteringBackButton);
    enteringTitle.fromTo(OPACITY, 0.01, 1, true);
    enteringToolBarItems.fromTo(OPACITY, 0.01, 1, true);
    if (backDirection) {
      enteringTitle.fromTo(TRANSLATEX, OFF_LEFT, CENTER, true);
      enteringBackButton.fromTo(OPACITY, 0.01, 1, true);
    }
    else {
      enteringTitle.fromTo(TRANSLATEX, OFF_RIGHT, CENTER, true);
      enteringToolBarBg
        .beforeClearStyles([OPACITY])
        .fromTo(OPACITY, 0.01, 1, true);
      enteringBackButton.fromTo(OPACITY, 0.01, 1, true);
      if (backButtonEl) {
        const enteringBackBtnText = new AnimationC();
        enteringBackBtnText
          .addElement(shadow(backButtonEl).querySelector('.button-text'))
          .fromTo(TRANSLATEX, (isRTL ? '-100px' : '100px'), '0px');
        enteringToolBar.add(enteringBackBtnText);
      }
    }
  }
  if (leavingEl) {
    const leavingContent = new AnimationC();
    leavingContent.addElement(leavingEl.querySelector(':scope > ion-content'));
    leavingContent.addElement(leavingEl.querySelectorAll(':scope > ion-header > *:not(ion-toolbar), :scope > ion-footer > *'));
    rootTransition.add(leavingContent);
    if (backDirection) {
      leavingContent
        .beforeClearStyles([OPACITY])
        .fromTo(TRANSLATEX, CENTER, (isRTL ? '-100%' : '100%'));
    }
    else {
      leavingContent
        .fromTo(TRANSLATEX, CENTER, OFF_LEFT, true)
        .fromTo(OPACITY, 1, OFF_OPACITY, true);
    }
    const leavingToolBarEle = leavingEl.querySelector(':scope > ion-header > ion-toolbar');
    if (leavingToolBarEle) {
      const leavingToolBar = new AnimationC();
      leavingToolBar.addElement(leavingToolBarEle);
      const leavingTitle = new AnimationC();
      leavingTitle.addElement(leavingToolBarEle.querySelector('ion-title'));
      const leavingToolBarItems = new AnimationC();
      leavingToolBarItems.addElement(leavingToolBarEle.querySelectorAll('ion-buttons,[menuToggle]'));
      const leavingToolBarBg = new AnimationC();
      leavingToolBarBg.addElement(shadow(leavingToolBarEle).querySelector('.toolbar-background'));
      const leavingBackButton = new AnimationC();
      const backButtonEl = leavingToolBarEle.querySelector('ion-back-button');
      leavingBackButton.addElement(backButtonEl);
      leavingToolBar
        .add(leavingTitle)
        .add(leavingToolBarItems)
        .add(leavingBackButton)
        .add(leavingToolBarBg);
      rootTransition.add(leavingToolBar);
      leavingBackButton.fromTo(OPACITY, 0.99, 0, true);
      leavingTitle.fromTo(OPACITY, 0.99, 0, true);
      leavingToolBarItems.fromTo(OPACITY, 0.99, 0, true);
      if (backDirection) {
        leavingTitle.fromTo(TRANSLATEX, CENTER, (isRTL ? '-100%' : '100%'));
        leavingToolBarBg
          .beforeClearStyles([OPACITY])
          .fromTo(OPACITY, 1, 0.01, true);
        if (backButtonEl) {
          const leavingBackBtnText = new AnimationC();
          leavingBackBtnText.addElement(shadow(backButtonEl).querySelector('.button-text'));
          leavingBackBtnText.fromTo(TRANSLATEX, CENTER, (isRTL ? -124 : 124) + 'px');
          leavingToolBar.add(leavingBackBtnText);
        }
      }
      else {
        leavingTitle
          .fromTo(TRANSLATEX, CENTER, OFF_LEFT)
          .afterClearStyles([TRANSFORM]);
        leavingBackButton.afterClearStyles([OPACITY]);
        leavingTitle.afterClearStyles([OPACITY]);
        leavingToolBarItems.afterClearStyles([OPACITY]);
      }
    }
  }
  return Promise.resolve(rootTransition);
}
