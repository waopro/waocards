import { Component, OnInit, Input } from '@angular/core';
import { RegistroUsuariosService } from '../../providers/registro-usuarios.service';
import { Valores } from '../../providers/valores';

@Component({
  selector: 'auto-complite-input',
  templateUrl: './auto-complite-input.component.html',
  styleUrls: ['./auto-complite-input.component.scss']
})
export class AutoCompliteInputComponent implements OnInit {

  @Input() parametroBusqueda: string;
  @Input() placeholder: string;
  hayCoincidencias: boolean = false;
  cadena = '';
  clicked = false;
  coincidencias: any = [{
    Mensaje: ''
  }];

  constructor(
    private registro: RegistroUsuariosService,
    private valor: Valores
  ) { }

  ngOnInit() {
  }

  listar(event){
    if(!this.clicked){
      this.registro.listarCoincidencias(this.valor.identificacion, event.detail.value, this.parametroBusqueda)
        .subscribe(respuesta => {
          console.log(respuesta);
          this.coincidencias = respuesta;
          this.hayCoincidencias = true;
          // this.referidos = respuesta;
        });
      console.log("ESTAMOS LISTANDO");
      console.log(event.detail.value);
    }
    else{
      this.clicked = false;
    }

  }

}
