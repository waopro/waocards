import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutoCompliteInputComponent } from './auto-complite-input.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
 //import { Component } from '@angular/core';

@NgModule({
  declarations: [AutoCompliteInputComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ],
  exports: [
    AutoCompliteInputComponent
  ]
})
export class AutoCompliteModule { }
