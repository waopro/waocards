import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoCompliteInputComponent } from './auto-complite-input.component';

describe('AutoCompliteInputComponent', () => {
  let component: AutoCompliteInputComponent;
  let fixture: ComponentFixture<AutoCompliteInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoCompliteInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoCompliteInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
