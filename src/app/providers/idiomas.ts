import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { UserData } from './user-data';

@Injectable({
  providedIn: 'root'
})
export class Idiomas {
  data: any;

  constructor(public http: HttpClient,
              public user: UserData) {}

  cargarIdiomas() {
    return this.http.get('./assets/data/idiomas.json');
  }

}
