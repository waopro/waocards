import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_waocardsService1 } from './urls.servicios';
//import * as urls from './urls.servicios';
import { Valores } from './valores';

@Injectable({
  providedIn: 'root'
})
export class PagosService {

  constructor(
    public http: HttpClient,
    public valore: Valores
  ) { }

  pagarTarjetas(idUsr: string, cantidadTarjetas: string){
    const datosCompraTarjetas = {
      usuario: idUsr,
      cantidadTarjetas: cantidadTarjetas
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    return this.http
      .post(
        URL_waocardsService1 + '/Pagos',
        datosCompraTarjetas
      );
  }
}
