import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserData } from './user-data';
import * as urls from '../providers/urls.servicios';


@Injectable({
  providedIn: 'root'
})
export class Experiencias {

  data: any;

  constructor(public http: HttpClient,
              public user: UserData,) { }

  load(): any {
    if (this.data) {
      return of(this.data);
    } else {
      return this.http
        .get(urls.URLphpCrud + 'experiencias_laborales');
      // .pipe(map(this.processData, this));
    }
  }



  getExperiencia() {
    return this.load().pipe(
      map((data: any) => {
        return data.records.sort((a: any, b: any) => {
          const aName = a.Identificacion.split(' ').pop();
          const bName = b.Identificacion.split(' ').pop();
          return aName.localeCompare(bName);
        });
      })
    );
  }

}
