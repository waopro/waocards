import { Injectable, ViewChild } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { MensajeWAOChat } from '../interfaces/mensaje-waochat';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransferObject, FileTransfer, FileUploadOptions } from '@ionic-native/file-transfer/ngx';
import * as urls from './urls.servicios';
import { fechaCompilacion } from './data.version';
import { IonInfiniteScroll } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class WAOChatService {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  private itemmsCollections1: AngularFirestoreCollection<MensajeWAOChat>;
  private itemmsCollections2: AngularFirestoreCollection<MensajeWAOChat>;
  data: any[] = Array(29);
  fotografia: any;

  constructor(
    private afs: AngularFirestore,
    public camera: Camera,
    public transfer: FileTransfer
    ) {

     }

     cargarmensajes1(idContact: string, idUser: string, limite: number){

        console.log('SUBSCRITO SUPUESTAMENTE A ' + idContact + '_' + idUser);
       //console.log(limite);

       this.itemmsCollections1 = this.afs.collection<MensajeWAOChat>(idContact + '_' + idUser, ref => ref.orderBy('fecha', "desc").limit(limite));
       return this.itemmsCollections1.valueChanges();
     }

     cargarmensajes2(idContact: string, idUser: string, limite: number){
       console.log('SUBSCRITO SUPUESTAMENTE A ' + idUser + '_' + idContact);
       //console.log(limite);

       this.itemmsCollections2 = this.afs.collection<MensajeWAOChat>(idUser + '_' + idContact, ref => ref.orderBy('fecha', "desc").limit(limite));
       return this.itemmsCollections2.valueChanges();
     }

     enviarMensaje(idContact: string, idUser: string, usrName: string, msg: string, imgMsg?: string, PDFMsg?: string) {
      let mensajeChat;
      console.log('Recibido ', idContact, idUser, usrName, msg, imgMsg, PDFMsg);
      if (imgMsg || PDFMsg) {
        if (msg === '') {msg = ' '}
        mensajeChat = {
          nombre: usrName,
          mensaje: msg,
          UsrFoto: idUser + '.jpg',
          fecha: new Date().getTime(),
          imagenMsg: imgMsg,
          PDFMsg: PDFMsg
        };
   if (!PDFMsg) {
     console.log('Subio imagen');
    this.SubirFotoWaoChat(imgMsg);
  }
      }
      else {
        mensajeChat = {
          nombre: usrName,
          mensaje: msg,
          UsrFoto: idUser + '.jpg',
          fecha: new Date().getTime()
        };
       }
       this.afs.collection<MensajeWAOChat>(idContact + '_' + idUser).add(mensajeChat).then(response => {

         console.log('SUPUESTAMENTE YA SE ESCRIBIÓ EN LA BASE DE DATOS DEL CHAT');
         console.log(response);
       }).catch(error => {
         console.log(error);
       });
     }

  iniciarChat(idContact: string, idUser: string, usrName: string, msgs: number){
    let collection: string = '';
    if(msgs === 0){
      collection = idContact + '_' + idUser;
    }
    else{
      collection = idUser + '_' + idContact;
    }
    this.afs.collection<MensajeWAOChat>(collection).add(
       {
         nombre: usrName,
         mensaje: '',
         UsrFoto: idUser + '.jpg',
         fecha: new Date().getTime()
       }
       ).then(response => {
         //this.nameValue = this.placeValue = '';
         console.log('SUPUESTAMENTE YA SE CREO LA COLECCION' + collection);
         console.log(response);
       }).catch(error => {
         console.log(error);
       });
  }

  galeriaImagen() {
    const options: CameraOptions = {
      quality: 95,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      allowEdit: true,
      targetWidth: 600,
      targetHeight: 600
    };

    return this.camera.getPicture(options);
  }

  SubirFotoWaoChat(imgName: string) {
    // create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();
    console.log('direccion: ' + urls.URL_waocardsService1 + '/UploadPhotoWaoChat');

    // option transfer
    let options: FileUploadOptions = {
      fileKey: 'photoWaoChat',
      fileName: imgName,
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: 'image/jpeg',
      headers: {}
    };

    // file transfer action
    fileTransfer
      .upload(this.fotografia, urls.URL_waocardsService1 + '/UploadPhotoWaoChat', options)
      .then(
        data => {
          // alert('Correcto');
          console.log('LA DATA DESPUÉS DE SUBIR LA FOTO DESDE EL SERVICIO WAOCHAT AL CHAT ES: ');
          console.log(data);
        },
        err => {
          console.log('EL ERROR QUE DEVUELVE EL SERVICIO AL INTENTAR SUBIR LA FOTO DESDE EL SERVICIO WAOCHAT AL CHAT ES: ');
          console.log(err);
          // alert('Incorrecto');
        }
      );
  }

  TomarFotoWaoChat(){
    const options: CameraOptions = {
      quality: 95,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
      allowEdit: true,
      targetWidth: 600,
      targetHeight: 600
    };

    return this.camera.getPicture(options);
  }

  cargar_mensajes_anteriores( posicion_actual: any){
    console.log( 'Posicion actual!!!');
    console.log(posicion_actual)  
  }
  cargar_mensajes_finales( posicion_final: any){
    console.log( 'Posicion final!!!');
    console.log(posicion_final)  
  
  }
  //loadData(event) {
    //setTimeout(() => {
      //console.log('Done');
      //event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      //if (this.data.length <= 3) {
        //this.data.push('1');
        //console.log('carga');
      //}
      //if (this.data.length === 3) {
        //event.target.disabled = true;
        //this.data.push('1');
        //console.log('no carga')
      //}
   //}, 500);
  //} 
  loadData(event){
    console.log('cargando siguientes');

    setTimeout(()=> {

      if(this.data.length>50){
        event.target.complete();
        this.infiniteScroll.disabled=true;
        return;
      }
      const nuevoArr= Array(20);
      this.data.push(nuevoArr);
      event.target.complete();

    }, 1000 );
  }


  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }
  
}


