import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { URL_waocardsService1 } from './urls.servicios';

@Injectable({
  providedIn: 'root'
})
export class BusquedaManual {

  RTAbusqueda:Object = [];

  constructor(private http: HttpClient) { }

  buscarManual(cadenaBusquedaManual, identificacion){

    let datosBusquedaManual = {cadenaOriginal: cadenaBusquedaManual, Identificacion: identificacion};
    // ESTE POST HACE REALIZA LA BUSQUEDA EN LA BASE DE DATOS
    return this.http
    .post(URL_waocardsService1 + "/BusquedasManuales", datosBusquedaManual);
  }
}
