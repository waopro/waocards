import { TestBed } from '@angular/core/testing';

import { IngresoPlataformaService } from './ingreso-plataforma.service';

describe('IngresoPlataformaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IngresoPlataformaService = TestBed.get(IngresoPlataformaService);
    expect(service).toBeTruthy();
  });
});
