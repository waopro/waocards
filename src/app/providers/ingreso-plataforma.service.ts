import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { URL_waocardsService1 } from './urls.servicios';


@Injectable({
  providedIn: 'root'
})
export class IngresoPlataformaService {

  //RTA: any;

  constructor(private httpCliente:HttpClient) {
    console.log('El servicio de ingreso a la plataforma está bien configurado y listo para ser usado'); 
   }

   validarDatos(user: string, password: string, UUID: string){

    let datosUsuario = {
      usuario: user,
      contrasena: password,
      UUID: UUID
    }
    // ESTE POST VERIFICA LA EXISTENCIA Y/O VALIDEZ DE LOS DATOS INGRESADOS POR EL USUARIO EN LA PÁGINA DE LOGIN
    return this.httpCliente
      .post(URL_waocardsService1 + '/Login', datosUsuario);
      //.pipe(map(respuesta =>this.RTA = respuesta));
      /*.subscribe((respuesta: LoginValidation) => {
        //console.log(respuesta.id_usuario);
        //console.log(respuesta.token);
        //console.log(respuesta.mensaje);
        
        this.RTA = respuesta;        

      });*/

      //return this.RTA;
   }
}
