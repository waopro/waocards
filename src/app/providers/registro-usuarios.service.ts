import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { URL_waocardsService1 } from './urls.servicios';
import * as urls from './urls.servicios';
import { Valores } from './valores';

@Injectable({
  providedIn: 'root'
})
export class RegistroUsuariosService {
  public RTA: any;

  public NuevasTarjetasRepartidas: any = [{
    nuevasTarjetas: '',
    cantidad: ''
  }];
  public MisTarjetasRecibidas: any = [{
    Tarjetas: '',
    cantidad: ''
  }];
  public PersonasAntesdeRepartir: any = [{
    Personas: ''
  }];
  public referidos: any = [{
    error: '',
    mensaje: ''
  }];
  public ContactosenWAOCards: any = [{
    contactos: ''
  }];
  public PINGenerado: any = [{
    mensaje: '',
    PINAsignado: ''
  }];
  public PINLlegado: any = [{
    mensaje: ''
  }];
  public PersonasCercanas: any = [{
    cercanos: ''
  }];
  public Repartidas: any = [{
    Personas: ''
  }];
  public Amigos: any = [{
    amigos: ''
  }];
  public AmigosEnComun: any = [{
    amigoscomun: ''
  }];
  public SolicitudComprobada: any = [{
    solicitudenviada: ''
  }];
  verificacionContrasena = false;
  public solicitudesPendientes: any = [{
    solicitudes: '',
    cantidad: ''
  }];
  public CargaAmigosCompleta: boolean;
  public CargaAmigosComunCompleta: boolean;
  public RTA_Clasificado: boolean = false;
  public RTA_Feria_evento: boolean = false;
  public Contrasena: boolean;
  public ContactoCorrecto = false;
  public IDRetornado: any = [{
    Identificacion: ''
  }];
  // public VisibilidadCampoAccionUsuario: any = [{
  //     Preferencias: '',
  //     CampoAccion: ''
  //   }];
  constructor(private http: HttpClient,
              public valor: Valores) {
    // console.log('El servicio de registro está bien configurado y listo para ser usado');
  }

  preregistrar(
    user: string,
    telCel: string,
    email: string,
    bornDate: string,
    id_user: string
  ): any {
    const datosPreRegistro = {
      user: user,
      telCel: telCel,
      email: email,
      bornDate: bornDate,
      id_user: id_user
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    return this.http
      .post(URL_waocardsService1 + '/PreRegistroUsuario', datosPreRegistro); 
  }

  preregistroEmpresa(
    enterprisename: string,
    nit_enterprise: string,
    Enterprise_tel_cel: string,
    Enterpriseemail: string,
    ciiu: string
  ): any {
    const datosPreRegistro = {
      enterprisename: enterprisename,
      nit_enterprise: nit_enterprise,
      Enterprise_tel_cel: Enterprise_tel_cel,
      Enterpriseemail: Enterpriseemail,
      ciiu: ciiu
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/PreRegistroEmpresa', datosPreRegistro)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      }, (err: any) => {
          this.RTA = 'error';
          // console.log(this.RTA);
      });
  }

  registrar(
    Tipo_Perfil: string,
    Cargo_Actual: string,
    Ciudad_Residencia: string,
    Direccion_Residencia: string,
    Perfil: string,
    Nivel_Academico: string,
    Twitter: string,
    Instagram: string,
    Linkedin: string,
    Github: string,
    WWW: string,
    Profesion: string,
    Institucion_Educativa: string,
    Ha_Trabajado: string,
    Cuantos_Idiomas: string,
    Empresa: string,
    id_user: string
  ): any {
    const datosRegistro = {
      Tipo_Ocupacion: Tipo_Perfil,
      Cargo_Actual: Cargo_Actual,
      Ciudad_Residencia: Ciudad_Residencia,
      Direccion_Residencia: Direccion_Residencia,
      Perfil: Perfil,
      Nivel_Academico: Nivel_Academico,
      Twitter: Twitter,
      Instagram: Instagram,
      Linkedin: Linkedin,
      Github: Github,
      WWW: WWW,
      Profesion: Profesion,
      Institucion_Educativa: Institucion_Educativa,
      Ha_Trabajado: Ha_Trabajado,
      Cuantos_Idiomas: Cuantos_Idiomas,
      Empresa: Empresa,
      id_user: id_user
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/RegistroUsuario', datosRegistro)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroProfesion(Profesion: string, Descripcion: string): any {
    const datosRegistroProfesion = {
      Profesion: Profesion,
      Descripcion: Descripcion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(
        URL_waocardsService1 + '/RegistroProfesiones',
        datosRegistroProfesion
      )
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroEduucacion(
    Nivel_Academico: string,
    Institucion_Educativa: string,
    Identificacion: string,
    Titulo_Obtenido: string,
    Ciudad: string,
    Fecha_Inicio: string,
    Fecha_Fin: string
  ): any {
    const datosRegistroEducacion = {
      Nivel_Academico: Nivel_Academico,
      Institucion_Educativa: Institucion_Educativa,
      Identificacion: Identificacion,
      Titulo_Obtenido: Titulo_Obtenido,
      Ciudad: Ciudad,
      Fecha_Inicio: Fecha_Inicio,
      Fecha_Fin: Fecha_Fin
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(
        URL_waocardsService1 + '/RegistroFormacionAcademica',
        datosRegistroEducacion
      )
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroEmpresas(
    NIT: string,
    RazonSocial: string,
    Direccion: string,
    Telefonos: string,
    Correos: string,
    Ciudad: string
  ): any {
    const datosRegistroEmpresas = {
      NIT: NIT,
      RazonSocial: RazonSocial,
      Direccion: Direccion,
      Telefonos: Telefonos,
      Correos: Correos,
      Ciudad: Ciudad
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/RegistroEmpresas', datosRegistroEmpresas)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroTipoPerfil(
    Identificacion: string,
    Tipo_Ocupacion: string
  ): any {
    const datosRegistroTipo = {
      Identificacion: Identificacion,
      Tipo_Ocupacion: Tipo_Ocupacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/RegistroTipoUsr', datosRegistroTipo)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroExperiencia(
    Identificacion: string,
    Cargo: string,
    Empresa: string,
    Funciones_Cargo: string,
    Fecha_Inicio: string,
    Fecha_Fin: string,
    Telefono1: string,
    Telefono2: string
  ): any {
    const datosRegistroExperiencia = {
      Identificacion: Identificacion,
      Cargo: Cargo,
      Empresa: Empresa,
      Funciones_Cargo: Funciones_Cargo,
      Fecha_Inicio: Fecha_Inicio,
      Fecha_Fin: Fecha_Fin,
      Telefono1: Telefono1,
      Telefono2: Telefono2
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(
        URL_waocardsService1 + '/RegistroExperienciasLaborales',
        datosRegistroExperiencia
      )
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroHabilidad(
    Aptitud: string,
    Descripcion: string,
    Identificacion: string,
    ):
    any {
    const datosHabilidad = {
      Aptitud: Aptitud,
      Descripcion: Descripcion,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/RegistroAptitudes', datosHabilidad)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroReferencias(
    Tipo: string,
    Nombre_Referencia: string,
    Ocupacion: string,
    Telefono_Celular: string,
    Identificacion: string,
  ):
    any {
    const datosRef = {
      Tipo: Tipo,
      Nombre_Referencia: Nombre_Referencia,
      Ocupacion: Ocupacion,
      Telefono_Celular: Telefono_Celular,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/RegistroReferencias', datosRef)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  registroIdioma(
    Certificado: string,
    Identificacion: string,
    idioma: string,
    Nivel: string
  ): any {
    const datosIdioma = {
      Certificado: Certificado,
      Identificacion: Identificacion,
      idioma: idioma,
      Nivel: Nivel
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/RegistroIdiomas', datosIdioma)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  CambioVisiblilidad(id_user: string, Visible: string): any {
    const datosVisible = {
      id_user: id_user,
      Visible: Visible
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/CambioVisibilidad', datosVisible)
      .subscribe(respuesta => {
       console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  ActualizarCoordenadss(
    id_user: string,
    LatitudActual: string,
    LongitudActual: string
  ): any {
    const datosCoord = {
      id_user: id_user,
      LatitudActual: LatitudActual,
      LongitudActual: LongitudActual
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/ActualizarCoordenadas', datosCoord)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA = respuesta;
      });
  }

  RepartirTarjetas(
    Identificacion,
    seleccionados: string[]
  ): any {
    const datosCoord = {
      Identificacion: Identificacion,
      seleccionados: seleccionados
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
     return this.http
      .post(URL_waocardsService1 + '/RepartirTarjeta', datosCoord);
      
  }


  BuscarPersonas(
    latitud: string,
    longitud: string,
    Identificacion: string,
    radio: string
  ): any {
    const datosCoord = {
      latitud: latitud,
      longitud: longitud,
      Identificacion: Identificacion,
      Radio: radio
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    return this.http
      .post(URL_waocardsService1 + '/PersonasCercanas', datosCoord);
      // .subscribe(respuesta => {
      //   // console.log(respuesta);
      //   this.PersonasCercanas = respuesta;
      // });
  }

  UltimoID(
    Identificacion: string,
    tipoPauta: string
  ): any {
    const datosID = {
      Identificacion: Identificacion,
      tipoPauta: tipoPauta
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/ClasificadoID', datosID)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.IDRetornado = respuesta;
        console.log('ESTE ES EL ID QUERECUPERA DE LA BASE DE DATOS');
        console.log(this.IDRetornado);
      });
  }
  RegistrarClasificado(
    Identificacion: string,
    Titulo: string,
    Descripcion: string,
    Contacto: string,
    PalabrasClave: string,
    Tipo: string,
    Color: string,
    ID: string
  ): any {
    const datosClasf = {
      Identificacion: Identificacion,
      Titulo: Titulo,
      Descripcion: Descripcion,
      Contacto: Contacto,
      PalabrasClave: PalabrasClave,
      Tipo: Tipo,
      Color: Color,
      ID: ID
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS-
    this.http
      .post(URL_waocardsService1 + '/RegistrarClasificado', datosClasf)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.RTA_Clasificado = true;
      }, (error: any) => {
        // console.log(error);
        this.RTA_Clasificado = false;
      });
  }

  RegistrarFeriaEvento(
    Identificacion: string,
    Ciudad: string,
    Pais: string,
    Titulo: string,
    Direccion: string,
    fechaIni: string,
    fechaFin: string,
    precio: number,
    PalabrasClave: string,
    Tipo: string,
    Expositores,
    Color: string,
    ID: string
  ) {

    const datosFeriEvent = {
      Identificacion: Identificacion,
      Ciudad: Ciudad,
      Pais: Pais,
      Titulo: Titulo,
      Direccion: Direccion,
      fechaIni: fechaIni,
      fechaFin: fechaFin,
      precio: precio,
      PalabrasClave: PalabrasClave,
      Tipo: Tipo,
      Expositores: Expositores,
      Color: Color,
      ID: ID
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS-
    this.http
      .post(URL_waocardsService1 + '/RegistrarFeriaEvento', datosFeriEvent)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.RTA_Feria_evento = true;
      }, (error: any) => {
        console.log(error);
        this.RTA_Feria_evento = false;
      });

  }

  comprobarContrasena(
    Identificacion: string,
    Contrasena: string
  ) {

    const datosContrasena = {
      Identificacion: Identificacion,
      Contrasena: Contrasena
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS-
    return this.http
      .post(URL_waocardsService1 + '/ComprobarContrasena', datosContrasena);

  }

  cargarEmpresaUsuarios(
    Empresa: string
  ) {

    const datosProfesion = {
      Empresa: Empresa
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS-
    this.http
      .put(urls.URLphpCrud + 'usuarios/' + this.valor.identificacion, datosProfesion)
      .subscribe((respuesta: any) => {

        console.log(respuesta);
      }, (error: any) => {
        console.log(error);
      });

  }

  cambiarPDFSubido(
    PDFSubido: string
  ) {

    const datosBandera = {
      PDFSubido: PDFSubido
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS-
    this.http
      .put(urls.URLphpCrud + 'usuarios/' + this.valor.identificacion, datosBandera)
      .subscribe((respuesta: any) => {

        console.log(respuesta);
      }, (error: any) => {
        console.log(error);
      });

  }

  cambiarPDFFormacion(
    PDFSubido: string
  ) {

    const datosBandera = {
      PDFSubido: PDFSubido
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS-
    this.http
      .put(urls.URLphpCrud + 'formacion_academica/' + this.valor.identificacion, datosBandera)
      .subscribe((respuesta: any) => {

        console.log(respuesta);
      }, (error: any) => {
        console.log(error);
      });

  }

  cambiarNombre(
    Nombre: string,
    Identificacion: string
  ) {

    const datosNombre = {
      Nombre: Nombre,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS-
    this.http
      .put(urls.URLphpCrud + 'usuarios/' + this.valor.identificacion, datosNombre)
      .subscribe((respuesta: any) => {

        console.log(respuesta);
      }, (error: any) => {
        console.log(error);
      });

  }



  cargarPreciosTamanosPautas() {
    // if (this.data) {
    //  return of(this.data);
    // } else {
    return this.http
      .get(URL_waocardsService1 + '/FeriasEventosAleatorios/PreciosTamanosPautas');
    // .pipe(map(this.processData, this));
    // }
  }

  VerAmigos(
    Identificacion: string,
    lat: string,
    lon: string
  ): any {
    const datosID = {
      Identificacion: Identificacion,
      lat: lat,
      lon: lon
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/Amigos', datosID)
      .subscribe(respuesta => {

        this.Amigos = respuesta;

        this.CargaAmigosCompleta = true;
      }, (error: any) => {
        this.CargaAmigosCompleta = false;
      });
  }

  VerAmigosComun(
    Identificacion: string,
    ID_Publicador: string
  ): any {
    const datosID = {
      Identificacion: Identificacion,
      ID_Publicador: ID_Publicador
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/AmigosEnComun', datosID)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.AmigosEnComun = respuesta;
        this.CargaAmigosComunCompleta = true;
      }, (error: any) => {
        this.CargaAmigosComunCompleta = false;
      });
  }

  ComprobarSolicitud(
    Identificacion: string,
    Id_Contacto: string
  ): any {
    const datosID = {
      Identificacion: Identificacion,
      Id_Contacto: Id_Contacto
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/ComprobarSolicitud', datosID)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.SolicitudComprobada = respuesta;
      }, (error: any) => {
      });
  }

  Contactar(
    Identificacion: string,
    ID_Contacto: string
  ): any {
    const datosID = {
      Identificacion: Identificacion,
      ID_Contacto: ID_Contacto
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/Contactar', datosID)
      .subscribe(respuesta => {
        this.ContactoCorrecto = true;
        // console.log(respuesta);
      }, (error: any) => {
        this.ContactoCorrecto = false;
      });
  }

  RevisarSolicitudes(
    Identificacion: string,
  ): any {
    const datosID = {
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/RevisarSolicitudes', datosID)
      .subscribe(respuesta => {
        this.solicitudesPendientes = respuesta;
        // // console.log(respuesta);
      }, (error: any) => {
         // console.log(error);
      });
  }

  ConfirmarSolicitud(
    Identificacion: string,
    Id_Contacto: string
  ): any {
    const datosID = {
      Identificacion: Identificacion,
      Id_Contacto: Id_Contacto
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/ConfirmarSolicitud', datosID)
      .subscribe(respuesta => {
        // this.ContactoCorrecto = true;
        // console.log(respuesta);
        //// console.log(respuesta);
      }, (error: any) => {
        // this.ContactoCorrecto = false;
        // console.log(error);
      });
  }

  RechazarSolicitud(
    Identificacion: string,
    Id_Contacto: string
  ): any {
    const datosID = {
      Identificacion: Identificacion,
      Id_Contacto: Id_Contacto
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/RechazarSolicitud', datosID)
      .subscribe(respuesta => {
        // this.ContactoCorrecto = true;
        // console.log(respuesta);
        //// console.log(respuesta);
      }, (error: any) => {
        // this.ContactoCorrecto = false;
        // console.log(error);
      });
  }

  GuardarContrasena( Contrasena: string,  Identificacion: string
  ) {
    const datosContrasena = {
      Contrasena: Contrasena,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/Contrasena', datosContrasena)
      .subscribe(respuesta => {
        // console.log(respuesta);
        this.verificacionContrasena = true;
      }, (error: any) => {
        // console.log(error);
        this.verificacionContrasena = false;
      });
  }


  confirmarPIN(
    PINingresado: string,
    Identificacion: string
  ): any {
    const datosPIN = {
      PINingresado: PINingresado,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/ConfirmarPin', datosPIN)
      .subscribe(respuesta => {
         console.log(respuesta);
        this.PINLlegado = respuesta;
      }, (error: any) => {
         console.log(error);
       // this.verificacionContrasena = false;
      });
  }

  generarPIN(
    Correo: string,
    Identificacion: string
  ): any {
    const datosPIN = {
      Correo: Correo,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/GenerarPIN', datosPIN)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.PINGenerado = respuesta;
      }, (error: any) => {
        console.log(error);
        // this.verificacionContrasena = false;
      });
  }

  /// Bandera para saber que se realizó el cambio de la contraseña
  contrasenaCambiada(
    Valor: string,
    Identificacion: string
  ): any {
    const datosCNT = {
      Valor: Valor,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/ContrasenaCambiada', datosCNT)
      .subscribe(respuesta => {
        console.log(respuesta);
        // this.PINGenerado = respuesta;
      }, (error: any) => {
        console.log(error);
        // this.verificacionContrasena = false;
      });
  }


  enviarNumeros(
    Libreta: string [],
    Identificacion: string
  ): any {
    const datosLibreta = {
      Libreta: Libreta,
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/ImportarContactos', datosLibreta)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.ContactosenWAOCards = respuesta;
      }, (error: any) => {
        console.log(error);
      });
  }

  verPersonasAntesdeRepartir(
    Identificacion: string,
    pEncontradasIds: string[]
  ): any {
    const datosCoordenadas = {
      Identificacion: Identificacion,
      pEncontradasIds: pEncontradasIds
    };

    console.log('Datos que lleganId: ', datosCoordenadas.Identificacion);
    console.log('Datos que llegan: ', datosCoordenadas.pEncontradasIds);
    console.log('Datos que llegan2: ', pEncontradasIds);

    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    return this.http
      .post(URL_waocardsService1 + '/AntesdeRepartir', datosCoordenadas);
      /*
      .subscribe(respuesta => {
        console.log(respuesta);
        this.PersonasAntesdeRepartir = respuesta;
      }, (error: any) => {
        console.log(error);
      });*/
  }

  // guardarPreferenciasVisibilidadUsuario(seleccionFinal: string, Identificacion: string) {
  //   const datosPreferenciasUser = {
  //     preferenciasUser: seleccionFinal,
  //     Identificacion: Identificacion

  //   };
  //   // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
  //   return this.http
  //     .post(URL_waocardsService1 + '/GuardarPreferenciasVisibilidad', datosPreferenciasUser);


  // }

  guardarPreferenciasPreferenciasCampoAccion(seleccionFinal: string, Identificacion: string) {
    const datosPreferenciasUser = {
      preferenciasUser: seleccionFinal,
      Identificacion: Identificacion

    };
    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    return this.http
    .post(URL_waocardsService1 + '/GuardarPreferenciasCampoAccion', datosPreferenciasUser);
  }

// la siguiente funcion realiza dos consultas las preferencia: visivilidad y el campo accion
  ConsultarVisibilidadUsuario(Identificacion: string ) {
    const datos = {
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    return this.http
      .post(URL_waocardsService1 + '/ConsultarCamposAccionUsuario', datos);

  }

  obtenerCalificacionComentarios(Identificacion: string) {
    const datos = {
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    return this.http
      .post(URL_waocardsService1 + '/ObtenerCalificacionComentarios', datos);

  }

  guardarCalificacionComentarios(IdentificacionA: string, Id_CalificanteA: string, CalificacionA: string, ComentarioA: string) {
    const datos = {
      Identificacion: IdentificacionA,
      Id_Calificante: Id_CalificanteA,
      Calificacion: CalificacionA,
      Comentario: ComentarioA
    };

    // ESTE POST HACE EL REGISTRO EN LA BASE DE DATOS
    return this.http
      .post(URL_waocardsService1 + '/CalificarComentar', datos);

  }

  consultarCamposAccionYCodigosUsuario(Identificacion: string ) {
    const datos = {
      Identificacion: Identificacion
    };

    return this.http
     .post(URL_waocardsService1 + '/ConsultarCamposAccionUsuario', datos);
    // prueba para hacer ejercio nuevo
      // .post(URL_waocardsService1 + '/ConsultarVisibilidadUsuario', datos); 

  }

  envioCorreoSugerenciaCampoAccion(asunto, areaTexto) {
    const datos = {
      asunto: asunto,
      areaTexto: areaTexto
    };

    return this.http
     .post(URL_waocardsService1 + '/Emails', datos);
  }
  revisarNuevasTarjetasRepartidas(
    Identificacion: string,
  ): any {
    const datosID = {
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/RevisarNuevasTarjetasRepartidas', datosID)
      .subscribe(respuesta => {
        this.NuevasTarjetasRepartidas = respuesta;
       // console.log(respuesta);

      }, (error: any) => {
        // console.log(error);
      });
    }

  verMisTarjetasRecibidas(
    Identificacion: string,
  ): any {
    const datosID = {
      Identificacion: Identificacion
    };

    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/MisTarjetasRecibidas', datosID)
      .subscribe(respuesta => {
        this.MisTarjetasRecibidas = respuesta;
        // console.log(respuesta);

      }, (error: any) => {
        // console.log(error);
      });
  }

    Registro_referidos(
      Identificacion: string,
      referidos?: string
    ): any {
      const datos = {
        Identificacion: Identificacion,
        referidos: referidos
      };
      // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
      this.http
        .post(URL_waocardsService1 + '/Referidos', datos)
        .subscribe(respuesta => {
          console.log(respuesta);
          this.referidos = respuesta;
        });
    }

  aceptarRechazarRepartidos(
    Identificacion: string,
    Id_Repartidor: string,
    Confirmacion: string
  ): any {
    const datos = {
      Identificacion: Identificacion,
      Id_Repartidor: Id_Repartidor,
      Confirmacion: Confirmacion
    };
    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    this.http
      .post(URL_waocardsService1 + '/AceptarRechazarRepartidos', datos)
      .subscribe(respuesta => {
        console.log(respuesta);
        this.referidos = respuesta;
      });
  }

  listarCoincidencias(
    Identificacion: string,
    Cadena: string,
    ParametroBusqueda: string
  ): any {
    const datos = {
      Identificacion: Identificacion,
      Cadena: Cadena,
      ParametroBusqueda: ParametroBusqueda
    };
    // ESTE POST HACE EL PREREGISTRO EN LA BASE DE DATOS
    return this.http
      .post(URL_waocardsService1 + '/Autocompletar', datos);
  }
}
