import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserData } from './user-data';
import * as urls from '../providers/urls.servicios';


@Injectable({
  providedIn: 'root'
})
export class usuarios {
  data: any;
  dto: any;
 public cv: any =[{
    DatosPersonales: '',
    Educacion: [{

    }],
    Experiencia: [{

      }],
    Habilidades: [{

    }],
    Idiomas: [{

    }],
  Referencias: [{

    }]
  }];
  incompleto: boolean;

  constructor(public http: HttpClient,
              public user: UserData,
               ) {}

  // CONSULTA TODOS
  load(): any {
    if (this.data) {
      return of(this.data);
    } else {
    return this.http
        .get(urls.URLphpCrud + 'usuarios');
       // .pipe(map(this.processData, this));
    }
  }





  getSpeakers() {
    return this.load().pipe(
      map((data: any) => {
        return data.records.sort((a: any, b: any) => {
          const aName = a.Nombre.split(' ').pop();
          const bName = b.Nombre.split(' ').pop();
          return aName.localeCompare(bName);
        });
      })
    );
  }

  //CONSULTA CV DE USUARIO DEL TARJETERO
  datosCVUsuario(IdentificacionUsuario) {
    console.log('El ID de la persona es: ', IdentificacionUsuario);
    const datoID = { Identificacion: IdentificacionUsuario };
    // ESTE POST HACE REALIZA LA BUSQUEDA EN LA BASE DE DATOS
    return this.http
      .post(urls.URL_waocardsService1 + '/CVUsuario', datoID);
     
  }

  // CONSULTA UNICA PARA ENCONTRADOS
  datosUsuario(idUsr) {
    let datoID = {id_user: idUsr};
    // ESTE POST HACE REALIZA LA BUSQUEDA EN LA BASE DE DATOS
    return this.http
    .post(urls.URL_waocardsService1 + "/InfoUsuario", datoID);
  }




}
