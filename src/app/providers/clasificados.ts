import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserData } from './user-data';
import * as urls from '../providers/urls.servicios';

@Injectable({
  providedIn: 'root'
})
export class Clasificados {
  data: any;

  constructor(public http: HttpClient,
              public user: UserData) {}

  load(): any {
    if (this.data) {
      return of(this.data);
    } else {
      return this.http
        .get(urls.URL_waocardsService1 + '/ClasificadosAleatorios/CAleatorios');
       // .pipe(map(this.processData, this));
    }
  }

}
