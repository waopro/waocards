
import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LoadingController } from '@ionic/angular';
import { RegistroUsuariosService } from './registro-usuarios.service';
import { Valores } from './valores';

@Injectable({
  providedIn: 'root'
})
export class Ubicacion {
  data: any;
  activarGPS: boolean = false;
  public longitud: any;
  public latitud: any;
  precision: any;
  watch: any;

  constructor(public geoubicacion: Geolocation,
              public loadingController: LoadingController,
              public registro: RegistroUsuariosService,
              public valor: Valores ) {
    console.log('El servicio de ubicación está activado');
  }

  iniciarGeubicacion() {
    const options = {
      timeout: 99000,
      enableHighAccuracy: true,
      maximumAge: 0
    };
    this.watch = this.geoubicacion.getCurrentPosition(options).then((resp) => {
      this.longitud = resp.coords.longitude;
      this.latitud = resp.coords.latitude;
      console.log(this.latitud, this.longitud);
      this.precision = resp.coords.accuracy;
      setTimeout(() => {
        this.registro.ActualizarCoordenadss(
          this.valor.identificacion,
          this.latitud,
          this.longitud
        );
        this.activarGPS = true;
      }, 1000);
    }).catch((error) => {
      this.activarGPS = false;
    });
  }
  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: 'dots',
      message: 'Obteniendo coordenadas',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }
  ubicacionInstantanea() {
    const options = {
      timeout: 99000,
      enableHighAccuracy: true
    };
    this.watch = this.geoubicacion.watchPosition(options);
    this.watch.subscribe((data) => {
      this.longitud = data.coords.longitude;
      this.latitud = data.coords.latitude;
      this.activarGPS = true;
      console.log('Ubicación al instante recibida');
    },
    (err) => {
      this.activarGPS = false;
      console.log('Error getting location', err);
    });
  }

  cancelarUbicacion() {
    try {
      this.watch.unsubscribe();
    } catch (e) {
      console.log(JSON.stringify(e));
    }
  }

}
