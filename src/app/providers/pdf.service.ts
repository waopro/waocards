import { Injectable } from '@angular/core';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { Downloader, DownloadRequest, NotificationVisibility } from '@ionic-native/downloader/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { RegistroUsuariosService } from '../providers/registro-usuarios.service';
import { Valores } from './valores';
import { AlertController, LoadingController } from '@ionic/angular';
import * as urls from '../providers/urls.servicios';
import { UserData } from './user-data';

@Injectable({
  providedIn: 'root'
})
export class PDFService {

  public ResultadoSubida: any = [{
    response: ''
  }];
  PDF: any;
  loading: any;
  RutaPDFSeleccionada: boolean;
  DescargandoPDFBandera = false;
  indiceclicado: any;


  constructor(
    public registro: RegistroUsuariosService,
    private transfer: FileTransfer,
    private fileChooser: FileChooser,
    public valor: Valores,
    private alertController: AlertController,
    private loadingController: LoadingController,
    public userData: UserData,
    private downloader: Downloader,
    private fileOpener: FileOpener
  ) { }


  seleccionarArchivo() {
    this.fileChooser.open()
      .then(uri => {
        this.PDF = uri;
        setTimeout(() => {
          this.confirmar();
        }, 2000);
      })
      .catch(e => console.log(e));
  }

  seleccionarArchivoWaoChat() {
   return this.fileChooser.open();
  }

  async confirmarWaoChat(filename: any) {
    const alert = await this.alertController.create({
      header: 'Confirmar',
      message: '¿Desea enviar el documento a su contacto?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            // open(urls.URL_waocardsService + this.userData.datosactualizacion.url, '_system');
            this.mostrarSubiendo();
            // console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }


  async confirmar() {
    const alert = await this.alertController.create({
      header: 'Confirmar',
      message: 'Se subirá el archivo PDF seleccionado al servidor',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            // open(urls.URL_waocardsService + this.userData.datosactualizacion.url, '_system');
            this.mostrarSubiendo();
            this.SubirCV(this.PDF);
            // console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  async mostrarSubiendo() {
    this.loading = await this.loadingController.create({
      message: 'Subiendo..'
    });
    await this.loading.present();

  }

  SubirPDFWaoChat(PDF: any, filename: any) {
    // create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();
    console.log('direccion: ' + urls.URL_waocardsService1 + '/UploadPDFWaoChat');
    console.log('archivo que llega a ChatPDF:', filename);

    // option transfer
    let options: FileUploadOptions = {
      fileKey: 'documento',
      fileName: filename,
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: 'application/pdf',
      headers: {}
    };

    // file transfer action
   return  fileTransfer
     .upload(PDF, urls.URL_waocardsService1 + '/UploadPDFWaoChat', options);
  }

  SubirCV(PDF: any) {
    // create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();
    console.log('direccion: ' + urls.URL_waocardsService1 + '/uploadPDF');

    // option transfer
    let options: FileUploadOptions = {
      fileKey: 'documento',
      fileName: this.valor.identificacion + '.pdf',
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: 'application/pdf',
      headers: {}
    };

    // file transfer action
    fileTransfer
      .upload(PDF, urls.URL_waocardsService1 + '/uploadPDF', options);
  }

  async presentarResultadoPDF(mensaje: string) {
    // console.log('se dispara correcto');
    const alert = await this.alertController.create({
      header: mensaje,
      buttons: ['Continuar']
    });
    alert.present();
  }

  descargarPDF(ruta: any, indice) {
    console.log('evento clic', indice);
    this.indiceclicado = indice;
    this.DescargandoPDFBandera = true;
    const request: DownloadRequest = {
      uri: ruta,
      title: 'Documento Recibido.pdf',
      description: 'Documento recibido',
      mimeType: 'application/pdf',
      visibleInDownloadsUi: true,
      notificationVisibility: NotificationVisibility.VisibleNotifyCompleted,
      destinationInExternalFilesDir: {
        dirType: 'Downloads',
        subPath: 'Documento Recibido.pdf'
      }
    };


    this.downloader.download(request)
      .then((locat: string) => {
        this.DescargandoPDFBandera = false;
        console.log('ubicacion: ' + locat);
        this.abrirPDF(locat);
      })
      .catch((error: any) => console.log(error));
  }

  abrirPDF(rutadesc: any) {
    console.log('llego a abrir archivo: ', rutadesc);
    console.log('Se abrira: ', rutadesc);
    this.fileOpener.open(rutadesc, 'application/pdf')
      .then(() => console.log('File is opened'))
      .catch(e => console.log('Error opening file', e));
  }
}
