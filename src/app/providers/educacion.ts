import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserData } from './user-data';
import * as urls from '../providers/urls.servicios';

@Injectable({
  providedIn: 'root'
})
export class Educacion {
  data: any;

  constructor(public http: HttpClient,
              public user: UserData) {}

  load(): any {
    if (this.data) {
      return of(this.data);
    } else {
      return this.http
        .get(urls.URLphpCrud + 'formacion_academica');
       // .pipe(map(this.processData, this));
    }
  }



  getFormacion() {
    return this.load().pipe(
      map((data: any) => {
        return data.records.sort((a: any, b: any) => {
          const aName = a.Identificacion.split(' ').pop();
          const bName = b.Identificacion.split(' ').pop();
          return aName.localeCompare(bName);
        });
      })
    );
  }





}
