import { TestBed } from '@angular/core/testing';

import { BusquedaManualService } from './busqueda-manual.service';

describe('BusquedaManualService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BusquedaManualService = TestBed.get(BusquedaManualService);
    expect(service).toBeTruthy();
  });
});
