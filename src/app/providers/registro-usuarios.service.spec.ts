import { TestBed } from '@angular/core/testing';

import { RegistroUsuariosService } from './registro-usuarios.service';

describe('RegistroUsuariosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegistroUsuariosService = TestBed.get(RegistroUsuariosService);
    expect(service).toBeTruthy();
  });
});
