import { Contacts, Contact, ContactField, ContactName  } from '@ionic-native/contacts/ngx';
import { LoadingController, Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class Contactos  {
  public allContacts: any;
  public Libreta: any;


  constructor(private contacts: Contacts,
              private platform: Platform,
             public loadingController: LoadingController) {

  }


  listarContactos() {
    this.presentLoading();
      setTimeout(() => {
        this.contacts.find(['displayName', 'phoneNumbers', 'emails']).then(data => {
          this.allContacts = data;
          console.log(this.allContacts);
          console.log(JSON.stringify(this.allContacts));
          this.Libreta = this.allContacts.phoneNumbers;
        });
      }, 500);
  }

   async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Cargando la libreta...',
      duration: 5500
    });
    await loading.present();

  }
 

}