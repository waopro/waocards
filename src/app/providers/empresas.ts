import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserData } from './user-data';
import * as urls from '../providers/urls.servicios';

@Injectable({
  providedIn: 'root'
})
export class Empresas {
  data: any;

  constructor(public http: HttpClient,
              public user: UserData) {}

  cargarEmpresas() {
    console.log(this.http.get(urls.URLphpCrud + 'empresas'));
      return this.http.get(urls.URLphpCrud + 'empresas');
  }

}
