import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Events, MenuController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { UserData } from './providers/user-data';
import { Valores } from './providers/valores';
import * as urls from './providers/urls.servicios';
import * as ver from './providers/data.version';
import { RegistroUsuariosService } from './providers/registro-usuarios.service';
import { FindPeoplePage } from './pages/find-people/find-people.page';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { Actualizaciones } from './providers/actualizaciones';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { Vibration } from '@ionic-native/vibration/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { TabsPage } from './pages/tabs-page/tabs-page';
import { DetallePersonaEncontradaPage } from '../app/pages/detalle-persona-encontrada/detalle-persona-encontrada.page';
import { ModalController } from '@ionic/angular';
import { Downloader, DownloadRequest, NotificationVisibility } from '@ionic-native/downloader/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { TitleCasePipe } from '@angular/common';
import { Paises } from './providers/paises';
import { Device } from '@ionic-native/device/ngx';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { WAOChatService } from './providers/waochat.service';
import { MensajeWAOChat } from './interfaces/mensaje-waochat';
import { ChatPage } from './pages/chat/chat.page';
import { Ubicacion } from './providers/ubicacion';
//import { IonInfiniteScroll } from '@ionic/angular';


//import { Environment } from '@ionic-native/google-maps/ngx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  appPages = [
    {
      title: 'Inicio',
      url: '/app/tabs/schedule',
      icon: 'assets/icons/home-outline.svg'
    },
    {
      title: 'Tarjetero',
      url: '/app/tabs/speakers',
      icon: 'assets/icons/business-card.svg'
    },
    {
      title: 'Encontrar',
      url: '/app/tabs/find-people',
      icon: 'assets/icons/rocket.svg'
    },
    {
      title: 'Perfil',
      url: '/app/tabs/account',
      icon: 'assets/icons/user-outline.svg'
    }
  ];
  loggedIn = false;
  usuario: any[] = [];
  URLavatar = urls.URLimagen;
  usr: any[] = [];
  visible: boolean;
  EstaDesactualizado = false;
  NuevaVersion: any;
  idioma: string;
  rutaDescarga: any;
  // labels y elementos que cambiaran de idioma
  InicioLink: string;
  TarjeteroLink: string;
  EncontrarLink: string;
  PerfilLink: string;
  NuevaVersionDisp: string;
  Actual: string;
  Nueva: string;
  ActualizarBTN: string;
  CancelarBTN: string;
  IniciandoSesion: string;
  NoHayConexion: string;
  ListoBTN: string;
  SesionNoI: string;
  BienvenidoDeNuevo: string;
  NotificacionIDRecibida: any;
  NotificacionIDRecibidaTR: any;
  cantidad: any; // cantidad de notificaciones recibidas
  cantidadTR: any; // cantidad de notificaciones recibidas
  intervalo: any;
  toaster: any;
  ///IDnotif: 

  suscritoChat: boolean = false;

  public WAOChats: Observable<any[]>;

  constructor(
    private events: Events,
    private menu: MenuController,
    private platform: Platform,
    private router: Router,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    public paises: Paises,
    public userData: UserData,
    private valor: Valores,
    private registro: RegistroUsuariosService,
    public mapPage: FindPeoplePage,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public alertController: AlertController,
    public actualizaciones: Actualizaciones,
    private _translate: TranslateService,
    private globalization: Globalization,
    public uniqueDeviceID: UniqueDeviceID,
    public device: Device,
    public vibration: Vibration,
    public localNotifications: LocalNotifications,
    public tabs: TabsPage,
    public modalController: ModalController,
    private downloader: Downloader,
    private fileOpener: FileOpener,
    private titlecasePipe: TitleCasePipe,
    public waochat: WAOChatService,
    public chat: ChatPage,
    public ubicacion: Ubicacion  ) {
    this.initializeApp();
    this.verificarIdioma();
    this.cargarIdiomaElementos();
    this.userData.getUserID();

    setTimeout(() => {
      let userLang = navigator.language.split('-')[0];
      userLang = /(en|de|it|fr|es|be)/gi.test(userLang) ? userLang : this.idioma;
      this._translate.setDefaultLang(this.idioma);
      this._translate.use(userLang);
      this.obtenerID();
    }, 300);
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
     /*  Environment.setEnv({
        // Api key for your server
        // (Make sure the api key should have Website restrictions for your website domain only)
        'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyB8pf6ZdFQj5qw7rc_HSGrhUwQKfIe9ICw',

        // Api key for local development
        // (Make sure the api key should have Website restrictions for 'http://localhost' only)
        'API_KEY_FOR_BROWSER_DEBUG': '(YOUR_API_KEY_IS_HERE)'
      }); */
      this.statusBar.overlaysWebView(false);
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('#f4f5f9'); // define el color de la barra de notificaciones
    });
  this.paises.cargarPaises();
  }

  verificarNotificaciones() {
   // console.log(this.registro.solicitudesPendientes.cantidad);
    if (this.registro.solicitudesPendientes.solicitudes !== undefined && this.registro.solicitudesPendientes.cantidad >= '1') {
      this.tabs.NotifPerfil = true;

      if (this.cantidad != this.registro.solicitudesPendientes.cantidad) {
        let indice = this.registro.solicitudesPendientes.cantidad - 1;
        // console.log('tiene nueva solicitud de:');
        // console.log(this.registro.solicitudesPendientes.solicitudes[indice].Identificacion);
        // console.log('Un ' + this.registro.solicitudesPendientes.solicitudes[indice].Cargo_Actual +' desea contactarse con usted');
        this.NotificacionIDRecibida = this.registro.solicitudesPendientes.solicitudes[indice].Identificacion;
        this.vibration.vibrate([300, 100, 300]);
        this.localNotifications.schedule({
          id: 1,
          title: 'Nueva solicitud de contacto',
          text: 'Un ' + this.registro.solicitudesPendientes.solicitudes[indice].Cargo_Actual +
                 ' desea contactarse con usted',
          sound: this.setSound(),
          priority: 1,
          icon: urls.URLimagen + this.registro.solicitudesPendientes.solicitudes[indice].foto_perfil,
          color: '6600CC',
          led: '6600CC',
          actions: [
            { id: 'yes', title: 'Ver contacto'},
            { id: 'no', title: 'Cancelar' }
          ],
          data: { secret: 2 }
        });


      this.notificacion(this.registro.solicitudesPendientes.solicitudes[indice].Cargo_Actual,
                        this.registro.solicitudesPendientes.solicitudes[indice].foto_perfil,
                        urls.URLimagen,
                        this.registro.solicitudesPendientes.solicitudes[indice].Identificacion,
                        'Nueva solicitud', 'Ver contacto');
      this.cantidad = this.registro.solicitudesPendientes.cantidad;
      // console.log('La cantidad es: ' + this.cantidad);
      }

    }

  /// notificacion de chatnuevo


    /// VERIFICA SI HAY CONTACTOS NUEVOS Y NO SE HAN NOTIFICADO AUN EN LA APP
      if (this.registro.solicitudesPendientes.solicitudes !== undefined && this.registro.solicitudesPendientes.cantidad == '1') {
        this.tabs.NotifPerfil = true;
        if (this.NotificacionIDRecibida != this.registro.solicitudesPendientes.solicitudes["0"].Identificacion){
          // console.log('tiene nueva solicitud de:');
          // console.log(this.registro.solicitudesPendientes.solicitudes["0"].Identificacion);
          // console.log('Un ' + this.registro.solicitudesPendientes.solicitudes["0"].Profesion + ' desea contactarse con usted');
          this.NotificacionIDRecibida = this.registro.solicitudesPendientes.solicitudes["0"].Identificacion;
          this.vibration.vibrate([300, 100, 300]);
          this.localNotifications.schedule({
            id: 1,
            title: 'Nueva solicitud de contacto',
            text: 'Un ' + this.registro.solicitudesPendientes.solicitudes["0"].Profesion + ' desea contactarse con usted',
            sound: this.setSound(),
            priority: 1,
            icon: urls.URLimagen + this.registro.solicitudesPendientes.solicitudes["0"].foto_perfil,
            color: '6600CC',
            led: '6600CC',
            actions: [
              { id: 'yes', title: 'Ver contacto'},
              { id: 'no', title: 'Cancelar' }
            ],
            data: { secret: 2 }
          });
          this.localNotifications.on('click').subscribe((data: any) => {
            if (data.actions.id == 'yes') {
              alert('clicked: ' + data.actions.id);
            }
          });

        }

      }

      if (this.registro.solicitudesPendientes.cantidad === undefined) {
        // console.log('No tiene solicictudes');
    }
  }

  verificarNotificacionesTR() {
   // console.log(this.registro.NuevasTarjetasRepartidas.cantidad);

    if (this.registro.NuevasTarjetasRepartidas.nuevasTarjetas !== undefined && this.registro.NuevasTarjetasRepartidas.cantidad >= '1') {
      this.tabs.NotifTarjetero = true;

      if (this.cantidadTR != this.registro.NuevasTarjetasRepartidas.cantidad) {
        this.cantidadTR = this.registro.NuevasTarjetasRepartidas.cantidad;
        let indice = this.registro.NuevasTarjetasRepartidas.cantidad - 1;
       //  console.log('tiene nueva solicitud:');
        // console.log(this.registro.solicitudesPendientes.solicitudes[indice].Identificacion);
        // console.log('Un ' + this.registro.solicitudesPendientes.solicitudes[indice].Cargo_Actual +' desea contactarse con usted');
        this.NotificacionIDRecibidaTR = this.registro.NuevasTarjetasRepartidas.nuevasTarjetas[indice].Identificacion;
        this.vibration.vibrate([200, 100, 200]);
        this.localNotifications.schedule({
          id: 1,
          title: 'Nueva solicitud de contacto',
          text: this.registro.NuevasTarjetasRepartidas.nuevasTarjetas[indice].Nombre +
            ' ha compartido su tarjeta a usted',
          sound: this.setSound(),
          priority: 1,
          icon: urls.URLimagen + this.registro.NuevasTarjetasRepartidas.nuevasTarjetas[indice].foto_perfil,
          color: '6600CC',
          led: '6600CC',
          actions: [
            { id: 'yes', title: 'Ver tarjeta'},
            { id: 'no', title: 'Cancelar' }
          ],
          data: { secret: 2 }
        });


        this.notificacion(this.registro.NuevasTarjetasRepartidas.nuevasTarjetas[indice].Nombre,
          this.registro.NuevasTarjetasRepartidas.nuevasTarjetas[indice].foto_perfil,
          urls.URLimagen,
          this.registro.NuevasTarjetasRepartidas.nuevasTarjetas[indice].Identificacion,
          'Nueva tarjeta recibida', 'Ver');
        this.cantidad = this.registro.NuevasTarjetasRepartidas.nuevasTarjetas.cantidad;
        // console.log('La cantidad es: ' + this.cantidad);
      }

    }

    /// notificacion de chatnuevo


    /// VERIFICA SI HAY CONTACTOS NUEVOS Y NO SE HAN NOTIFICADO AUN EN LA APP
    if (this.registro.NuevasTarjetasRepartidas.nuevasTarjetas !== undefined && this.registro.NuevasTarjetasRepartidas.cantidad == '1') {
      this.tabs.NotifTarjetero = true;
      if (this.NotificacionIDRecibidaTR != this.registro.NuevasTarjetasRepartidas.nuevasTarjetas["0"].Identificacion) {
        // console.log('tiene nueva solicitud de:');
        // console.log(this.registro.solicitudesPendientes.solicitudes["0"].Identificacion);
        // console.log('Un ' + this.registro.solicitudesPendientes.solicitudes["0"].Profesion + ' desea contactarse con usted');
        this.NotificacionIDRecibidaTR = this.registro.NuevasTarjetasRepartidas.nuevasTarjetas["0"].Identificacion;
        this.vibration.vibrate([300, 100, 300]);
        this.localNotifications.schedule({
          id: 1,
          title: 'Nueva solicitud de contacto',
          text: this.registro.NuevasTarjetasRepartidas.nuevasTarjetas["0"].Nombre + ' ha compartido su tarjeta a usted',
          sound: this.setSound(),
          priority: 1,
          icon: urls.URLimagen + this.registro.NuevasTarjetasRepartidas.nuevasTarjetas["0"].foto_perfil,
          color: '6600CC',
          led: '6600CC',
          actions: [
            { id: 'yes', title: 'Ver tarjeta' },
            { id: 'no', title: 'Cancelar' }
          ],
          data: { secret: 2 }
        });
        this.localNotifications.on('click').subscribe((data: any) => {
          if (data.actions.id == 'yes') {
            alert('clicked: ' + data.actions.id);
          }
        });

      }

    }

    if (this.registro.NuevasTarjetasRepartidas.cantidad === undefined) {
    //  console.log('No tiene solicictudes');
    }
  }

  cargarAmigos() {

    // this.completo = 'false';
    // console.log("los speakers antes: " + this.completo);
    // console.log(this.speakers);
    this.URLavatar = urls.URLimagen;
    this.registro.VerAmigos(this.valor.identificacion, this.ubicacion.latitud, this.ubicacion.longitud);
    this.userData.getContador();
    this.userData.getTiempo();
   setTimeout(() => {
     console.log(this.userData.Contador);
     console.log(this.userData.Tiempo);
     if (this.userData.Contador === null) {
       this.userData.Contador = [];
      for(var i = 0; i < this.registro.Amigos.amigos.length; i++) {
        this.userData.Contador.push(0);
      }
      console.log('Como el contador esta vacío aún: ', this.userData.Contador);
      this.userData.setContador(this.userData.Contador);
     }
     if (this.userData.Tiempo === null) {
       this.userData.Tiempo = [];
       for (var jj = 0; jj < this.registro.Amigos.amigos.length; jj++) {
         this.userData.Tiempo.push(0);
       }
       console.log('Como el tiempo esta vacío aún: ', this.userData.Tiempo);
       this.userData.setTiempo(this.userData.Tiempo);
     }
   }, 200);
  }


  async openChat(speaker: string, ID: string, photo: string) {
    console.log('VAMOS A CREAR LA VENTAN PARA EL CHAT CON ' + speaker);
    const modal = await this.modalController.create({
      component: ChatPage,
      componentProps: {
        speakerName: speaker,
        speakerID: ID,
        speakerPhoto: photo
      }
    });

    modal.onDidDismiss()
      .then((data) => {
        console.log('RETORNANDO FALSE?', data );
        this.suscritoChat = data.data;
      });

    await modal.present();

  }


  mensajeNuevoChat(cont: number, nombre: string, mensaje: string, usrfoto: string) {
    //this.IDnotif = usrfoto.split('.jpg').join('');
    if (mensaje !== '') {
      console.log('El mensaje no es nulo, se muestra la notificacion');
      this.vibration.vibrate([300, 100, 300]);
      this.localNotifications.schedule({
        id: cont,
        title: nombre,
        text: mensaje,
        sound: this.setSound(),
        priority: 1,
        icon: urls.URLimagen + usrfoto,
        color: '6600CC',
        led: '6600CC',
        actions: [
          { id: 'ver', title: 'Ver conversación' }
        ],
        data: { secret: 2, speaker: usrfoto.split('.jpg').join('') }
      });
      this.localNotifications.on('click').subscribe((data: any) => {
        console.log('data');
        console.log(data);
        // console.log(suscripcion);
        console.log('suscritochat es:', this.suscritoChat);
        if (this.suscritoChat === false) {
          this.openChat(nombre, usrfoto.split('.jpg').join(''), usrfoto);
          this.suscritoChat = true;
          console.log('se corrio por aqui');
        }
        if (this.suscritoChat === true) {
          console.log('Ya Está Suscrito!');
        }
      });

    }
  }

 verificarChats() {
   // console.log('MENSAJE DE :', this.registro.Amigos.amigos[9].Identificacion + this.registro.Amigos.amigos[9].Nombre);
   let cont = 0;
    for (let j = 0; j < this.registro.Amigos.amigos.length; j++) {

      console.log('Posicion antes del subscribe:', j);
    /*   for(var l = 0; l < 1000; l++) {
        console.log('Posicion:', j);
      }  */
      
      this.waochat.cargarmensajes2(this.registro.Amigos.amigos[j].Identificacion, this.valor.identificacion, 1)
        .subscribe((data: MensajeWAOChat[]) => {
          if (data[0] !== undefined) {
          if (this.userData.Tiempo[j] !== data[0].fecha) {
              cont++;
              console.log('Se notificara, nuevo mensaje!!!');
              this.userData.Tiempo[j] = data[0].fecha;
              setTimeout(() => {
                this.userData.setTiempo(this.userData.Tiempo);
                console.log('array Tiempo:', this.userData.Tiempo, 'posicion', j);
                this.mensajeNuevoChat(cont, data[0].nombre, data[0].mensaje, data[0].UsrFoto);
              }, 50);
            }
          }

         // this.userData.Contador[j]++;
        });

  }
  }

  async notificacion(cargo_actual, foto_perfil, URLAvatar, Identificacion, header, boton) {
    const alert = await this.alertController.create({
      header: header,
      message: '<ion-item lines="none" text-wrap> <img src="'
                + URLAvatar + foto_perfil +
               '" class="fotoEncontra"><ion-label><p class="parrafoEnc"><ion-text>' +
               cargo_actual +
               '</ion-text></p></ion-label></ion-item>',
      buttons: [
        {
          text: '',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // console.log('Confirm Cancel: blah');
          }
        }, {
          text: boton,
          handler: () => {
            // console.log('Confirm Okay');
            if (header === 'Nueva solicitud') {
              this.verPerfil(Identificacion);
            }
          }
        }
      ]
    });

    await alert.present();
  }

  async verPerfil(Identificacion) {
    // console.log('Entra al perfil async de: ' + Identificacion);
    const modal = await this.modalController.create({
      component: DetallePersonaEncontradaPage,
      componentProps: {
        IDEncontrado: Identificacion,
        solicitante: 'true'
      }
    });
    modal.showBackdrop = true;
    modal.backdropDismiss = true;
    return await modal.present();
  }

  setSound() {
    if (this.platform.is('android')) {
      return 'file://assets/sounds/notifsound.mp3';
    } else {
      return 'file://assets/sounds/notifsound.mp3';
    }
  }

  obtenerID() {
    this.uniqueDeviceID.get()
      .then((uuid: any) => this.valor.UUID = uuid)
      .catch((error: any) => { if (this.platform.is('cordova')) {
                              this.obtenerID();
                               }});
    this.valor.Plataforma = this.device.platform;
    this.valor.VersionPlataforma = this.device.version;
  }

  verificarIdioma() {
    this.globalization.getPreferredLanguage()
      .then(res => this.idioma = res.value);

      /*  console.log('idioma: ' + this.idioma);
      if (this.idioma !== 'es-es') {
       this.idioma = 'en';
      } */
  }

  cargarIdiomaElementos() {
    this._translate.stream('PAGES.APPCOMPONENTE.NUEVA_VERSION_D').subscribe((text: string) => {
      this.NuevaVersionDisp = text;
    });
    this._translate.stream('PAGES.APPCOMPONENTE.ACTUAL').subscribe((text: string) => {
      this.Actual = text;
    });
    this._translate.stream('PAGES.APPCOMPONENTE.NUEVA').subscribe((text: string) => {
      this.Nueva = text;
    });
    this._translate.stream('PAGES.APPCOMPONENTE.ACTUALIZAR_BTN').subscribe((text: string) => {
      this.ActualizarBTN = text;
    });
    this._translate.stream('PAGES.APPCOMPONENTE.CANCELAR_BTN').subscribe((text: string) => {
      this.CancelarBTN = text;
    });
    this._translate.stream('PAGES.APPCOMPONENTE.INICIANDO_SESION').subscribe((text: string) => {
      this.IniciandoSesion = text;
    });
    this._translate.stream('PAGES.APPCOMPONENTE.NO_HAY_CONEXION').subscribe((text: string) => {
      this.NoHayConexion = text;
    });
    this._translate.stream('PAGES.APPCOMPONENTE.LISTO_BTN').subscribe((text: string) => {
      this.ListoBTN = text;
    });
    this._translate.stream('PAGES.APPCOMPONENTE.SESION_NO_I').subscribe((text: string) => {
      this.SesionNoI = text;
    });
    this._translate.stream('PAGES.APPCOMPONENTE.BIENVENIDO_DE_NUEVO').subscribe((text: string) => {
      this.BienvenidoDeNuevo = text;
    });
    this._translate.stream('PAGES.TABS.INICIO').subscribe((text: string) => {
      this.InicioLink = text;
    });
    this._translate.stream('PAGES.TABS.TARJETERO').subscribe((text: string) => {
      this.TarjeteroLink = text;
    });
    this._translate.stream('PAGES.TABS.ENCONTRAR').subscribe((text: string) => {
      this.EncontrarLink = text;
    });
    this._translate.stream('PAGES.TABS.PERFIL').subscribe((text: string) => {
      this.PerfilLink = text;
    });
    setTimeout(() => {
      this.appPages = [
        {
          title: this.InicioLink,
          url: '/app/tabs/schedule',
          icon: 'assets/icons/home-outline.svg'
        },
        {
          title: this.TarjeteroLink,
          url: '/app/tabs/speakers',
          icon: 'assets/icons/business-card.svg'
        },
        {
          title: this.EncontrarLink,
          url: '/app/tabs/find-people',
          icon: 'assets/icons/rocket.svg'
        },
        {
          title: this.PerfilLink,
          url: '/app/tabs/account',
          icon: 'assets/icons/user-outline.svg'
        }
      ];
    }, 800);
  }

  ngOnInit() {
    this.userData.InicioSesion = false;
    this.userData.inicio();
    setTimeout(() => {
      this.checkLoginStatus();
    }, 3100);
    setTimeout(() => {
      this.comprobarActualizaciones();
    }, 9000);
    setTimeout(() => {
      this.verificarContrasenaCambiada();
    }, 11600);
    this.userData.cargarDatosActualizacion();
    this.URLavatar = urls.URLimagen; // URL de acceso a las imagenes
    this.evaluarvisibilidad();
     }

     verificarContrasenaCambiada() {
       if (this.userData.datos.ContrasenaCambiada == '1') {
         this.logout();
         this.presentarCambioContrasena();
       }
       if (this.userData.datos.ContrasenaCambiada == '0'){
        console.log('No se ha cambiado la contraseña');
       }
     }

  async presentarCambioContrasena() {
    console.log('se dispara correcto');
    const alert = await this.alertController.create({
      header: '¡Contraseña cambiada últimamente!',
      message: 'Se ha cambiado la contraseña. Se cerró la sesión en este dispositivo',
      buttons: ['Continuar']
    });
    alert.present();
  }

    comprobarActualizaciones() {
       setTimeout(() => {
         if (ver.versionNumber < this.userData.datosactualizacion.versionNumber) {
          // console.log('version Nueva');
           this.NuevaVersion = this.userData.datosactualizacion.version;
          this.EstaDesactualizado = true;
          this.presentarAlertActualizar();
         } else {
           if (this.userData.datosactualizacion.versionNumber === null) {
              this.presentarToastError();
           }
           this.EstaDesactualizado = false;
           // console.log('No hay version nueva');
         }
       }, 500);
     }

  async presentarAlertActualizar() {
    const alert = await this.alertController.create({
      header: this.NuevaVersionDisp,
      message: '<h5>' + this.Actual + ': <strong>' + ver.version +
        '</strong> (' + ver.fechaCompilacion + ')</h5> <h5>' + this.Nueva + ': <strong>'
               + this.userData.datosactualizacion.version + '</strong> ('
               + this.userData.datosactualizacion.fechaCompilacion + ')</h5>',
      buttons: [
        {
          text: this.CancelarBTN,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // console.log('Confirm Cancel: blah');
          }
        }, {
          text: this.ActualizarBTN,
          handler: () => {
            // open(urls.URL_waocardsService + this.userData.datosactualizacion.url, '_system');
            this.descargarActualizacion();
            this.toastDescargando();
            // console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  descargarActualizacion() {
    const request: DownloadRequest = {
      uri: urls.URL_waocardsService + this.userData.datosactualizacion.url,
      title: this.userData.datosactualizacion.NombreArchivo,
      description: 'Instalador de WAOCards',
      mimeType: 'application/vnd.android.package-archive',
      visibleInDownloadsUi: true,
      notificationVisibility: NotificationVisibility.VisibleNotifyCompleted,
      destinationInExternalFilesDir: {
        dirType: 'Downloads',
        subPath: this.userData.datosactualizacion.NombreArchivo
      }
    };


    this.downloader.download(request)
      .then((locat: string) => {this.descargaAlert('Correcto', 'correcta', this.userData.datosactualizacion.NombreArchivo, locat);
                                  this.toaster.dismiss(); })
      .catch((error: any) => this.descargaAlert('Error', 'fue incorrecta', this.userData.datosactualizacion.NombreArchivo,undefined));
  }

  async descargaAlert(Titulo, Mensaje, Nombrearch, locat) {
    console.log('lo que llego: ' , Titulo, Mensaje, Nombrearch, locat);
    const alert = await this.alertController.create({
      header: Titulo,
      message: '¡La descarga de <strong>' + Nombrearch + '</strong> es ' + Mensaje + '!',
      buttons: [
        {
          text: '',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: ' + blah);
          }
        }, {
          text: 'Instalar',
          handler: () => {
            console.log('Confirm Okay');
            this.abrirArchivo(locat);
          }
        }
      ]
    });

    await alert.present();
  }

  abrirArchivo(rutadesc: any) {
    console.log('llego a abrir archivo: ', rutadesc);
    console.log('Se abrira: ', rutadesc);
    this.fileOpener.open(rutadesc, 'application/vnd.android.package-archive')
      .then(() => console.log('File is opened'))
      .catch(e => console.log('Error opening file', e));
  }
 
     cambiarVisibilidad() {
       console.log('cambió la visibilidad');
       this.visible = !this.visible;
       if (this.visible === true) {
         // console.log('cambió a visible');
         this.registro.CambioVisiblilidad(
           this.valor.identificacion,
           '1'
         );
       } if (this.visible === false) {
         // console.log('cambio a invisible');
         this.registro.CambioVisiblilidad(
           this.valor.identificacion,
           '0'
         );
       }
     }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  sesiones() {
    this.userData.inicio();
    setTimeout(() => {
    this.evaluarvisibilidad();
    }, 700);
    setTimeout(() => {
      this.evaluarvisibilidad();
    }, 700);
  }

  evaluarvisibilidad() {
    // console.log('Visibilidad: ' + this.userData.datos.Visible);
    if (this.userData.datos.Visible == '1') {
    this.visible = true;
    }
    if (this.userData.datos.Visible == '0') {
      this.visible = false;
    }
  }

  checkLoginStatus() {
    // console.log('cargo el checklogin: ' + this.valor.identificacion);
    if (this.valor.identificacion !== null) {
      // console.log('inicio sesion correctamente');
      this.updateLoggedInStatus(true);
    } if (this.valor.identificacion === null) {
  this.updateLoggedInStatus(false);
    }
  }

 updateLoggedInStatus(loggedIn: boolean) {
    setTimeout(() => {
      this.loggedIn = loggedIn;
      // console.log(this.loggedIn);
      if (this.loggedIn ) {
        setTimeout(() => {
          this.sesiones();
          this.router.navigateByUrl('/complete-cv');
         // this.router.navigateByUrl('/app/tabs/find-people');
        }, 800);
        setTimeout(() => {
          this.userData.InicioSesion = true;
          this.presentarToastLogged();
          this.intervalo = setInterval(() => {
            // console.log('Se actualizara coordenadas');
            this.mapPage.actualizarCoords();
          }, 25000);
        }, 3500);
        this.presentLoading();
        setTimeout(() => {
          this.evaluarvisibilidad();
        }, 4000);
        //// verifica contactos nuevos
        this.localNotifications.requestPermission();
       this.intervalo = setInterval(() => {
         // console.log('Intervalo activado en LoggedIn');
          this.registro.RevisarSolicitudes(this.valor.identificacion);
          this.verificarNotificaciones();

          this.registro.revisarNuevasTarjetasRepartidas(this.valor.identificacion);
         this.registro.verMisTarjetasRecibidas(this.valor.identificacion);
         this.verificarNotificacionesTR();
       

        }, 5000);
        this.cargarAmigos();
        setTimeout(() => {
           this.verificarChats();
           
        }, 900);
        setTimeout(() => {
          this.registro.Registro_referidos(this.valor.identificacion);
        }, 1500);
      }  else {
        this.valor.guardarid(null);
        this.userData.InicioSesion = false;
        this.presentarToast();
       // console.log('Intervalo desactivado en LoggedIn');
      }
    }, 300);
  }

 
  logout() {
    clearInterval(this.intervalo);
    this.userData.logout();
    this.menu.enable(false);
    this.valor.guardarid(null);
    this.valor.identificacion = null;
    console.log('Se cerró sesión');
    this.userData.datos = [];
    this.mapPage.finalizarRastreo();
    setTimeout(() => {
      this.sesiones();
    }, 300);
    setTimeout(() => {
      this.router.navigateByUrl('/welcome');
    }, 300);
    this.router.navigateByUrl('/welcome');
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: this.IniciandoSesion,
      duration: 1500
    });
    await loading.present();

  }

  async presentarToastError() {
    const toast = await this.toastController.create({
      message: this.NoHayConexion,
      showCloseButton: true,
      position: 'bottom',
      closeButtonText: this.ListoBTN,
      color: 'danger'
    });
    toast.present();
  }

  async presentarToast() {
    const toast = await this.toastController.create({
      message: this.SesionNoI,
      showCloseButton: true,
      position: 'top',
      closeButtonText: this.ListoBTN,
      color: 'success',
      duration: 2000
    });
    toast.present();
  }

  async toastDescargando() {
    this.toaster = await this.toastController.create({
      message: 'Descargando paquete de actualización...',
      position: 'bottom',
      cssClass: 'urgent-notification'
    });
    this.toaster.present();
  }


  async presentarToastLogged() {
    const toast = await this.toastController.create({
      message: this.BienvenidoDeNuevo + this.titlecasePipe.transform(this.userData.datos.Nombre) + '!',
      showCloseButton: true,
      position: 'top',
      closeButtonText: this.ListoBTN,
      color: 'success',
      duration: 3500
    });
    toast.present();
  }

  getUserDataFromDB(iniciateSession: boolean) {
    if (iniciateSession === false) {
    this. presentarToast();
    }
}



}
