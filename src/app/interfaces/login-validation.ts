export interface LoginValidation {
    error: boolean,
    token: string,
    id_usuario: string,
    hecho: boolean,
    mensaje: string
}
