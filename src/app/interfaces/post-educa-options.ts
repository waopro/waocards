export interface PostEducaOptions {
  Nivel_Academico: string;
  Institucion_Educativa: string;
  Institucion_Educativa_Pregrado: string;
  Institucion_Educativa_Especializacion: string;
  Institucion_Educativa_Maestria: string;
  Institucion_Educativa_Doctorado: string;
  Identificacion: string;
  Titulo_Obtenido: string;
  Titulo_ObtenidoDoctorado: string;
  Titulo_ObtenidoEspecializacion: string;
  Titulo_ObtenidoPregrado: string;
  Titulo_ObtenidoTecnico: string;
  Titulo_ObtenidoMaestria: string;
  Ciudad: string;
  Fecha_Inicio: string;
  Fecha_Fin: string;
}
