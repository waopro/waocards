import { TestBed } from '@angular/core/testing';

import { FeriasEventosService } from './ferias-eventos.service';

describe('FeriasEventosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeriasEventosService = TestBed.get(FeriasEventosService);
    expect(service).toBeTruthy();
  });
});
