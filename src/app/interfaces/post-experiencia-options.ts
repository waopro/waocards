export interface PostExpericenciaOptions {
  Identificacion: string;
  Cargo: string;
  Empresa: string;
  Funciones_Cargo: string;
  Fecha_Inicio: string;
  Fecha_Fin: string;
  Telefono1: string;
  Telefono2: string;
}
