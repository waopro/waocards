export interface PostClasificadosOptions {
  Identificacion: string;
  Titulo: string;
  Descripcion: string;
  Contacto: string;
  PalabrasClave: string;
  Tipo: string;
}
