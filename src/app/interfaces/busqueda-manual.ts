export interface BusquedaManualInterface {
    Encontrados: boolean,
    Mensaje: string,
    PEncontradas: any[],
    PSugeridas: any[],
    AmigosComunes: any
}
