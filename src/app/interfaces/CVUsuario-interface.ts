
export interface CVUsuarioInterface {
    error: boolean,
    mensaje: string,
    DatosPersonales: any[],
    Educacion: any[],
    Experiencia: any[],
    Habilidades: any[],
    Idiomas: any[],
    Referencias: any[]
}
