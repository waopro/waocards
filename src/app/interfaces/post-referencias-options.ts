export interface PostReferenciasOptions {
  Tipo: string,
  Nombre_Referencia: string;
  Ocupacion: string;
  Telefono_Celular: string;
  Identificacion: string;
}
