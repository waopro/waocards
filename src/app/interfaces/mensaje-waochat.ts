export interface MensajeWAOChat {
    /*
    text: string,
    sender: string,
    image: string
    */
    nombre: string,
    mensaje: string,
    UsrFoto: string,
    numeroMsg?: number,
    fecha: number,
    imagenMsg?: string,
    PDFMsg?: string
}
