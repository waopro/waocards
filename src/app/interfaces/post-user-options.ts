export interface PostUserOptions {
  id_user: string;
  Tipo_Perfil: string;
  Cargo_Actual: string;
  Ciudad_Residencia: string;
  Direccion_Residencia: string;
  Perfil: string;
  Nivel_Academico: string;
  Twitter: string;
  Instagram: string;
  Linkedin: string;
  Github: string;
  WWW: string;
  Profesion: string;
  Institucion_Educativa: string;
  Ha_Trabajado: string;
  Cuantos_Idiomas: string;
  referidos?: string;
}
