
export interface EnterpriseOptions {
  enterprisename: string;
  nit_enterprise: string;
  Enterprise_tel_cel: string;
  Enterpriseemail: string;
  ciiu: string;
}
