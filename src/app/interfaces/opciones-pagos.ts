export interface OpcionesPagos {
    num_tarjeta: string;
    name_lastname: string;
    fecha_exp:string;
    cod_seguridad: string;
    num_doc: string
}
