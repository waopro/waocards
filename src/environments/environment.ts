// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAR6XQIWn4x6lFsKKgyn_-vNPVDed4qLQQ',
    authDomain: 'waochat-4a33f.firebaseapp.com',
    databaseURL: 'https://waochat-4a33f.firebaseio.com',
    projectId: 'waochat-4a33f',
    storageBucket: '',
    messagingSenderId: '298005711473',
    appId: '1:298005711473:web:1e6b959a60d3cdd1'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
